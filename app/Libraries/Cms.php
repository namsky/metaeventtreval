<?php
namespace App\Libraries;
use Config\XCache;
use App\Libraries\XCached;
class Cms
{
  	public $static_path = 'statics';
  	public $vendor_path = 'vendors';
  	public $allow_type = ['js','css'];
  	public $vendors = [];
  	public $type = ['js','css'];
  	public $breadcrumbs = [];
  	public $cache;
	public function __construct()
	{
		// Load cache
		$this->cache = \Config\Services::cache();
	}
	
    public function default_locale() {
		$default_locale = $this->cms_config('default_locale');
		return ($default_locale)?$default_locale:'vn';
	}
    /**
     * Get lang
     * @return string
     */
    public function lang($key, $args = [], $locale = 'default') {
		$cached = cms_config('cache');
		if($locale == 'default') $locale_name = $this->default_locale();
		else $locale_name = $locale;
		$lang = $cached?$this->cache->get('lang_'.$locale_name.'_'.$key):false;
		if(!$lang) {
			$model = model('App\Modules\Cms\Models\LanguageDataModel');
			$lang_data = $model->where('key', $key)->where('locale', $locale_name)->first();
			if(isset($lang_data->value)) {
				$lang = $lang_data->value;
				if(is_array($args) && count($args)) {
					foreach($args as $key=>$value) {
						if(is_numeric($key)) $_key = $key+1;
						else $_key = $key;
						$lang = str_replace('{'.$_key.'}', $value, $lang);
					}
				}
				$this->cache->save('lang_'.$locale_name.'_'.$key, $lang, 86400);
			}
		}
		return ($lang)?$lang:lang($key);
	}
    /**
     * Load widget
     * @return html
     */
    public function widget($widget, $agrs) {
		try {
			if(strpos($widget, '/')) {
				$modules = explode('/', $widget);
				$namespace = 'App\Modules\\'.$modules[0].'\Widgets\\'.$modules[1];
			}
			else {
				$namespace = 'App\Widgets\\'.$widget;
			}
			return view_cell($namespace.'::index', $agrs);
		} catch(Exception $e) {
			return false;
		}
    }
    /**
     * Load vendor to Cms
     * @return void
     */
    public function load_vendors($module, $deep_scan = true) {
        if(is_array($module)) {
            foreach($module as $loader) $this->load_vendors($loader);
        } else {
            $path = realpath(FCPATH.DIRECTORY_SEPARATOR.$this->static_path.DIRECTORY_SEPARATOR.$this->vendor_path.DIRECTORY_SEPARATOR.$module);
            if(is_dir($path)) {
                $this->vendors[$module] = [];
                $this->scan_directiory($path, $module, $deep_scan);
            }
        }
    }
    /**
     * Scan directiory
     * @return void
     */
    private function scan_directiory($path, $module, $deep_scan=true) {
        $directories = array_diff(scandir($path), array('..', '.'));
        $return = array();
        if(is_array($directories)) {
			$sub_dirs = [];
            foreach($directories as $dir) {
                $path_dir = realpath($path.DIRECTORY_SEPARATOR.$dir);
                if(!is_dir($path_dir)) {
                    $file = $path.DIRECTORY_SEPARATOR.$dir;
                    $ext = pathinfo($file, PATHINFO_EXTENSION);
                    if(in_array($ext, $this->allow_type)) {
                        if(!isset($this->vendors[$module])) $this->vendors[$module] = [];
                        $remove_path = realpath(FCPATH.DIRECTORY_SEPARATOR.$this->static_path);
                        $url = str_replace($remove_path, CDN, $file);
                        $url = str_replace("/\\", '/', $url);
                        $url = str_replace("\\", '/', $url);
                        $this->vendors[$module][$ext][] = $url;
                    }
                }
                else {
					$sub_dirs[] = $path_dir;
                }
            }
            if($deep_scan) {
				foreach($sub_dirs as $path_dir) {
					   $this->scan_directiory($path_dir, $module, $deep_scan);
				}
            }
        }
    }
    /**
     * Return a key value from database, or all the object if $item = null.
     *
     * @param $item String
     * @return mixed
     */
    public function cms_config($item = null)
    {
		if($item) {
			$config = $this->cache->get('config_'.$item);
			if(!$config) {
				$configModel = model('App\Modules\Admins\Models\ConfigurationModel');
				$config = $configModel->get_config($item);
				if(is_null($config)) $config = false;
				$this->cache->save('config_'.$item, $config, 86400);
			}
			return $config;
		}
		else return null;
    }
    /**
     * Load breadcrumb to Cms
     * @return void
     */
    public function set_breadcrumb($title, $slug=NULL) {
		if(is_array($title) || is_object($title)) {
			$categories = $title;
			foreach($categories as $category) {
				$this->set_breadcrumb($category->name, $category->slug);
				if(isset($category->child) && is_array($category->child))
					$this->set_breadcrumb($category->child);
				break;
			}
		} else
			$this->breadcrumbs[] = [$title, $slug];
    }
    /**
     * Show breadcrumb to Cms
     * @return void
    */
    public function breadcrumb() {
		if(count($this->breadcrumbs)) {
			$list_item = [];
		?>
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
			<?
				echo '<li class="breadcrumb-item"><a href="'.URL.'">Trang chủ</a></li>';
				$site_name = $this->cms_config('site_name')?$this->cms_config('site_name'):'Trang chủ';
				$list_item[] = '
					{
						"@type": "ListItem", 
						"position": 1, 
						"name": "'.$site_name.'",
						"item": "'.URL.'"
					}';
				$i = 2;
				foreach($this->breadcrumbs as $item) {
					if(strstr($item[1], 'http')) $url = $item[1];
					else $url = URL.'/'.$item[1];
					echo '<li class="breadcrumb-item"><a href="'.$url.'">'.$item[0].'</a></li>';
					$list_item[] = '
					{
						"@type": "ListItem", 
						"position": '.$i.', 
						"name": "'.$item[0].'",
						"item": "'.$url.'"  
					}';
					$i++;
				}
			?>
			</ol>
			<?php if(count($list_item)) { ?>
			<script type="application/ld+json">
			{
				"@context": "https://schema.org/", 
				"@type": "BreadcrumbList", 
				"itemListElement": [<?=implode(",", $list_item);?>]
			}
			</script>
			<?php } ?>
		</nav>
		<?
		}
    }
    /**
     * Load a asset file
     *
     * @param mixed $type Could be css or js.
     * @param string $filename Full file name.
     * @return string
     */
    public function load_asset($type, $filename, $dir=NULL) {
		if($dir) $dir = '/'.$dir;
        switch ($type)
        {
            case 'css':
                return '<link href="' . CDN.$dir.'/css/' . $filename . '" rel="stylesheet">';
                break;
            case 'js':
                return '<script src="' . CDN.$dir.'/js/' . $filename . '"></script>';
                break;
                break;
            case 'image':
                return CDN.$dir.'/images/' . $filename;
                break;
            case 'custom':
                return $filename;
                break;
        }
        foreach($this->vendors as $vendor) {
            if(isset($vendor[$type])) {
                foreach($vendor[$type] as $url) {
                    if($type=='css') echo '<link href="'.$url.'" rel="stylesheet" />'."\n";
                    elseif($type=='js') echo '<script src="'.$url.'"></script>'."\n";
                }
            }
        }
    }
    /**
     * Render vendor to views
     * @return void
     */
    public function show_asset($type) {
		$is_first = true;
        foreach($this->vendors as $vendor) {
            if(isset($vendor[$type])) {
                foreach($vendor[$type] as $url) {
					if($is_first) {
						$is_first = false;
						$tab = "";
					} else $tab = "\t";
                    if($type=='css') echo $tab.'<link href="'.$url.'" rel="stylesheet" />'."\n";
                    elseif($type=='js') echo $tab.'<script src="'.$url.'"></script>'."\n";
                }
            }
        }
    }
	
    /**
     * Auto gen javascript for CMS
     * @return void
     */
    public function gen_js($args = NULL)
    {
		$base_url = isset($args['base_url'])?$args['base_url']:current_url();
		$is_datatable = isset($args['is_datatable'])?$args['is_datatable']:false;
		$columns = isset($args['columns'])?$args['columns']:false;
		$hot = isset($args['hot'])?$args['field_slug']:false;
		$field_slug = isset($args['field_slug'])?$args['field_slug']:false;
		$search = isset($args['search'])?(bool)$args['search']:true;
		$all_item = isset($args['all_item'])?(bool)$args['all_item']:true;
		$render = isset($args['render'])?(bool)$args['render']:true;
		$save = isset($args['save'])?(bool)$args['save']:true;
		$remove = isset($args['remove'])?(bool)$args['remove']:true;
		$restore = isset($args['restore'])?(bool)$args['restore']:true;
		?>
		<script>
			var args = {};
            <?php if($is_datatable) { ?>
            function loadTable() {
                var param = $.param(args);
                var table = $('#content_table').DataTable({
                    autoWidth: false,
                    scrollCollapse: false,
                    paging: false,
                    info: false,
                    searching: false,
                    ordering: false,
                    serverSide: true,
                    processing: true,
                    ajax: {
                        "url": "<?=$base_url;?>/datatable?"+param,
                        "dataSrc": function (response) {
                            data = response;
                            $("#total_items").html(data['recordsTotal']);
                            $("#paging").html(data['paging']);
                            return response['data'];
                        },
                    },
                    "columns": [<?=$columns?>]
                });
                table.destroy();
            }
            function page(page_number) {
                args['page'] = page_number;
                loadTable();
            }
            <?php } ?>
            function clear_filter() {
                <?php if($is_datatable) { ?>
                $("#form-filter")[0].reset();
                args = {};
                loadTable();
                <?php } else { ?>
                window.location.replace("<?=$base_url;?>");
                <?php } ?>
            }
            <?php if($render) { ?>
            function render(id) {
                $('#form-item')[0].reset();
                if(id) {
                    $("#btn_save").html("Update");
                    $.ajax({url: "<?=$base_url;?>/get_data?item_id="+id, success: function(result){
                        $.each(result, function(key, value) {
                            if ($("#"+key).is("textarea")) {
                                if($("#"+key).hasClass('editor')) {
                                    if(!value) value = '';
                                    if(typeof(tinymce) !== 'undefined') {
                                        var editor = tinymce.get(key).setContent(value);
                                    }
                                }
                                else {
                                    $("#"+key).html(value).change();
                                }
                            }
                            else if($("#"+key).attr('type') == 'checkbox') {
                                var value = parseInt(value);
                                $("#"+key).attr("checked", !!value);
                            }
                            else if($("#"+key).hasClass('datepicker')) {
                                if(value > 0) {
                                    var date = new Date(value * 1000);
                                    var year = date.getFullYear();
                                    var month = date.getMonth()+1;
                                    if(month<10) month = '0'+month;
                                    var day = date.getDate();
                                    if(day<10) day = '0'+day;
                                    var hour = date.getHours();
                                    if(hour<10) hour = '0'+hour;
                                    var min = date.getMinutes();
                                    if(min<10) min = '0'+min;
                                    $("#"+key).val(day+'/'+month+'/'+year).change();
                                    $("#"+key+"_hour").val(hour).change();
                                    $("#"+key+"_min").val(min).change();
                                }
                            }
                            else if($("#"+key).hasClass('select2')) {
                                $("#"+key).html(value).change();
                            }
                            else {
                                if($.isNumeric(value)) value = parseInt(value);
                                $("#"+key).val(value).change();
                            }
                        });
                    }, dataType: "json"});
                } else {
                    $("#id").val('');
                    $(".select2").val('').change();
                    $("#btn_save").html("Save");
                }
            }
            <?php } ?>
            <?php if($save) { ?>
            function save() {
                if(typeof(tinymce) !== 'undefined') {
                    tinymce.triggerSave();
                }
                var args = $("#form-item").serialize();
                $.post("<?=$base_url;?>/save_data", args, function(data) {
                    if(data.status==true || data.status=="success") {
                    <?php if($is_datatable) { ?>
                        $('#modal-item').modal('hide');
                        notify("", data.message, 'success', true);
                        loadTable();
                    <?php } else { ?>
                        notify("", data.message, 'success', true);
                        setTimeout(function(){
                            location.reload();
                        }, 2000);
                    <?php } ?>
                    } else {
                        notify("", data.message, 'warning', true);
                    }
                });
                return false;
            }
            <?php } ?>
            <?php if($remove) { ?>
            function remove(id) {
                swal({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    type: "warning",
                    showCancelButton: !0,
                    buttonsStyling: !1,
                    confirmButtonClass: "btn btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonClass: "btn btn-secondary"
                }).then(t => {
                    if(id && t.value) {
                        $.post("<?=$base_url;?>/remove", {id:id}, function(data,status){
                            if(data) {
                                $("#item-"+id).remove();
                                swal({
                                    title: "Your item has been deleted.",
                                    type: "success",
                                    buttonsStyling: !1,
                                    confirmButtonClass: "btn btn-success"
                                });
                            }
                            else {
                                swal({
                                    title: "Something went wrong.",
                                    type: "warning",
                                    buttonsStyling: !1,
                                    confirmButtonClass: "btn btn-primary"
                                });
                            }
                        });
                    }
                });
                return false;
            }
            <?php } ?>
            <?php if($restore) { ?>
            function restore(id) {
                $.post("<?=$base_url;?>/restore", {id:id}, function(data,status){
                    if(data) {
                        $("#item-"+id).remove();
                        swal({
                            title: "Your item has been restored.",
                            type: "success",
                            buttonsStyling: !1,
                            confirmButtonClass: "btn btn-success"
                        });
                    }
                    else {
                        swal({
                            title: "Something went wrong.",
                            type: "warning",
                            buttonsStyling: !1,
                            confirmButtonClass: "btn btn-primary"
                        });
                    }
                });
            }
            <?php } ?>
            function update(field, item_id, element) {
                var last_html = $(element).html();
                $(element).html('<div class="spinner-grow spinner-grow-sm" role="status"><span class="sr-only">Loading...</span></div>');
                $.post("<?=$base_url;?>/update_"+field, {item_id:item_id}, function(data) {
                    if(data.status == "success") {
                        loadTable();
                    } else {
                        notify("", "Something went wrong", 'warning', true);
                        $(element).html(last_html);
                    }
                });
            }
            /* Upload thumb */
            function uploadImage(input, id) {
                if(input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#"+id).addClass('loading'); 
                    };
                    reader.readAsDataURL(input.files[0]);
                    var form_data = new FormData();
                    form_data.append('image', input.files[0]);
                    $.ajax({
                        url: '<?=URL?>/v-manager/uploads/photo',
                        type: 'POST',
                        data: form_data,
                        contentType: false,
                        processData: false,
                        success: function(response){
                            if(response != 0) {
                                if(response.status == 'success') {
                                    if ($("#"+id).is("textarea")) {
                                        var current = $("#"+id).html();
                                        if(current) current += "\n";
                                        $("#"+id).html(current+response.location); 
                                    } else {
                                        $("#"+id).val(response.location); 
                                    }
                                } else alert(response.message);
                            } else {
                                alert(response);
                            }
                            $("#"+id).removeClass('loading'); 
                        },
                    });
                }
            }
            /* Upload File */
            function uploadFile(input, id) {
                if(input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $("#"+id).addClass('loading'); 
                    };
                    reader.readAsDataURL(input.files[0]);
                    var form_data = new FormData();
                    form_data.append('file', input.files[0]);
                    $.ajax({
                        url: '<?=URL?>/v-manager/uploads/file',
                        type: 'POST',
                        data: form_data,
                        contentType: false,
                        processData: false,
                        success: function(response){
                            if(response != 0) {
                                if(response.status == 'success') {
                                    if ($("#"+id).is("textarea")) {
                                        var current = $("#"+id).html();
                                        if(current) current += "\n";
                                        $("#"+id).html(current+response.location); 
                                    } else {
                                        $("#"+id).val(response.location); 
                                    }
                                } else alert(response.message);
                            } else {
                                alert(response);
                            }
                            $("#"+id).removeClass('loading'); 
                        },
                    });
                }
            }

            $(document).ready(function() {
                $('.modal.custom').on('show.bs.modal', function (e) {
                    $('.card').hide();
                });
                $('.modal.custom').on('hidden.bs.modal', function (e) {
                    $('.card').show();
                });
                <?php if($is_datatable) { ?>
                loadTable();
                $("#form-filter").submit(function() {
                    var form_args = $("#form-filter").serializeArray();
                    $.each(form_args, function(key, value) {
                        if(typeof(value['name']) !== 'undefined')
                            args[value['name']] = value['value'];
                    });
                    loadTable();
                    return false;
                });
                <?php } ?>
                <?php if($hot) { ?>
                $('.hot').change(function() {
                    var id = $(this).val();
                    var value = $(this).prop('checked');
                    if(id) {
                        $.post("<?=$base_url;?>/hot", {id:id, value:value}, function(result) {
                            if(result.status=="success") {
                            } else {
                                notify("", data.message, 'warning', true);
                            }
                        });
                    }
                });
                <?php } ?>
                <?php if($search) { ?>
                $("[name='q']").keypress(function (e) {
                    if (e.which == 13) {
                        $('#form-filter').submit();
                        return false;
                    }
                });
                <?php } ?>
                <?php if($field_slug) { ?>
                $("#<?=$field_slug?>").keyup(function (e) {
                    $("#btn_save").prop('disabled', true);
                    var id = $("#id").val();
                    var data = $(this).val();
                    if(data) {
                        $.post("<?=$base_url;?>/get_slug", {id:id, data:data}, function(result) {
                            if(result.status=="success") {
                                $("#slug").val(result.slug);
                            } else {
                                notify("", result.message, 'warning', true);
                            }
                            $("#btn_save").prop('disabled', false);
                        });
                    }
                });
                <?php } ?>
                <?php if($all_item) { ?>
                $("#all_item").click(function (e) {
                    if($(this).is(':checked')) $(".items").prop('checked', true);
                    else $(".items").prop('checked', false);
                });
                $("#do_action").click(function (e) {
                    var action = $("#bulk_actions").val();
                    var values = $(".items").map(function(){
                        if($(this).is(':checked')) return $(this).val();
                    }).get();
                    var ids = values.join(',');
                    if(ids && action) {
                        $.post("<?=$base_url;?>/bulk_action", {action:action, ids:ids}, function(data) {
                            if(data=="true") {
                                location.reload();
                            } else {
                                notify("", "Something went wrong", 'warning', true);
                            }
                        });
                    }
                });
                <?php } ?>
                $(".thumb_upload").change(function() {
                    var id = $(this).data("target");
                    uploadImage(this, id);
                });
                $(".file_upload").change(function() {
                    var id = $(this).data("target");
                    uploadFile(this, id);
                });
            });
		</script>
		<?
    }
    /**
     * Return the code needed to load the configured editor.
     *
     * @return string
     * @todo Revisar este código de 'invocaçao' do editor.
     * @todo Adicionar o filemanager no tinyMCE.
     */
    public function load_editor($selector='.editor', $height = 500, $mode = 'full')
    {
		switch($mode) {
			case "mini":
				$plugin = 'image imagetools link media table hr lists wordcount';
				$toolbar = 'formatselect | bold italic | align | outdent indent |  numlist bullist';
				break;
			default:
				$plugin = 'searchreplace autolink autosave fullscreen image imagetools link media table hr code toc advlist lists wordcount codesample';
				$toolbar = 'undo redo | formatselect removeformat toc | bold italic | align | outdent indent | numlist bullist | image imagetools media link unlink | codesample blockquote code fullscreen';
		}
		?>
		<script data-minify-level="0">
			/* Editor */
			if(typeof(tinymce) !== 'undefined') {
				tinymce.init({
					selector: '<?=$selector?>',  // change this value according to your HTML
					plugins: '<?=$plugin?>',
					toolbar: '<?=$toolbar?>',
					menubar: false,
					images_upload_url: '<?=URL?>/v-manager/uploads/photo?field=file',
					height: <?=$height?>,
					toc_title: 'Nội dung',
					relative_urls : false,
					remove_script_host : false,
				});
			}
		</script>
		<?
    }
    public function show_menu($data) {
		$code = '<ol class="dd-list">';
		if(is_array($data) && count((array)$data))
		foreach ($data as $item) {
			$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
				$code .= '<div class="dd-handle">'.clearString($item->title, false).'</div>';
				$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
				$code .= '<span style="margin-right: 5px;font-size: 11px;">'.$item->type.'</span>';
				$code .= '<a class="edit p-1" href="javascript:;" onclick="render_item('.$item->id.');" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>';
				$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove_item('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
				if(is_array($item->child) && count($item->child)) $code .= $this->show_menu($item->child);
			$code .= '</li>';
		}
		$code .= '</ol>';
		return $code;
	}
	public function show_checkbox_categories($data, $current_categories = [], $is_child=false) {
		$code = '<ul class="category-list">';
		if(is_array($data) && count((array)$data))
		foreach ($data as $item) {
			$checked = '';
			if(in_array($item->id, $current_categories)) {
				$checked = 'checked';
			}
            $code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
            $class = ($is_child)?'category_child':'category_parrent';
			$code .= '<input class="category_item '.$class.'" type="checkbox" '.$checked.' id="category_'.$item->id.'" name="categories[]" value="'.$item->id.'" /><label for="category_'.$item->id.'">&nbsp;'.clearString($item->name, false).'</label>';
			if(is_array($item->child) && count((array)$item->child)) {
				$script = '';
                $code .= $this->show_checkbox_categories($item->child, $current_categories, true);
            }
			$code .= '</li>';
		}
        $code .= '</ul>';
		return $code;
	}
    public function show_categories($data) {
		$code = '<ol class="dd-list">';
		if(is_array($data) && count($data))
		foreach ($data as $item) {
			$seo_status = '<span class="seo_status seo_bad"></span>';
			if($item->description && $item->keywords) {
				$seo_status = '<span class="seo_status seo_good"></span>';
				if(strlen($item->description) < 150 || strlen($item->description) > 350)
					$seo_status = '<span class="seo_status seo_nomal"></span>';
			}
			$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
				$code .= '<div class="dd-handle">'.clearString($item->name, false).'</div>';
				$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
				$code .= '<span style="margin-right: 5px;font-size: 11px;">'.number_format($item->count,0,',','.').' '.lang('posts').'</span>';
				$code .= $seo_status;
				$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" ><i class="fa fa-edit"></i></a>';
				$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
				if(is_array($item->child) && count($item->child)) {
					$code .= $this->show_categories($item->child);
				}
			$code .= '</li>';
		}
		$code .= '</ol>';
		return $code;
	}
    public function dropdown_categories($data, $level=0) {
		if($level == 0) $code = '<option value="0">No Parent</option>';
		else $code = '';
		if(is_array($data) && count((array)$data))
		foreach ($data as $item) {
			$space = '';
			if($level) for($i=0;$i<$level;$i++) $space .= '-- ';
			$code .= '<option value="'.$item->id.'">'.$space.$item->name.'</option>';
			if(is_array($item->child) && count((array)$item->child)) $code .= $this->dropdown_categories($item->child, $level+1);
		}
		return $code;
	}
    public function dropdown_menus($data, $level=0) {
		if($level == 0) $code = '<option value="0">No Parent</option>';
		else $code = '';
		if(is_array($data) && count((array)$data))
		foreach ($data as $item) {
			$space = '';
			if($level) for($i=0;$i<$level;$i++) $space .= '-- ';
			$code .= '<option value="'.$item->id.'">'.$space.$item->title.'</option>';
			if(is_array($item->child) && count((array)$item->child)) $code .= $this->dropdown_menus($item->child, $level+1);
		}
		return $code;
	}
    public function schemas($type, $args=NULL) {
		//https://technicalseo.com/tools/schema-markup-generator/
		switch($type) {
			case "product":
				$json = '<script type="application/ld+json">';
				$json .= '{';
				$json .= '  "@context": "http://www.schema.org",';
				$json .= '  "@type": "product",';
				$json .= '  "brand": "EGO",';
				$json .= '  "name": "EGO",';
				$json .= '  "image": "https://go.com/uploads/images/1ef607f1e2af0901c8877e0a11d2777e.png",';
				$json .= '  "description": "1",';
				$json .= '  "aggregateRating": {';
				$json .= '	"@type": "aggregateRating",';
				$json .= '	"ratingValue": "1",';
				$json .= '	"reviewCount": "1"';
				$json .= '  }';
				$json .= '}';
				$json .= '</script>';
				return $json;
				break;
			case "item_page":
				$json = '';
				if(!empty($args['url']) && !empty($args['title'])) {
					$json = '<script type="application/ld+json">
					{
						"@context": "https://schema.org/",
						"@type": "ItemPage",
						"url": "'.$args['url'].'",
						"mainEntityOfPage":"'.$args['title'].'"
					}
					</script>';
				}
				return $json;
				break;
			case "website":
				$json = '<script type="application/ld+json">';
				$json .= '{';
				$json .= '  "@context": "http://www.schema.org",';
				$json .= '  "@type": "WebSite",';
				$json .= '  "name": "EGO",';
				$json .= '  "alternateName": "EGO Play",';
				$json .= '  "url": "https://8am.us/"';
				$json .= '}';
				$json .= '</script>';
				return $json;
				break;
			case "person":
				$json = '<script type="application/ld+json">';
				$json .= '{';
				$json .= '  "@context": "http://www.schema.org",';
				$json .= '  "@type": "person",';
				$json .= '  "name": "Viet Vu Duy",';
				$json .= '  "jobTitle": "CTO",';
				$json .= '  "url": "https://8am.us/",';
				$json .= '  "address": {';
				$json .= '	"@type": "PostalAddress",';
				$json .= '	"streetAddress": "So 9 Ngo 252 Tay Son",';
				$json .= '	"postOfficeBoxNumber": "100000",';
				$json .= '	"addressLocality": "Ha Noi",';
				$json .= '	"addressRegion": "HANOI",';
				$json .= '	"postalCode": "100000",';
				$json .= '	"addressCountry": "Vietnam"';
				$json .= '  },';
				$json .= '  "email": "vietvd.gtv@gmail.com",';
				$json .= '  "telephone": "0967374222",';
				$json .= '  "birthDate": "1990-02-02"';
				$json .= '}';
				$json .= '</script>';
				return $json;
				break;
			case "article":
				$json = '<script type="application/ld+json">
				{
				  "@context": "https://schema.org",
				  "@type": "Article",
				  "headline": "",
				  "image": "",  
				  "author": {
					"@type": "",
					"name": ""
				  },  
				  "publisher": {
					"@type": "Organization",
					"name": "",
					"logo": {
					  "@type": "ImageObject",
					  "url": "",
					  "width": ,
					  "height": 
					}
				  },
				  "datePublished": ""
				}
				</script>';
				return $json;
				break;
			case "breadcrumb":
				$json = '<script type="application/ld+json">
				{
				  "@context": "https://schema.org/", 
				  "@type": "BreadcrumbList", 
				  "itemListElement": [{
					"@type": "ListItem", 
					"position": 1, 
					"name": "",
					"item": ""  
				  },{
					"@type": "ListItem", 
					"position": 2, 
					"name": "",
					"item": ""  
				  }]
				}
				</script>';
				return $json;
				break;
			case "search":
				$json = '<script type="application/ld+json">
				{
				  "@context": "https://schema.org/",
				  "@type": "WebSite",
				  "name": "'.cms_config('site_title').'",
				  "url": "'.URL.'",
				  "potentialAction": {
					"@type": "SearchAction",
					"target": "'.URL.'/search/?q={search_term_string}",
					"query-input": "required name=search_term_string"
				  }
				}
				</script>';
				return $json;
				break;
			case "faq":
				$json = '';
				if(!empty($args['faqs']) && is_array($args['faqs'])) {
					$content = '';
					foreach($args['faqs'] as $key=>$faq) {
						$content .= '{
							"@type": "Question",
							"name": "'.$faq->question.'",
							"acceptedAnswer": {
							  "@type": "Answer",
							  "text": "'.$faq->answer.'"
							}
						}';
						if($key<count($args['faqs'])-1) $content .= ',';
					}
					if($content) {
					$json = '<script type="application/ld+json">
					{
					  "@context": "https://schema.org",
					  "@type": "FAQPage",
					  "mainEntity": ['.$content.']
					}
					</script>';
					}
				}
				return $json;
				break;
		}
	}
}