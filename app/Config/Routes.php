<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes(true);

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */


/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
/**
 * --------------------------------------------------------------------
 * Modules Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
$path = realpath(APPPATH.DIRECTORY_SEPARATOR.'Modules');
if(is_dir($path)) {
    $file = $path.DIRECTORY_SEPARATOR.'Admins'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'Routes.php';
    if(file_exists($file)) require_once $file;
    $file = $path.DIRECTORY_SEPARATOR.'Cms'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'Routes.php';
    if(file_exists($file)) require_once $file;
	$directories = array_diff(scandir($path), array('..', '.', 'Admins', 'Cms'));
	if(is_array($directories)) {
		foreach($directories as $dir) {
			$file = $path.DIRECTORY_SEPARATOR.$dir.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'Routes.php';
			if(file_exists($file)) require_once $file;
		}
	}
}