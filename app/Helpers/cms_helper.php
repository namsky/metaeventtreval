<?php

function encrypt($data, $key=NULL)
{
	$config = config('Encryption');
	if(!$key) $key = $config->key;
	$method = 'AES-256-CTR';
    $ivSize = openssl_cipher_iv_length($method);
    $iv = openssl_random_pseudo_bytes($ivSize);
    $encrypted = openssl_encrypt($data, $method, $key, OPENSSL_RAW_DATA, $iv);
    // For storage/transmission, we simply concatenate the IV and cipher text
    $encrypted = base64_encode($iv . $encrypted);
    return $encrypted;
}

function decrypt($data, $key=NULL)
{
	$config = config('Encryption');
	if(!$key) $key = $config->key;
	$method = 'AES-256-CTR';
    $data = base64_decode($data);
    $ivSize = openssl_cipher_iv_length($method);
    $iv = substr($data, 0, $ivSize);
    $data = openssl_decrypt(substr($data, $ivSize), $method, $key, OPENSSL_RAW_DATA, $iv);
    return $data;
}
if (!function_exists('cms_lang'))
{
    function cms_lang($key, $args = [], $locale = 'default')
    {
        if(!is_array($args)) $args = [$args];
		$cms = new \App\Libraries\Cms();
        return $cms->lang($key, $args, $locale);
    }
}

if (!function_exists('cms_config'))
{
    /**
     * Return a config item or all the object from config.json.
     *
     * @param $item mixed Config item.
     * @return mixed
     */
    function cms_config($item = null)
    {
		$cms = new \App\Libraries\Cms();
        return $cms->cms_config($item);
    }
}
use CodeIgniter\HTTP\UserAgent;
if(!function_exists('sendmail'))
{
    function sendmail($to, $from, $reply, $name, $subject, $message, $debug = 0, $type = 1) {
        if (function_exists('imap_open') && $type == 0) {
            $message = stripslashes($message);
            $subject = stripslashes($subject);
            $uid = strtoupper(md5(uniqid(time())));
            $to = trim($to);
            $header = "From: " . $name . " <" . $from . ">\r\nReply-To: " . $reply . "\r\n";
            $header .= "MIME-Version: 1.0\r\n";
            $header .= "Content-Type: text/html\r\n";
            $header .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
            $header .= "$message\r\n";
        } else {
            $smtp_host = cms_config('smtp_host');
            $smtp_username = cms_config('smtp_username');
            $smtp_password = decrypt(decrypt(cms_config('smtp_password')));
            $smtp_port = cms_config('smtp_port');
            if($smtp_host) {
                if($smtp_host == 'smtp.gmail.com') {
                    $from = $smtp_username;
                    $reply = $smtp_username;
                }
                require_once APPPATH.'ThirdParty/mailer/PHPMailerAutoload.php';
                $mail = new PHPMailer();
                $mail->CharSet = 'UTF-8';
                $mail->isSMTP();
                $mail->SMTPSecure = "ssl";
                $mail->SMTPDebug = $debug;
                $mail->Debugoutput = 'html';
                $mail->Host = $smtp_host;
                $mail->Port = $smtp_port;
                $mail->SMTPAuth = true;
                $mail->Username = $smtp_username;
                $mail->Password = $smtp_password;
                $mail->setFrom($from, $name);
                $mail->addReplyTo($reply, $name);
                $mail->addAddress($to);
                $mail->Subject = $subject;
                $mail->msgHTML($message);
                return $mail->send();
            }
        }
    }
}
if(!function_exists('getOS'))
{
	function getOS($useragent) {
        $os = '';
        if($useragent) {
            $agent = new UserAgent();
            $agent->parse($useragent);
            $os = $agent->getPlatform();
            if($os == 'Unknown Windows OS') $os = 'Windows OS';
        }
        return $os;
	}
}
if(!function_exists('getBrowser'))
{
	function getBrowser($useragent) {
		$agent = new UserAgent();
		$agent->parse($useragent);
		$browser = $agent->getBrowser();
		return $browser;
	}
}
if(!function_exists('cms_redirect'))
{
    function cms_redirect($uri, $type=301)
    {
		header('Location: '.$uri, true, $type);
		exit();
    }
}
if(!function_exists('check_service'))
{
	function check_service($service) {
		if (stristr(PHP_OS, "win")) {
			$cmd = 'tasklist /FI "IMAGENAME eq '.$service.'.exe" 2>NUL | find /I /N "'.$service.'.exe"';
			@exec($cmd, $output);
			if(count($output)) return true;
			return false;
		}
		else
		{
			$cmd = "/sbin/service $service status";
			exec($cmd . " 2>&1", $output, $return_val);
			if(!is_array($output)) $output[0] = $output;
			foreach($output as $line) {
				if(strpos($line, 'running') || strpos(strtolower($line), 'pid file exists')) return true;
			}
			return false;
		}
    }
}
if (!function_exists('widget'))
{
    function widget($widget, $args=[])
    {
		$cms = new \App\Libraries\Cms();
        return $cms->widget($widget, $args);
    }

}
if(!function_exists('notify'))
{
    function notify($message, $title=NULL, $type = 'info', $dismissible = FALSE, $offset = NULL, $align = 'center')
    {
		if(!$offset) $offset = [15,15];
        if(!in_array($type, ['info','success','warning','danger','error'])) $type = 'info';
        $dismissible = ($dismissible)?'true':'false';
        $str = '
		<!-- Notification -->
		<script>
			$(function() {
				$.notify({
					title: "'.$title.'",
					message: "'.$message.'",
				},{
					position: "absolute",
					element: "body",
					type: "'.$type.'",
					allow_dismiss: '.$dismissible.',
					placement: {
						from: "top",
						align: "'.$align.'"
					},
					offset: {
						x: '.$offset[1].',
						y: '.$offset[0].'
					},
					spacing: 10,
					delay: 1000,
					timer: 5000,
					animate: {
						enter: "animated fadeInDown",
						exit: "animated fadeOutUp"
					},
					template: "<div data-notify=\"container\" class=\"alert alert-dismissible alert-{0} alert-notify\" role=\"alert\"><span class=\"alert-icon\" data-notify=\"icon\"></span> <div class=\"alert-text\"</div> <span class=\"alert-title\" data-notify=\"title\">{1}</span> <span data-notify=\"message\">{2}</span></div><button type=\"button\" class=\"close\" data-notify=\"dismiss\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button></div>"
				});
			});
		</script>
        ';
        return $str;
    }
}
function set_referer($key) {
    $session = \Config\Services::session();
    $current_url = base_url(uri_string());
    $session->set([$key.'_referer' => $current_url]);
}
function set_message($message='', $type='info', $redirect = null)
{
	if(!empty($message))
	{
		$session = \Config\Services::session();
		$session->set('message', $type .'::'. $message);
	}
	if($redirect !== null) redirect($redirect);
}
function get_message($message='', $type='info')
{
	$return = array('message' => $message, 'type' => $type);
	// Does session data exist?
	if(empty($message))
	{
		$session = \Config\Services::session();
		$message = $session->get('message');
		if(!empty($message))
		{
			// Split out our message parts
			$temp_message = explode('::', $message);
			$return['type']     = $temp_message[0];
			$return['message']  = $temp_message[1];
			unset($temp_message);
		}
	}
	$session->set('message', '');
	return $return;
}
function load_css($filename, $dir = NULL) {
	return '<link href="'.CDN.'/'.$dir.'/css/'.$filename.'" rel="stylesheet">';
}
function load_js($filename, $dir = NULL) {
	return '<script src="'.CDN.'/'.$dir.'/js/'.$filename.'"></script>';
}
if (!function_exists('check_permissions'))
{
    function check_permissions($permissions)
    {
		$auth = new \App\Modules\Admins\Libraries\Auth();
		$return = [];
		if(!is_array($permissions)) $permissions = [$permissions];
        foreach($permissions as $permission)
        {
			$value = $auth->check_permission('admin/'.$permission);
            if($value) $return[$permission] = $value;
        }
		return $return;
    }

}
if (!function_exists('convert_time'))
{
	/* Input format: d/m/Y */
    function convert_time($datetime=0, $current_format="d/m/Y")
    {
		$datetime = str_replace("-", "/", $datetime);
		$hour = '';
		if(strpos($datetime, ' ')) {
			$exploder = explode(" ", $datetime);
			$datetime = $exploder[0];
			$hour = $exploder[1];
		}
		if(strpos($datetime, '/')) {
			if($current_format == "d/m/Y") {
				$date = explode("/", $datetime);
				$_date = $date[1].'/'.$date[0].'/'.$date[2];
				if($hour) $_date .= ' '.$hour;
				return strtotime($_date);
			} else return strtotime($datetime);
		} return null;
    }
}
if (!function_exists('gen_asset'))
{

    /**
     * Get the include code for assets: CSS or JavaScript.
     *
     * @param $type string Type of library, CSS, JS or Custom.
     * @param $filename string File name to be included.
     * @return String
     */
    function gen_asset($type, $filename)
    {
        switch ($type)
        {
            case 'css':
                return "<link href=\"" . base_url(CDN.'/assets/css/' . $filename) . "\" rel=\"stylesheet\">\n";
                break;
            case 'js':
                return "<script src=\"" . base_url(CDN.'/assets/js/' . $filename) . "\" type=\"text/javascript\"></script>\n";
                break;
            case 'custom':
                return $filename;
                break;
        }
    }

}


if (!function_exists('get_meta'))
{

    /**
     * Return a full Meta-Tag to header of the site.
     *
     * @return mixed
     */
    function get_meta()
    {
        $CI = & get_instance();
        return $CI->cms->get_meta();
    }

}

if (!function_exists('gen_link'))
{

    /**
     * This produces a link based on the string $var
     *
     * @return string
     */
    function gen_link($var)
    {
        return strtolower(url_title(convert_accented_characters($var)));
    }

}

if (!function_exists('active_link'))
{

    /**
     * Return class="active" to bootstrap menu.
     *
     * @param $link mixed Link param to check, It could be string or array.
     * @param $segment integer Segment of the URL to be checked, default 2 to Control Panel.
     * @param $return string Code returned case is active, default: class="active".
     * @return string
     */
    function active_link($link, $check_sub = false, $return = 'active', $segment = 2)
    {
        $uri = service('uri');
        $uri->setSilent();
        if(empty($segment)) $segment = 2;

        if (is_array($link)) {
            $var = $uri->getSegment($segment, '');
            if (in_array($var, $link)) {
                if($check_sub) {
                    $sub = $uri->getSegment($segment+$i, '');
                    if(!empty($sub)) $return = '';
                }
                return $return;
            } else {
                return '';
            }
        } else {
            if(strpos($link, '/')) {
                $links = explode('/', $link);
                $i = 0;
                foreach($links as $item) {
                    $var = $uri->getSegment($segment+$i, '');
                    $i++;
                    if($var != $item) {
                        $return = '';
                        break;
                    }
                }
                if($check_sub) {
                    $sub = $uri->getSegment($segment+$i, '');
                    if(!empty($sub)) $return = '';
                }
                return $return;
            } else {
                $var = $uri->getSegment($segment, '');
                if($var == $link) {
                    if($check_sub) {
                        $sub = $uri->getSegment($segment+1, '');
                        if(!empty($sub)) $return = '';
                    }
                    return $return;
                } else {
                    return '';
                }
            }
        }
    }

}

if (!function_exists('status_post'))
{

    /**
     * Return a bootstrap label tag according to the post status.
     *
     * @param $status int Post status.
     * @return string
     * */
    function status_post($status)
    {
        if ($status == '1')
            return '<span class="font-weight-bold text-success">'.lang('Published').'</span>';
        else
            return '<span class="font-weight-bold text-danger">'.lang('Pending').'</span>';
    }

}
if (!function_exists('status'))
{
    function status($status)
    {
        if ($status == '1')
            return '<span class="font-weight-bold text-success">'.lang('Actived').'</span>';
        else
            return '<span class="font-weight-bold text-danger">'.lang('Inactived').'</span>';
    }

}
if(!function_exists('show_type'))
{
    function show_type($type)
    {
        if($type == '-1')
            $html = '<span class="font-weight-bold"><i class="fas fa-file-alt"></i> Page</span>';
        elseif($type == '2')
            $html = '<span class="font-weight-bold"><i class="fas fa-video"></i> Video</span>';
        else
            $html = '<span class="font-weight-bold"><i class="fas fa-newspaper"></i> News</span>';
		return $html;
    }

}
if(!function_exists('update_status'))
{
    function update_status($status, $item_id, $options=['Inactived','Actived'])
    {
        if($status == '1')
            $html = '<a href="javascript:;" class="font-weight-bold text-success" onclick="update(\'status\', '.$item_id.', this)">'.lang($options[1]).'</a>';
        else
            $html = '<a href="javascript:;" class="font-weight-bold text-danger" onclick="update(\'status\', '.$item_id.', this)">'.lang($options[0]).'</a>';
		return $html;
    }

}
if(!function_exists('update_feature'))
{
    function update_feature($status, $item_id)
    {
        if($status == '1')
            $html = '<a href="javascript:;" class="font-weight-bold text-danger" onclick="update(\'hot\', '.$item_id.', this)">YES</a>';
        else
            $html = '<a href="javascript:;" class="font-weight-bold" onclick="update(\'hot\', '.$item_id.', this)">NO</a>';
		return $html;
    }
}
if(!function_exists('update_slider'))
{
    function update_slider($status, $item_id)
    {
        if($status == '1')
            $html = '<a href="javascript:;" class="font-weight-bold text-danger" onclick="update(\'slider\', '.$item_id.', this)">YES</a>';
        else
            $html = '<a href="javascript:;" class="font-weight-bold" onclick="update(\'slider\', '.$item_id.', this)">NO</a>';
		return $html;
    }
}
if(!function_exists('update_recommend'))
{
    function update_recommend($status, $item_id)
    {
        if($status == '1')
            $html = '<a href="javascript:;" class="font-weight-bold text-danger" onclick="update(\'recommend\', '.$item_id.', this)">YES</a>';
        else
            $html = '<a href="javascript:;" class="font-weight-bold" onclick="update(\'recommend\', '.$item_id.', this)">NO</a>';
		return $html;
    }
}
if(!function_exists('check_images'))
{
    function check_images($item, $return = 'html')
    {
        if(is_object($item)) $item = (array) $item;
        if(!empty($item['id'])) {
            $good = true;
            if(cms_config('cdn8')) $host = 'cdn8.net';
            else $host = $_SERVER['SERVER_NAME'];
            if(!empty($item['thumb']) && !strpos($item['thumb'], $host)) $good = false;
            if(!empty($item['content'])) {
                preg_match_all('/<img(.*)src=\"(.*)\"/U', $item['content'], $matches);
                if(!empty($matches[2])) {
                    foreach($matches[2] as $image) {
                        if(!strpos($image, $host)) {
                            $good = false;
                            break;
                        }
                    }
                }
            }
            if($good) {
                if($return == 'html') $_return = '<span class="font-weight-bold text-danger"><i class="fas fa-check"></i></span>';
                else $_return = true;
            }
            else {
                if($return == 'html') $_return = '<a href="javascript:;" class="font-weight-bold" onclick="update(\'hosted_images\', '.$item['id'].', this)"><i class="fas fa-times"></i></a>';
                else $_return = false;
            }
        } else {
            if($return == 'html') $_return = '<a href="javascript:;" class="font-weight-bold">-</a>';
            else $_return = false;
        }
		return $_return;
    }
}
if (!function_exists('show_avatar'))
{
    function show_avatar($avatar, $width=0, $height=0)
    {
        if($avatar) {
			if($width && $height)
				$avatar = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $avatar);
            return $avatar;
		}
        else return CDN."/images/no-avatar.jpg";
    }
}

function post_url($item) {
	if(is_array($item) && !empty($item['slug'])) $slug = $item['slug'];
	elseif(!empty($item->slug)) $slug = $item->slug;
	if(!empty($slug))
		return URL.'/'.strtolower($slug);
	elseif(is_string($item))
		return URL.'/'.strtolower($item);
	else return '#';
}
function category_url($item) {
	if(isset($item->slug))
		return URL.'/'.strtolower($item->slug);
	elseif(is_string($item))
		return URL.'/'.strtolower($item);
	else return '#';
}
function tag_url($item) {
	if(isset($item->slug))
		return URL.'/tag/'.strtolower($item->slug);
	elseif(is_string($item))
		return URL.'/tag/'.strtolower($item);
	else return '#';
}

if (!function_exists('show_thumb'))
{
	function show_thumb($thumb, $width=null, $height=null) {
		if($thumb) {
			$image_cdn = cms_config('image_cdn');
			if($width && $height) {
				$thumb = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $thumb);
			}
			if($image_cdn == 'WeServ') {
				$thumb = "https://images.weserv.nl/?url=".urlencode($thumb);
				if($width && $height)
					$thumb .= '&w='.$width.'&h='.$height.'&t=square&a=top';
			}
			elseif($image_cdn == 'Google') {
				$thumb = "https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&url=".urlencode($thumb);
				if($width && $height)
					$thumb .= '&resize_w='.$width.'&resize_h='.$height;
			}
			elseif($image_cdn == 'Statically') {
				$thumb = str_replace('http://', '', $thumb);
				$thumb = str_replace('https://', '', $thumb);
				$thumb = "https://cdn.statically.io/img/".$thumb;
			}
			return $thumb;
		}
		else return CDN.'/images/no-thumb.png';
	}
}
if (!function_exists('cdn_url'))
{
	function cdn_url() {
		$image_cdn = cms_config('image_cdn');
		$cdn = NULL;
		if($image_cdn == 'WeServ') {
			$cdn = "https://images.weserv.nl/?url=";
		}
		elseif($image_cdn == 'Google') {
			$cdn = "https://images2-focus-opensocial.googleusercontent.com/gadgets/proxy?container=focus&gadget=a&no_expand=1&url=";
		}
		elseif($image_cdn == 'Statically') {
			$cdn = "https://cdn.statically.io/img/";
		}
		return $cdn;
	}
}
function get_thumb($post, $width=null, $height=null) {
	if(is_object($post)) $post = (array)$post;
    if(isset($post['thumb']) && isset($post['content'])) {
        $thumb = $post['thumb'];
        if(!strpos($thumb, 'ttp')) {
            $youtube = getYoutubeID(clearString($post['content']));
            if($youtube) {
				$thumb = 'http://img.youtube.com/vi/' . $youtube . '/maxresdefault.jpg';
				if(!@file_get_contents($thumb)) $thumb = 'http://img.youtube.com/vi/' . $youtube . '/0.jpg';
			}
			elseif(strpos($post['content'], 'streamable.com')) {
				$streamable = getStreamableID($post['content']);
				$json = @file_get_contents('https://api.streamable.com/videos/'.$streamable);
				if($json) $json = json_decode($json);
				if(isset($json->thumbnail_url)) $thumb = 'http:'.$json->thumbnail_url;
			}
        }
        if (!strpos($thumb, 'ttp')) {
            $img = getString($post['content'], '<img', '>');
            $img = str_replace("'", '"', $img);
            $thumb = getString($img, 'src="', '"');
        }
        if($width && $height) {
			if(strpos($thumb, 'blogspot.com'))
				$thumb = str_replace('/s0/', '/w'.$width.'-h'.$height.'-c/', $thumb);
			if(strpos($thumb, 'images.weserv.nl')) {
				$parts = parse_url($thumb);
				parse_str($parts['query'], $query);
				if(isset($query['url'])) {
					$thumb = 'https://images.weserv.nl/?url='.$query['url'].'&w='.$width.'&h='.$height.'&t=square&a=top';
				}
			}
		}
        if ($thumb) return $thumb;
        else return CDN . '/images/no-thumb.png';
    } return false;
}
if (!function_exists('status_notification'))
{

    /**
     * Return a bootstrap label tag according to the notification status.
     *
     * @param $status int Notification status.
     * @return string
     * */
    function status_notification($status)
    {
        if ($status == '1')
            return '<span class="label label-success">Read</span>';
        else
            return '<span class="label label-info">Not read</span>';
    }

}

if (!function_exists('yes_no'))
{

    /**
     * Return a bootstrap label tag according to the user status.
     *
     * @author Joker
     * @param $status int - User status
     * @return string
     * */
    function yes_no($status)
    {
        if ($status == '1')
            return '<span class="label label-success">Yes</span>';
        else
            return '<span class="label label-danger">No</span>';
    }

}

if (!function_exists('status_user'))
{

    /**
     * Return a bootstrap label tag according to the user status.
     *
     * @author Joker
     * @param $status int - User status
     * @return string
     * */
    function status_user($status)
    {
        if ($status == '1')
            return '<span class="label label-success">Active</span>';
        else
            return '<span class="label label-danger">Blocked</span>';
    }

}

if (!function_exists('formata_money'))
{

    /**
     * Format numbers at monetary values.
     *
     * @param $var double Valor a ser convertido.
     * @param $format string Formato do retorno.
     * @return mixed
     */
    function formata_money($var, $format = 'vn')
    {
        if ($format == 'vn')
            return number_format($var, 2, ',', '.');
        else if ($format == 'us')
            return str_replace(',', '.', str_replace('.', '', $var));
    }

}

if(!function_exists('get_ip'))
{
	function get_ip()
	{
		if (isset($_SERVER['HTTP_CLIENT_IP']) == true) {
			$IPAddress = $_SERVER['HTTP_CLIENT_IP'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) == true) {
			$IPAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_X_FORWARDED']) == true) {
			$IPAddress = $_SERVER['HTTP_X_FORWARDED'];
		} else if (isset($_SERVER['HTTP_FORWARDED_FOR']) == true) {
			$IPAddress = $_SERVER['HTTP_FORWARDED_FOR'];
		} else if (isset($_SERVER['HTTP_FORWARDED']) == true) {
			$IPAddress = $_SERVER['HTTP_FORWARDED'];
		} else if (isset($_SERVER['REMOTE_ADDR']) == true) {
			$IPAddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$IPAddress = 'UNKNOWN';
		}
		return $IPAddress;
	}
}
if(!function_exists('convert_date'))
{
	function convert_date($date, $extra_time=0, $format="datetime")
	{
		$date = str_replace('/', '-', urldecode($date));
		$temp = explode('-', $date);
		if(count($temp)>2)
			$return = strtotime($temp[1].'/'.$temp[0].'/'.$temp[2]);
		else return false;
		
		if($return == 0) return false;
		$return += $extra_time;
		if($format=='unix') {
			return $return;
		}
		else return date("Y-m-d G:i:s", $return);
	}
}
if(!function_exists('show_date'))
{
	function show_date($timestamp, $content=true)
	{
        $now = time();
		if($content && abs($now - $timestamp) < 60) {
			return 'Mới đăng';
		}
		elseif($content && abs($now - $timestamp) < 3600) {
			$min = round(abs($now - $timestamp) / 60);
			if($min < 1) $min = 1;
			return $min.' phút';
		}
		elseif($content && abs($now - $timestamp) < 24*3600) {
			$hour = round(abs($now - $timestamp) / 3600);
			if($hour < 1) $hour = 1;
			return $hour.' giờ';
		}
		elseif($content && abs($now - $timestamp) < 7*24*3600) {
			$days = round(abs($now - $timestamp) / 86400);
			if($days < 1) $days = 1;
			return $days.' ngày';
		} else {
			$date_format = cms_config('date_format');
			if(!$date_format) $date_format = 'd/m/Y';
			return date($date_format, $timestamp);
		}
	}
}

// Define the custom sort function
function order_sort($a,$b) {
	return $a->order > $b->order;
}


function getString($string, $start, $end=null) {
    $string = " " . $string;
    $ini = strpos($string, $start);
    if ($ini == 0)
        return "";
    $ini += strlen($start);
	$new_string = substr($string, $ini);
	if($end) {
		$len = strpos($new_string, $end);
		return substr($new_string, 0, $len);
	}
	else return $new_string;
}
function clearString($string, $quot = true) {
    $string = str_replace('\'', "'", $string);
    $string = str_replace("\'", "'", $string);
    $string = str_replace('\"', '"', $string);
    $string = str_replace('\r\n', "", $string);
    $string = str_replace('\n', "", $string);
    if($quot) {
        $string = str_replace('"', "&quot;", $string);
        $string = str_replace("'", "&#039;", $string);
    }
    return $string;
}
function clear_utf8($string, $link = true, $lower = true) {
	$string = urldecode($string);
    $chars = array('a' => array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă'),'A' => array('Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),'e' => array('ế', 'ề', 'ể', 'ễ', 'ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê'),'E' => array('Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),'i' => array('í', 'ì', 'ỉ', 'ĩ', 'ị'),'I' => array('Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),'o' => array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ'),'O' => array('Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),'u' => array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư'),'U' => array('Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),'y' => array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ'),'Y' => array('Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),'d' => array('đ'),'D' => array('Đ'));
    foreach ($chars as $key => $arr)
        foreach ($arr as $val)
            $string = str_replace($val, $key, $string);
    if($link) {
		$string = str_replace(' ', '-', $string);
		$string = str_replace('/', '-', $string);
	}
    $string = preg_replace('/[^A-Za-z0-9 \-]/', '', $string);
    $string = str_replace(',', '', $string);
    $string = trim($string);
    if($lower) $string = strtolower($string);
    if($link)
        while (strstr($string, '--'))
            $string = str_replace('--', '-', $string);
    return $string;
}
function category_checkbox($data, $current_categories = []) {
	$code = '<ul class="category-list">';
	if(is_object($data) && count((array)$data))
	foreach ($data as $item) {
		$checked = '';
		if(in_array($item->id, $current_categories)) {
			$checked = 'checked';
		}
		$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
		$code .= '<input type="checkbox" '.$checked.' id="cateory_'.$item->id.'" name="categories[]" value="'.$item->id.'" /><label for="cateory_'.$item->id.'">&nbsp;'.clearString($item->name, false).'</label>';
		if(is_object($item->child) && count((array)$item->child)) $code .= category_checkbox($item->child, $current_categories);
		$code .= '</li>';
	}
	$code .= '</ul>';
	return $code;
}
function category_list($data) {
	$code = '<ol class="dd-list">';
	if(is_object($data) && count((array)$data))
	foreach ($data as $item) {
		$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
			$code .= '<div class="dd-handle">'.clearString($item->name, false).'</div>';
			$code .= '<div style="display: inline-block;position: absolute;right: 5px;top: 5px;">';
			$code .= '<span style="margin-right: 5px;font-size: 11px;">'.number_format($item->count,0,',','.').' '.lang('Posts').'</span>';
			$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" ><i class="fa fa-edit"></i></a>';
			$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
			if(is_object($item->child) && count((array)$item->child)) $code .= category_list($item->child);
		$code .= '</li>';
	}
	$code .= '</ol>';
	return $code;
}
function category_dropbox($data, $level=0) {
	if($level == 0) $code = '<option value="0">No Parent</option>';
	else $code = '';
	if(is_object($data) && count((array)$data))
	foreach ($data as $item) {
		$space = '';
		if($level) for($i=0;$i<$level;$i++) $space .= '-- ';
		$code .= '<option value="'.$item->id.'">'.$space.$item->name.'</option>';
		if(is_object($item->child) && count((array)$item->child)) $code .= category_dropbox($item->child, $level+1);
	}
	return $code;
}
function menu_dropbox($data, $level=0) {
	if($level == 0) $code = '<option value="0">No Parent</option>';
	else $code = '';
	if(is_object($data) && count((array)$data))
	foreach ($data as $item) {
		$space = '';
		if($level) for($i=0;$i<$level;$i++) $space .= '-- ';
		$code .= '<option value="'.$item->id.'">'.$space.$item->title.'</option>';
		if(is_object($item->child) && count((array)$item->child)) $code .= menu_dropbox($item->child, $level+1);
	}
	return $code;
}
function rebuild_url($remove = 0) {
	$uri_string = uri_string();
	if(strpos($uri_string, '/')) $segments = explode('/', $uri_string);
	else $segments = [$uri_string];
	for($i=0; $i<$remove; $i++) {
		array_pop($segments);
	}
	$admin_url = site_url($segments);
	return $admin_url;
}

function getYoutubeID($markup) {
    $markup = str_replace(" ", "\n", $markup);
    preg_match('#<object[^>]+>.+?https?://www\.youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+).+?</object>#s', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#https?://www\.youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#https?://www\.youtube(?:\-nocookie)?\.com/embed/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#(?:https?(?:a|vh?)?://)?(?:www\.)?youtube(?:\-nocookie)?\.com/watch\?.*v=([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#(?:https?(?:a|vh?)?://)?youtu\.be/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#youtube(?:\-nocookie)?\.com/[ve]/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]))
        preg_match('#youtube(?:\-nocookie)?\.com/embed/([A-Za-z0-9\-_]+)#', $markup, $matches);
    if (!isset($matches[1]) && function_exists('lyte_parse'))
        preg_match('#<div class="lyte" id="([A-Za-z0-9\-_]+)"#', $markup, $matches);
    if (isset($matches[1]))
        return trim($matches[1]);
    else
        return false;
}
function getStreamableID($markup) {
	$markup = strip_tags($markup);
    $markup = str_replace(" ", "\n", $markup);
    preg_match('#https://streamable.com/(.*)#s', $markup, $matches);
    if (isset($matches[1])) return trim($matches[1]);
    else return false;
}
function cutOf($string, $len, $type = false) {
    if (strlen($string) > $len) {
        if ($type == false) {
            $t1 = substr($string, 0, $len);
            $t2 = substr($string, $len, strlen($string));
            $t2 = str_replace(strstr($t2, " "), "...", $t2);
            return $t1 . $t2;
        } else {
            $t1 = substr($string, 0, $len);
            return $t1 . "...";
        }
    }
    else
        return $string;
}
function pr($output) {
	print_r('<pre>');
	print_r($output);
	print_r('</pre>');
}
function show_img($img, $width, $height, $title='') {
	if(cms_config('lazy_load')) {
		return '<img alt="'.$title.'" src="'.CDN.'/images/blank.gif" class="lazy" data-src="'.show_thumb($img, $width, $height).'" />';
	} else {
		return '<img alt="'.$title.'" src="'.show_thumb($img, $width, $height).'" />';
	}
}
function toggle($name, $value, $id=NULL) {
	$checked = ($value)?'checked':'';
	$value = ($id)?$id:1;
	return '
	<label class="form-toggle custom-toggle custom-toggle-danger">
		<input type="checkbox" class="'.$name.'" id="'.$name.'-'.$id.'" name="'.$name.'" '.$checked.' value="'.$value.'">
		<span class="custom-toggle-slider rounded-circle" data-label-off="No" data-label-on="Yes"></span>
	</label>
	';
}
function curl($url, $post = false, $cookie = false, $header = false, $socks5 = false, $auth = false, $head = 1) {
    $curl = ($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, 'User-Agent: Mozilla/5.0 (Windows NT 6.3; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0');
    if ($post != "") {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    }
    if (is_array($header))
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    if ($auth != "")
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
    if ($socks5 != "") {
        curl_setopt($ch, CURLOPT_PROXY, $socks5);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
    }
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 30);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
    curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
    if ($head) curl_setopt($ch, CURLOPT_HEADER, 1);
    //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 3);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function private_download() {
	if(isset($_SERVER['HTTP_REFERER'])) {
		$ref = $_SERVER['HTTP_REFERER'];
		if(strstr($ref, "google.com")){
			return true;
		}
	}
	return false;
}
// HTML Minifier
function minify_html($input) {
    return $input;
    if(trim($input) === "") return $input;
    // Remove extra white-space(s) between HTML attribute(s)
    $input = preg_replace_callback('#<([^\/\s<>!]+)(?:\s+([^<>]*?)\s*|\s*)(\/?)>#s', function($matches) {
        return '<' . $matches[1] . preg_replace('#([^\s=]+)(\=([\'"]?)(.*?)\3)?(\s+|$)#s', ' $1$2', $matches[2]) . $matches[3] . '>';
    }, str_replace("\r", "", $input));
    // Minify inline CSS declaration(s)
    if(strpos($input, ' style=') !== false) {
        $input = preg_replace_callback('#<([^<]+?)\s+style=([\'"])(.*?)\2(?=[\/\s>])#s', function($matches) {
            return '<' . $matches[1] . ' style=' . $matches[2] . minify_css($matches[3]) . $matches[2];
        }, $input);
    }
    if(strpos($input, '</style>') !== false) {
      $input = preg_replace_callback('#<style(.*?)>(.*?)</style>#is', function($matches) {
        return '<style' . $matches[1] .'>'. minify_css($matches[2]) . '</style>';
      }, $input);
    }
    if(strpos($input, '</script>') !== false) {
      $input = preg_replace_callback('#<script(.*?)>(.*?)</script>#is', function($matches) {
        return '<script' . $matches[1] .'>'. minify_js($matches[2]) . '</script>';
      }, $input);
    }

    return preg_replace(
        array(
            // t = text
            // o = tag open
            // c = tag close
            // Keep important white-space(s) after self-closing HTML tag(s)
            '#<(img|input)(>| .*?>)#s',
            // Remove a line break and two or more white-space(s) between tag(s)
            '#(<!--.*?-->)|(>)(?:\n*|\s{2,})(<)|^\s*|\s*$#s',
            '#(<!--.*?-->)|(?<!\>)\s+(<\/.*?>)|(<[^\/]*?>)\s+(?!\<)#s', // t+c || o+t
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<[^\/]*?>)|(<\/.*?>)\s+(<\/.*?>)#s', // o+o || c+c
            '#(<!--.*?-->)|(<\/.*?>)\s+(\s)(?!\<)|(?<!\>)\s+(\s)(<[^\/]*?\/?>)|(<[^\/]*?\/?>)\s+(\s)(?!\<)#s', // c+t || t+o || o+t -- separated by long white-space(s)
            '#(<!--.*?-->)|(<[^\/]*?>)\s+(<\/.*?>)#s', // empty tag
            '#<(img|input)(>| .*?>)<\/\1>#s', // reset previous fix
            '#(&nbsp;)&nbsp;(?![<\s])#', // clean up ...
            '#(?<=\>)(&nbsp;)(?=\<)#', // --ibid
            // Remove HTML comment(s) except IE comment(s)
            '#\s*<!--(?!\[if\s).*?-->\s*|(?<!\>)\n+(?=\<[^!])#s'
        ),
        array(
            '<$1$2</$1>',
            '$1$2$3',
            '$1$2$3',
            '$1$2$3$4$5',
            '$1$2$3$4$5$6$7',
            '$1$2$3',
            '<$1$2',
            '$1 ',
            '$1',
            ""
        ),
    $input);
}

// CSS Minifier => http://ideone.com/Q5USEF + improvement(s)
function minify_css($input) {
    if(trim($input) === "") return $input;
    return preg_replace(
        array(
            // Remove comment(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')|\/\*(?!\!)(?>.*?\*\/)|^\s*|\s*$#s',
            // Remove unused white-space(s)
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/))|\s*+;\s*+(})\s*+|\s*+([*$~^|]?+=|[{};,>~]|\s(?![0-9\.])|!important\b)\s*+|([[(:])\s++|\s++([])])|\s++(:)\s*+(?!(?>[^{}"\']++|"(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')*+{)|^\s++|\s++\z|(\s)\s+#si',
            // Replace `0(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)` with `0`
            '#(?<=[\s:])(0)(cm|em|ex|in|mm|pc|pt|px|vh|vw|%)#si',
            // Replace `:0 0 0 0` with `:0`
            '#:(0\s+0|0\s+0\s+0\s+0)(?=[;\}]|\!important)#i',
            // Replace `background-position:0` with `background-position:0 0`
            '#(background-position):0(?=[;\}])#si',
            // Replace `0.6` with `.6`, but only when preceded by `:`, `,`, `-` or a white-space
            '#(?<=[\s:,\-])0+\.(\d+)#s',
            // Minify string value
            '#(\/\*(?>.*?\*\/))|(?<!content\:)([\'"])([a-z_][a-z0-9\-_]*?)\2(?=[\s\{\}\];,])#si',
            '#(\/\*(?>.*?\*\/))|(\burl\()([\'"])([^\s]+?)\3(\))#si',
            // Minify HEX color code
            '#(?<=[\s:,\-]\#)([a-f0-6]+)\1([a-f0-6]+)\2([a-f0-6]+)\3#i',
            // Replace `(border|outline):none` with `(border|outline):0`
            '#(?<=[\{;])(border|outline):none(?=[;\}\!])#',
            // Remove empty selector(s)
            '#(\/\*(?>.*?\*\/))|(^|[\{\}])(?:[^\s\{\}]+)\{\}#s'
        ),
        array(
            '$1',
            '$1$2$3$4$5$6$7',
            '$1',
            ':0',
            '$1:0 0',
            '.$1',
            '$1$3',
            '$1$2$4$5',
            '$1$2$3',
            '$1:0',
            '$1$2'
        ),
    $input);
}

// JavaScript Minifier
function minify_js($input) {
    if(trim($input) === "") return $input;
    return preg_replace(
        array(
            // Remove comment(s)
            '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
            // Remove white-space(s) outside the string and regex
            '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
            // Remove the last semicolon
            '#;+\}#',
            // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
            '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
            // --ibid. From `foo['bar']` to `foo.bar`
            '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
        ),
        array(
            '$1',
            '$1$2',
            '}',
            '$1$3',
            '$1.$3'
        ),
    $input);
}
// function auto_index($url) {
// 	require_once APPPATH.'ThirdParty/Google-API/vendor/autoload.php';
// 	$client = new Google_Client();
// 	$key = decrypt('EwjpgDfjrjNzYbwGGyGCxeUnGQUh/v90U9kc/sWIaMhy/SLK4XUOmeqlbHxPL+uT7fL0qKDb/1RVvBaAw4lXBAN1jykxdtM7G9yv74wd1cn/oGoBSidwieOCpnZW6KNT2j46/mcQptYaY2BPxYqJx3Aq3U5iwhehQVVJPBdwcJORuNPynG9F2299ljlDLXhZhyeX6EKMIjS/az51i4plp/fOAPH0VqhLY32cbTB1sA4nW09/nsuXfnf8P56/u8JHXf+6JAbKPweGILE0YE9d67tBe6Qlw35+HrBADW2S1Ph/CYjPK30VXwFWbXNhalyLd68tpxAgHtOGYAq49nyuWLQs/7Egzvum00Em5cPQWeH9JmFsWmMJ9LKkv8PD29qrOzX7xgxd6Q1DYWqmf+Rfi/pqR187VhmYXmDceqmmud1e0QIZlAM3j64/gFR6ymv/gWNNlilGzZr4RB8vToq4vbSa9XKkV1eVIHaxluaSvAR/QrXKcIpVCoc/JJ4jAqSwqf+8kci4QaJBOXoU6K8iVD6oT45GLyo/nLGY0HClzrwRvI/RW3N1x3N/BAJkU3/ACYdYmNgHgiMjqDVoqdLqKkKsioXmBVtoCha/jOVHE5HOsd0adoqkFBpTyzAY8jRtzkcpJokGGHlcLHr4q+LA3gQN/HFdlefxERsXYuc5RvIFWlhZYr99HWxaOzu09PRqjuaMzEg5AohuHJ+vAFJG4q0LvRsprfDHq07HUQR0W1UqkbDNDC+ZUeSCDX8SU0/T9oUPKNPlQNUWC+vW/iY/L3uxZzUafMBk8LPjAedl5ZEjXY/UJ1ASogEbQHARAh7BSWUetOeKHhMsh3RX+n17A1NIqj2ZpV3FdVAHsIIPb8Rt2beRrsOyZB8b6dXLXRBDcsdjKLz6tbstgxLz+70oHePlvsNJQucVF2sMX1zMUJRmlEUdYiz2HN/MU7QEAnY2MOm0aVxnCc7uyf4AdvEI2IMJyeIP6/tfF4j60oCC5gNuKLu/9cQsC8/snMJEfJ+/OgRqEfg57E4JpxMsmUrwwTrrFoTnzykBbfFXKdS86w9WZvgD8F9trozcz/GRE/+YcPf2eFq4D8/ch4mesTh+vtF6E2GQ6l3ORTYiUrUyEwVfHKVAS0BnNZ9dnwakl1UW6UzykBUG1dEd8BHNS0p6jTACbCyZ5PPcvazgVfSN/Xvxt0sJUyG67ZHDAouUKsTYsHgSg6RcFh2BsEAK7JP+2PC772IpKQ5pPckMWXShgC+IvQ4b8mReF9Q2YOk8d51tqqp8hjh2bg2ArB81L9HCfH1xs+OKiD6tJ3Yu+Bp/oYWWeZsBksC0Hacf7hc3Qq2Wh2QRs/I370rPItLNrI6XVJKT4K8AraMX973roVfG5eHva1PmFFirWbUDKlbn9K74CA+8pdILJByANxo1kNHVJhOqDo90BsPUX2Rr1yzxZy+9uCnKYvjzrlC1stG5JHAsjKXaVLORJVSvN1lbMvXpz5P06Z0gekDj+8FwgeR2NGMWj0dZ/KjvbArFH73Oo+8x9gjzpY98jq/bPeIG3J0h89B6GENBb9/pqGG7tiUj7nRPyVIKc8+A7UkwH9aB0M6qAZW3qmrdn9rTLaOROR0K+Ze00gyltY7mij2h+G/17YHbH0tG8JBQb7VlA+BQikQo7X1fPNwEZDHYhBC8ViWxe8qDeyRTklazPqpPjs0BszgqjL1X3yGCbd+3beMPm38hgMJ5XcpsFoDkv2l51eaP+SRnc/j2bwC/CGiYq2cBy+S05Yg9B+J3Q9UOiDG4UkqM0Fw+1nUdIxtLGEwpnV6ePMF31Di/25zwZQrghvndNjXhdelFZ7X1nIXpZJ1ZDjHQ+gcpMB9Lb35V4U9Mn++KLa5VWnm4d0exAzltc4rirDAJzkVPptClKkT+tquVA5xkfq/elbPMDjquvrL5DxJ9nM8d0K6g7fwa+qHn7fcPgFsMml6Ql4IEc/VNHZaRZI6DgF+u63MtAMjPkdreF/asUGBpvmgJDD3hJyIxJGY5y43Ocsmdcoa+Blvv3gn4My6hm9oU7zOZ3G+v/BwyPtsTfksliluiaLwWkOmDkAlp3ddb529jLRKjZ73pDM02I9kL3xo8CYfyGWHHGMYfvwGJ0igJkAt6rSn3pi0lDkO/1/RibdqCDWrndF0nvHxE8WzbaBOerH77A/vRoIPQKQeVWTBtG+Z7Zkek//c07UeQ1EEQwqopqcTriY1mFa/AG74sqYPGopdHbaMRzbkish+DLeZuUM74ViU8J9WpN1+uhpEbFWtlFWhDYCwfrhZpcUJHmDfAHmcqw48wSgPPdk0tPpruuwhSTwhqzFzkOa4D0KoISvobXd16LqW+LEJ4w/WlHsttHqigb5AH+buA3evy1N2hQ+Pic2b1/UB+MeecQt/UIT4pBtuN5R/bvyY5tj7Cp9xCV1d422ABhGRC8dTRe7TO5k9YyX+i0f2Blzs3SD0C73VS5BTD7WpGKJWh+f9qe91TxizKOUSbh7k8ko/AAFJkShWvySfJwOLtmN4SSMFViiuWi5LpTlW7eUip6PBaVySaifem/Pft5XOFIRPD3UElFoZeq/7/AQoQJ/gMpVg+VXFLSsaEf+rr7mVIgzGG+X49bQN6dU2ZzdlTXonL70wS5YN+qvwUAzYjYIuph4q1gUcK0lChuUoJKYXT2XiTI6ZH0i8KbJ+myX+7QCp3S/KRIaoPXPohOtKj4EXyWyFWSfXmk1ASHXUs32EmpHmuwdEvfg2dCQvoNs5SAieCj1jYoAVVAPbvGLZi38eyb6FabPF98vPoXJcmWWICbGjXNZi9Fg+pEoi80l3bB7C+DJu94B//9zOrMN12mAQQgjyKJcIT+CHjzuxAZ07hmgWfV69PrJDZgZl0dKP/TT2Guq8uZv46DHIkXkgd53bRTPbRi0EDNfoyF5utphK4s08bYRIyI7BJRL5pTJKHDBOkhRkTuW3tdGwh0lqRp/RFV8uGKvpIkpQeXxOq+RcGlkFiWSrPU19ReeuR/mxZXINXyqMBTGCSx30wMDn3T/VDJ+HKVzzNSmu3FKZOxMpZFpn2qlJKxakKUmHdIHhgHQTPhFPNf+BfmmUJd6w/V+CjNw==', 'Tmnfs3hfUd2GF5kW');
// 	$config = json_decode($key, true);
// 	$client->useApplicationDefaultCredentials();
// 	// set the information from the config
// 	$client->setClientId($config['client_id']);
// 	$client->setConfig('client_email', $config['client_email']);
// 	$client->setConfig('signing_key', $config['private_key']);
// 	$client->setConfig('signing_algorithm', 'HS256');

// 	$client->setAccessType('offline');
// 	$client->addScope('https://www.googleapis.com/auth/indexing');

// 	// Get a Guzzle HTTP Client
// 	$httpClient = $client->authorize();
// 	$endpoint = 'https://indexing.googleapis.com/v3/urlNotifications:publish';

// 	// Define contents here. The structure of the content is described in the next step.
// 	$content = '{
// 		"url": "'.$url.'",
// 		"type": "URL_UPDATED"
// 	}';

// 	$response = $httpClient->post($endpoint, ['body' => $content]);
// 	$status_code = $response->getStatusCode();

// 	return (intval($status_code) == 200);
// }