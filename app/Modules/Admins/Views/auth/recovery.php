<!-- Main content -->
    <div class="main-content">
        <!-- Header -->
        <div class="header bg-gradient-primary py-8 py-lg-8 pt-lg-8">
            <div class="container">
                <div class="header-body text-center">
                    <div class="row justify-content-center">
                        <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                            <h1 class="text-white"><?=get_lang('welcome')?></h1>
                            <p class="text-lead text-white"><?=get_lang('recover_title')?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="separator separator-bottom separator-skew zindex-100">
                <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                    <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                </svg>
            </div>
        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary border-0 mb-0">
                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted mb-5">
                                <h3><?=get_lang('recover_message');?></h3>
                            </div>
                            <form method="POST" role="form" id="login-form" class="validation" novalidate>
                                <div class="form-group mb-4">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                        </div>
                                        <input class="form-control" name="email" type="email" autocomplete="off" required value="<?=$this->input->post('email');?>" placeholder="<?= get_lang('login_email'); ?>">
										<div class="invalid-feedback"><?=get_lang('email_validator');?></div>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary my-4"><?= get_lang('recover'); ?></button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-6">
                            <a href="<?=site_url('admin/login/')?>" class="text-light"><small><?=get_lang('link_login')?></small></a>
						</div>
						<!--div class="col-6 text-right">
							<a href="#" class="text-light"><small><?=get_lang('link_register')?></small></a>
						</div-->
                    </div>
                </div>
            </div>
        </div>
    </div>
