<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?= $title; ?></title>
    <?php $cms->show_asset('css'); ?>
    <!-- THEME STYLES -->
    <?=$cms->load_asset('css', 'admin.min.css'); ?>
	<!-- CORE PLUGINS -->
    <?php $cms->show_asset('js'); ?>
    <?=$cms->load_asset('js', 'admin.min.js'); ?>
</head>
<body class="bg-default">
    <?= $this->renderSection('content') ?>
    <!-- Footer -->
    <footer class="py-5" id="footer-main">
        <div class="container">
            <div class="row align-items-center justify-content-xl-between">
                <div class="col-xl-6">
                    <div class="copyright text-center text-xl-left text-muted">
                        &copy; <?=date('Y');?> <a href="https://8am.us" class="font-weight-bold ml-1" target="_blank">VCMS</a>
                    </div>
                </div>
                <div class="col-xl-6">
                    <ul class="nav nav-footer justify-content-center justify-content-xl-end">
                        <li class="nav-item">
                            <a href="<?=URL?>" class="nav-link"><?=lang('Home')?></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
	<?php
		$notice = get_message();
		if(isset($notice['message']) && $notice['message']) {
			$type = isset($notice['type'])?$notice['type']:'';
			echo notify($notice['message'], null, $type, true, [88,31], 'right');
		}
	?>
</body>
</html>