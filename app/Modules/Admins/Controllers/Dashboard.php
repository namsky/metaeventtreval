<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;

class Dashboard extends AdminController
{
	public function index()
	{
		// $result = auto_index('https://nghiencode.com/nen-lua-chon-web-server-nao-apache-hay-nginx');
		// pr($result);
		// exit;
		$this->view->setVar('title', 'Dashboard');
		/* Get report Post */
		$postModel = model('App\Modules\Cms\Models\PostModel');
		$userModel = model('App\Modules\Cms\Models\UserModel');
		$posts = [];
		$posts['this_week'] = $postModel->where('status', 1)->where('published>', time()-7*86400)->countAllResults();
		$posts['last_week'] = $postModel->where('status', 1)->where('published>', time()-14*86400)->where('published<', time()-7*86400)->countAllResults();
		$posts['view_this_week'] = $postModel->select('SUM(views) as total')->where('status', 1)->where('published>', time()-7*86400)->first()->total;
		$posts['view_last_week'] = $postModel->select('SUM(views) as total')->where('status', 1)->where('published>', time()-14*86400)->where('published<', time()-7*86400)->first()->total;
		$posts['view_this_month'] = $postModel->select('SUM(views) as total')->where('status', 1)->where('published>', time()-30*86400)->first()->total;
		$posts['view_last_month'] = $postModel->select('SUM(views) as total')->where('status', 1)->where('published>', time()-60*86400)->where('published<', time()-30*86400)->first()->total;
        $_users = $postModel->select('user_id, COUNT(*) as total')->groupBy('user_id')->orderBy('total', 'DESC')->findAll(4);
		$users = [];
		foreach($_users as $_user) {
			$user = $userModel->select('id,username,name,email,status')->find($_user->user_id);
			if(is_object($user)) {
				$user = $user->toArray();
				if(isset($user['id'])) {
					$user['total'] = $_user->total;
					$users[] = $user;
				}
			}
		}
		$this->view->setVar('users', $users);
		$this->view->setVar('posts', $posts);
		$this->view->setVar('cms', $this->cms);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'chart.js',
        ]);
		echo $this->view->render('dashboard');
	}
}