<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Access_logs extends AdminController
{
	function get_config() {
		$config = [
			'name' => 'Access_logs',
			'model' => 'App\Modules\Admins\Models\AccessLogModel',
			'datagrid_options' => [
				'orders' => ['id' => 'desc'],
			],
			'select_options' => [],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'ip_address' => [
					'name' => 'IP Address',
					'method' => 'template',
					'template' => '<span style="color:green; font-weight: bold">{$ip_address}</span>',
					'class' => 'text-center',
				],
				'os' => [
					'name' => 'OS',
					'method' => 'function',
					'function' => 'getOS|useragent',
					'column_name' => 'useragent',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'browser' => [
					'name' => 'Browser',
					'method' => 'function',
					'function' => 'getBrowser|useragent',
					'column_name' => 'useragent',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'admin_id' => [
					'name' => 'Created by',
					'method' => 'template',
					'template' => '{$admin->username}',
					'class' => 'text-center',
				],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-sm-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'with' => ['admin|id,username'],
			'rules' => [
				'required' => ['ip_address', 'useragent']
			],
			'record' => [
				'colums' => 12,
				'fields' => [
					'ip_address' => ['name' => 'IP Address'],
					'useragent' => ['name' => 'Useragent'],
				],
			],
		];
		return $config;
	}
}