<?php
namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use Config\View;
use Helper\Cms;
use App\Modules\Admins\Libraries\Auth;
use App\Core\Cms\CmsView;

class AdminController extends Controller
{
	public $view;
	protected $auth;
	protected $cms;
	protected $crud;
	protected $model;
	protected $helpers = ['cms'];
	protected $cache;
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		parent::initController($request, $response, $logger);
		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
        $this->session = session();
        $data = ['backend_ref' => base_url(uri_string())];
        $this->session->set($data);
		// Preload cms library
		$this->cms = new \App\Libraries\Cms();
		if(method_exists($this, 'get_config')) {
			$this->crud = $this->get_config();
		}
		// Preload view
		$path = dirname(dirname(__DIR__));
		$class = get_class($this);
		if(strpos($class, '\\')) {
			$_class = explode('\\', $class);
			if(count($_class) > 3) {
				$view_path = $_class[count($_class)-3];
			}
		} else $view_path = 'Admins';
		$this->view = new CmsView(config('View'), $path.DIRECTORY_SEPARATOR.$view_path.DIRECTORY_SEPARATOR.'Views');
		$layout = dirname(__DIR__).DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.'main.php';
		$this->view->extend($layout);
		
		/* Auth */
		$this->auth = new Auth();
        if($this->auth->is_logged() == TRUE) {
			$this->view->setVar('account', $this->auth->get_account());
            // if(!$this->auth->is_root()) {
                // if($this->auth->is_user())
                    // $this->set_message('This area is intended for admin users only.', 'danger', '');
                $permission = $this->auth->check_permission();
				if(!$permission) {
					$uri = uri_string();
					if($uri != 'v-manager/access_denied') {
						$login_url = site_url('v-manager/access_denied');
						return cms_redirect($login_url);
					}
				}
            // }
        } else {
			$login_url = site_url('v-manager/login');
			return cms_redirect($login_url);
		}
		if(is_array($this->crud) && !empty($this->crud['model'])) {
			$model_name = $this->crud['model'];
			if(strpos($model_name, '/')) {
				$_model = explode('/', $model_name);
				$model_name = end($_model);
			}
			$this->model = model($model_name);
		}
        $this->cache = cache();
	}
    /** Index page. **/
    public function index()
    {
		$title = isset($this->crud['name'])?$this->crud['name']:'Module';
		$this->view->setVar('title', $title);
		$arg = [];
		if(isset($this->crud['select_options'])) {
			foreach($this->crud['select_options'] as $option) {
				if(!is_array($option) && strpos($option, '|')) {
					$options = explode('|', $option);
					if(isset($options[2])) {
						$model = model($options[2]);
						$model_name = $options[0];
						if(is_object($model)) {
							$data = $model->findAll();
							$this->view->setVar($model_name, $data);
						}
					}
				}
			}
		}
        $this->view->setVar('crud', $this->crud);
        $this->view->setVar('request', $this->request);
        $this->view->setVar('cms', $this->cms);
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'lavalamp',
            'bootstrap-datepicker',
            'datatables.net',
        ]);
		$path = dirname(__DIR__).DIRECTORY_SEPARATOR.'Views'.DIRECTORY_SEPARATOR;
        echo $this->view->render($path."crud/datatable");
    }
	public function get_data() {
		$item_id = $this->request->getGet('item_id');
		if($item_id) {
			if(!empty($this->crud['record']['fields'])) {
				$list_fields = $this->crud['record']['fields'];
				$fields = ['id'];
				foreach($list_fields as $field_name=>$options) {
					if(!isset($options['only_edit']) && $field_name!='break_line') {
						$fields[] = $field_name;
					}
				}
				$this->model->select($fields);
			}
			$item = $this->model->find($item_id);
			$json = $item->toArray();
			$this->render_json($json);
		}
		else return false;
	}
	public function datatable($queries=NULL)
	{
		if(!empty($this->crud['model'])) {
			$args = $this->prepare_arg();
			$json['draw'] = 0;
			$json['recordsTotal'] = 0;
			/* Custom with / Relationship */
			if(isset($this->crud['with'])) {
				$with = $this->crud['with'];
				foreach($with as $relation) {
					if(strpos($relation, "|")) {
						$_relation = explode("|", $relation);
						$this->model->with($_relation[0], ['fields'=>$_relation[1]]);
					} else {
						$this->model->with($relation);
					}
				}
			}
			/* Orders */
			if(isset($this->crud['datagrid_options']['orders'])) {
				$orders = $this->crud['datagrid_options']['orders'];
				foreach($orders as $key=>$value) {
					$this->model->orderBy($key, $value);
				}
			}
			/* WHERE */
			if(isset($this->crud['datagrid_options']['where'])) {
				$where = $this->crud['datagrid_options']['where'];
				foreach($where as $key=>$value) {
					$this->model->where($key, $value);
				}
			}
			$limit_perpage = isset($this->crud['datagrid_options']['limit_perpage'])?$this->crud['datagrid_options']['limit_perpage']:12;
			foreach($args as $arg) {
				if(is_array($arg)) {
					if(isset($arg['like']) && count($arg['like']) == 2) {
						$this->model->like($arg['like'][0], $arg['like'][1]);
					}
					elseif(isset($arg['or_like']) && count($arg['or_like']) == 2) {
						$this->model->orLike($arg['or_like'][0], $arg['or_like'][1]);
					}
					elseif(isset($arg['where']) && count($arg['where']) == 2) {
						$this->model->where($arg['where'][0], $arg['where'][1]);
					}
				} else {
					if($arg == 'group_start') $this->model->groupStart();
					elseif($arg == 'group_end') $this->model->groupEnd();
				}
			}
			$data = $this->model->paginate($limit_perpage);
			if(isset($this->crud['columns'])) 
				$data = $this->prepare_show($data, $this->crud['columns']);
			else
				$data = $this->prepare_show($data);
			$json['paging'] = $this->model->pager->getAjaxLink();
			$json['recordsTotal'] = $this->model->pager->getTotal();
			$json['data'] = $data;
		}
		else $json = ['status' => false, 'message' => 'Model is not detected'];
		$this->render_json($json);
	}
    public function bulk_action()
    {
		if(!empty($this->model)) {
			$action = $this->request->getPost('action');
			$ids = $this->request->getPost('ids');
			if(!$ids || !$action) $json = ['status' => false];
			else {
				if(strpos($ids, ',')) $ids = explode(',', $ids);
				else {
					$ids = [intval($ids)];
				}
				if($action == 'delete') {
					foreach($ids as $id) $this->model->delete(intval($id));
					$json = ['status' => true];
				}
			}
		}
		else $json = ['status' => false, 'message' => 'Model is not detected'];
		$this->render_json($json);
    }
    public function remove()
    {
		if(!empty($this->model)) {
            $id = intval($this->request->getPost('id'));
            if($id) $json = $this->model->delete($id);
            else $json = ['status' => false, 'message' => 'ID not valid'];
		}
		else $json = ['status' => false, 'message' => 'Model is not detected'];
		$this->render_json($json);
    }
    public function save_data()
    {
		if(!empty($this->model)) {
			$posts = $this->request->getPost();
			if(isset($posts['id'])) {
				$message = NULL;
				$status = true;
				
				$id = intval($posts['id']);
				$data = $posts;
				/* Prepare input */
				unset($data['id']);
                if(isset($data['password']) && !$data['password']) unset($data['password']);
                if(!empty($this->crud)) {
                    if(!empty($this->crud['record']['fields'])) {
                        $fields = $this->crud['record']['fields'];
                        foreach($fields as $key=>$field) {
                            if(isset($field['type']) && $field['type']=='datetime') {
                                if(isset($data[$key.'_hour'])) {
                                    $hour = intval($data[$key.'_hour']);
                                    unset($data[$key.'_hour']);
                                } else $hour = 0;
                                if(isset($data[$key.'_min'])) {
                                    $min = intval($data[$key.'_min']);
                                    unset($data[$key.'_min']);
                                } else $min = 0;
                                $datetime = $data[$key].' '.$hour.':'.$min;
                                $data[$key] = convert_time($datetime);
                            }
                            if(isset($field['type']) && ($field['type']=='checkbox' || $field['type']=='switchbox')) {
                                if(empty($data[$key])) $data[$key] = 0;
                            }
                        }
                    }
                    if(!empty($this->crud['rules'])) {
                        $rules = $this->crud['rules'];
                        if(!empty($rules['required'])) {
                            foreach($rules['required'] as $required) {
                                if(!isset($data[$required]) || (isset($data[$required]) && !$data[$required])) {
                                    $message = ucfirst($required).' is required';
                                    $status = false;
                                    break;
                                }
                            }
                        }
                    }
                }
				$query = $this->model->db->query('SHOW INDEX FROM '.$this->model->table);
				$indexs = $query->getResult();
				if(is_array($indexs) && $indexs) {
					foreach($indexs as $unique) {
						if($unique->Null == Null && $unique->Non_unique == 0 && $unique->Key_name != 'PRIMARY') {
							$key = $unique->Column_name;
							if(!isset($data[$key]) || (isset($data[$key]) && !$data[$key])) {
								$message = ucfirst($key).' is required';
								$status = false;
								break;
							} else {
								if($id) $is_duplicate = $this->model->where('id<>', $id)->where($key, $data[$key])->countAllResults();
								else $is_duplicate = $this->model->where($key, $data[$key])->countAllResults();
								if($is_duplicate) {
									$message = 'Item "'.$data[$key].'" is existed in database';
									$status = false;
									break;
								}
							}
						}
					}
				}
				if($status == true) {
					if($id) {
						$status = $this->model->update($id, $data);
						$method = 'Update';
					} else {
						$status = $this->model->insert($data);
						$method = 'Add';
					}
				}
				$json = array();
				if($status == true) {
					$json['message'] = $method.' item successfully!';
					$json['status'] = true;
				} else {
					$json['message'] = $message;
					$json['status'] = false;
				}
			}
		}
		else $json = ['status' => false, 'message' => 'Model is not detected'];
		$this->render_json($json);
    }
	protected function get_config() {
		return NULL;
	}
	private function prepare_arg() {
		$args = [];
		$from = convert_date($this->request->getGet('from'), 0, 'unix');
		$to = convert_date($this->request->getGet('to'), 86400, 'unix');
		$keyword = $this->request->getGet('q');
		if(!empty($this->crud['datagrid_options'])) {
			if($keyword && isset($this->crud['datagrid_options']['search_by'])) {
				$args[] = 'group_start';
				$start = 0;
				foreach($this->crud['datagrid_options']['search_by'] as $search) {
					if($start == 0) $args[] = ['like' => [$search, $keyword]];
					else $args[] = ['or_like' => [$search, $keyword]];
					$start++;
				}
				$args[] = 'group_end';
			}
			if(!empty($this->crud['datagrid_options']['filter_by'])) {
				foreach($this->crud['datagrid_options']['filter_by'] as $filter_by) {
					$filter_data = $this->request->getGet($filter_by);
					if(is_numeric($filter_data)) {
						$args[] = ['where' => [$filter_by, intval($filter_data)]];
					}
					elseif(!empty($filter_data)) $args[] = ['where' => [$filter_by, $filter_data]];
				}
			}
		}
		if($from) $args[] = ['where' => ['created>=', $from]];
		if($to) $args[] = ['where' => ['created<=', $to]];
		return $args;
	}
	/* Prepare Data befor return json data_table */
	private function prepare_show($data, $columns=NULL) {
		$new_data = [];
		if(is_array($data) && count($data)>0)
		foreach($data as $key=>$item) {
			$item = $item->toArray();
			$new_data[$key] = [];
			$new_data[$key]['DT_RowId'] = 'item-'.$item['id'];
			if($columns) {
				foreach($columns as $column_name=>$options) {
					$id = isset($item['id'])?$item['id']:rand(10000, 99999);
					if(isset($item[$column_name])) {
						if(!empty($options['method'])) {
							/* Datetime */
							if($options['method'] == 'datetime') {
								$time = intval($item[$column_name]);
								if($time > 0) {
									$date_format = cms_config('date_format');
									$date_format = ($date_format)?$date_format:'d/m/Y';
									$new_data[$key][] = date($date_format, $time);
								} else {
									$new_data[$key][] = '-';
								}
							}
							/* Function */
							elseif($options['method'] == 'function' && isset($options['function'])) {
								$func = $options['function'];
								if(strpos($func, '|')) {
									$func_opt = explode('|', $func);
									$func = $func_opt[0];
                                    if(function_exists($func))
                                        $new_data[$key][] = $func($item[$column_name], $item[$func_opt[1]]);
                                    else
                                        $new_data[$key][] = '-';
								}
								else {
                                    if(function_exists($func))
                                        $new_data[$key][] = $func($item[$column_name]);
                                    else
                                        $new_data[$key][] = '-';
								}
							}
							/* Toggle */
							elseif($options['method'] == 'toggle' && !empty($options['toggle'])) {
								$func = $options['toggle'];
                                if(function_exists($func))
                                    $new_data[$key][] = $func($column_name, $item[$column_name], $item['id']);
                                else
                                    $new_data[$key][] = '-';
							}
							/* Model */
							elseif(!empty($options['template'])) {
								$template = $options['template'];
								preg_match_all("/{(.*)}/U", $template, $matches);
								if(!empty($matches[1])){
									foreach($matches[1] as $match) {
										$value = '';
										$match = str_replace('$', '', $match);
										if(strpos($match, '->')) {
											$fields = explode('->', $match);
											if(!empty($item[$fields[0]]) && is_object($item[$fields[0]])) {
												$sub_item = $item[$fields[0]]->toArray();
												$value = isset($sub_item[$fields[1]])?$sub_item[$fields[1]]:'-';
											} else $value = '-';
										} else {
											if(!empty($item[$match]))
												$value = $item[$match];
											else $value = '-';
										}
										if($value) $template = str_replace('{$'.$match.'}', $value, $template);
										else $template = str_replace('{$'.$match.'}', '-', $template);
									}
								}
								$template = str_replace("", "", $template);
								$new_data[$key][] = $template;
							}
						} else {
							$new_data[$key][] = $item[$column_name];
						}
					} else {
						if(!empty($options['type'])) {
							if($options['type']=="checkbox") {
								$new_data[$key][] = '
									<div class="custom-control custom-checkbox">
										<input class="custom-control-input items" name="items[]" id="item_'.$id.'" type="checkbox" value="'.$id.'">
										<label class="custom-control-label" for="item_'.$id.'"></label>
									</div>
								';
							}
							elseif($options['type']=="actions") {
								$action_html = '';
								if(!empty($options['edit'])) {
									$url = $options['edit'];
									if(!strpos($url, '://')) $url = rebuild_url(1).$url;
									$meta = 'href="'.$url.$id.'"';
								}
								else $meta = 'href="javascript:;" onclick="render('.$id.')" data-toggle="modal" data-target="#modal-item"';
								$action_html .= '
									<a '.$meta.' class="btn btn-sm btn-primary">
										<span class="fa fa-edit"></span>
									</a>';
								$action_html .= '
									<a href="javascript:;" onclick="remove('.$id.')" class="btn btn-sm btn-danger">
										<span class="fa fa-trash"></span>
									</a>
								';
								$new_data[$key][] = $action_html;
							}
						}
						else {
							if(isset($options['method']) && $options['method']=="function" && isset($options['function'])) {
								$func = $options['function'];
								if(strpos($func, '|')) {
									$func_opt = explode('|', $func);
									$func = $func_opt[0];
                                    if(function_exists($func))
									    $new_data[$key][] = $func($item[$func_opt[1]]);
                                    else
                                        $new_data[$key][] = '-';
								}
								else {
                                    if(function_exists($func))
                                        $new_data[$key][] = $func($item);
                                    else
                                        $new_data[$key][] = '-';
								}
							} else {
								$new_data[$key][] = '-';
							}
						}
					}
				}
			}
		}
		return $new_data;
	}
    public function render_json($json)
    {
        if(is_resource($json))
        {
            throw new RenderException('Resources can not be converted to JSON data.');
        }
        // If there is a fragments array and we've enabled profiling,
        // then we need to add the profile results to the fragments
        // array so it will be updated on the site, since we disable
		$this->response->setHeader("Access-Control-Allow-Origin", "*");
		$this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
		$this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
		$this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
		$this->response->setHeader("Access-Control-Allow-Credentials", "true");
		$this->response->setHeader("Expires", "0");
		$this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
		$this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		$this->response->setHeader("Pragma", "no-cache");
		$this->response->setHeader("Content-Type", "Application/json");
		echo json_encode($json);
    }
}