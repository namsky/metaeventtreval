<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\HTTP\Message;
use App\Modules\Cms\Libraries\Images;

class Uploads extends ApiController
{
	public function __construct()
	{
	}
	public function index()
	{
	}
    public function photo()
    {
		$field = $this->request->getGet('field');
		if(!$field) $field = 'image';
        $image = new Images();
        $json = $image->upload($field, $_FILES);
        if(empty($json))
            $json = ['status'=>'error', 'message'=>'Something went wrong'];
		return $this->render_json($json);
    }
    public function render_json($json)
    {
        if(is_resource($json))
        {
            throw new RenderException('Resources can not be converted to JSON data.');
        }
        // If there is a fragments array and we've enabled profiling,
        // then we need to add the profile results to the fragments
        // array so it will be updated on the site, since we disable
		$this->response->setHeader("Access-Control-Allow-Origin", "*");
		$this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
		$this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
		$this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
		$this->response->setHeader("Access-Control-Allow-Credentials", "true");
		$this->response->setHeader("Expires", "0");
		$this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
		$this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		$this->response->setHeader("Pragma", "no-cache");
		$this->response->setHeader("Content-Type", "Application/json");
		echo json_encode($json);
    }
}