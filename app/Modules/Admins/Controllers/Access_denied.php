<?php namespace App\Modules\Admins\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Access_denied extends AdminController
{
	public function index()
	{
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
        ]);
		$this->view->setVar('title', 'Access Denied');
		$this->view->setVar('cms', $this->cms);
		echo $this->view->render('access_denied');
	}
}