<?php namespace App\Modules\Admins\Models;
use CodeIgniter\Model;
use App\Modules\Admins\Libraries\Auth;

class AdminModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'admins';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name','username','email','password','salt','avatar','activated','status','token','token_by','role_id','auth_secret','auth_status'];

	protected $returnType = 'App\Modules\Admins\Models\Entities\Admin';
	protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';

	protected $beforeInsert = ['data_proccess'];
	protected $afterInsert = [];
	protected $beforeUpdate = ['data_proccess'];
	protected $afterUpdate = [];
	protected $afterFind = [];
	public function __construct()
	{
		$this->has_one['role'] = ['App\Modules\Admins\Models\RoleModel','id','role_id'];
		parent::__construct();
	}
	protected function data_proccess($data) {
		if(isset($data['data']['password'])) {
			$auth = new Auth();
			$data['data']['salt'] = $auth->_generate_hash(20);
			$data['data']['password'] = $auth->_hash_password($data['data']['password'], $data['data']['salt']);
		}
		return $data;
	}
}