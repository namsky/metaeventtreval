<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class IpAllowedModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ip_alloweds';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\IpAllowed';

	protected $allowedFields = ['description', 'ip_address'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
    public function is_whitelisted($ip_address)
    {
        if($this->countAll('ip_address', $ip_address) === 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}