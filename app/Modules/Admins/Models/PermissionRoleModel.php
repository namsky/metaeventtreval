<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class PermissionRoleModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'permission_role';
	protected $primaryKey = 'id';
	protected $returnType = 'App\Modules\Admins\Models\Entities\PermissionRole';
	
	protected $allowedFields = ['permission_id', 'role_id', 'value'];

	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}