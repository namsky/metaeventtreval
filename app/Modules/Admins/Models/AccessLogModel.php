<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class AccessLogModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'access_logs';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\AccessLog';

	protected $allowedFields = ['ip_address', 'useragent', 'admin_id'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	public function __construct()
	{
		$this->has_one['admin'] = ['App\Modules\Admins\Models\AdminModel','id','admin_id'];
		parent::__construct();
	}
}