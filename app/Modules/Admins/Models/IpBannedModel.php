<?php namespace App\Modules\Admins\Models;

use CodeIgniter\Model;

class IpBannedModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ip_banneds';
	protected $primaryKey = 'id';

	protected $returnType = 'App\Modules\Admins\Models\Entities\IpBanned';

	protected $allowedFields = ['ip_address', 'reason', 'expired'];

	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
    public function is_whitelisted($ip_address)
    {
        if($this->countAll('ip_address', $ip_address) === 0) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
}