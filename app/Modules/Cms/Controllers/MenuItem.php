<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class MenuItem extends \App\Modules\Admins\Controllers\AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\MenuItemModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'Edit Menu');
        $menu_id = $this->request->uri->getSegment(3);
		$menu = model('App\Modules\Cms\Models\MenuModel');
		$category = model('App\Modules\Cms\Models\CategoryModel');
		$menus = $menu->with('items')->findAll();
        $this->view->setVar('menus', $menus);
		if($menu_id) {
            $menu_id = intval($menu_id);
            $_menu = $menu->find($menu_id);
            if(empty($_menu)) {
                return \cms_redirect(rebuild_url(2) . '/menus');
            } else {
                $items = $this->model->get_list($menu_id);
                $this->view->setVar('current_menu', $_menu);
                $this->view->setVar('menu_id', $menu_id);
                $this->view->setVar('items', $items);
            }
		}
		$categories = $category->get_list();
        $this->view->setVar('categories', $categories);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'netable',
            'select2',
        ]);
		$this->view->setVar('cms', $this->cms);
		echo $this->view->render('menu_item');
	}
    public function get_data()
    {
        $item_id = $this->request->getGet('item_id');
        $item_id = intval($item_id);
        $item = null;
        if($item_id) {
            $item = $this->model->find($item_id);
        }
        if(isset($item->id)) {
            if(($item->type=='post' || $item->type=='page') && $item->value) {
                $postModel = model('App\Modules\Cms\Models\PostModel');
                $_post = $postModel->select('id,title')->find($item->value);
                $item->options = $_post;
            }
            $json = $item->toArray();
        } else $json = ['id'=>0];
		$this->render_json($json);
	}
    public function update()
    {
		$data = $this->request->getPost('data');
		$data = json_decode($data, true);
		$status = $this->update_order($data);
        $json = ['status' => $status];
        $this->render_json($json);
    }
	protected function update_order($data, $parent=0) {
		try {
			$i=0;
			if(is_array($data) && count($data))
			foreach ($data as $key => $item) {
				$arg = array('id'=>$item['id'], 'parent'=>$parent,'order'=>$key);
				$this->model->update($arg['id'], $arg);
				if(isset($item['children']) && $item['children']) $this->update_order($item['children'], $item['id']);
				$i++;
			}
			return true;
		} catch(Excaption $e) {
			return false;
		}
	}
    public function save_data()
    {
		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$id = intval($posts['id']);
			$data = array();
			$data['menu_id'] = intval($posts['menu_id']);
			$data['type'] = $posts['type'];
			if($posts['type'] == 'category')
				$data['value'] = $posts['category'];
			elseif($posts['type'] == 'post' && !empty($posts['post']))
				$data['value'] = $posts['post'];
			elseif($posts['type'] == 'page' && !empty($posts['page']))
				$data['value'] = $posts['page'];
			else
				$data['value'] = $posts['link'];
			$data['parent'] = isset($posts['parent'])?intval($posts['parent']):0;
			$data['title'] = $posts['title'];
			$data['icon'] = $posts['icon'];
			/* Check requiced */
			$status = true;
			if(empty($data['title'])) {
				$status = false;
				$message = 'Title is requiced';
			}
			elseif(empty($data['type'])) {
				$status = false;
				$message = 'Type is requiced';
			}
			/* Process to database */
			if(!empty($data['parent'])) {
				if($id) $check_parent = $this->model->where('id', $data['parent'])->where('id<>', $id)->countAllResults();
				else $check_parent = $this->model->where('id', $data['parent'])->countAllResults();
			} else $check_parent = 1;
			if($check_parent) {
				if($status) {
					if($id) {
						$status = $this->model->update($id, $data);
						$method = 'Update';
					} else {
						$status = $this->model->insert($data);
						$method = 'Add';
					}
				}
			} else {
				$status = false;
				$message = 'Parent is invalid';
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			$this->render_json($json);
		}
    }
}