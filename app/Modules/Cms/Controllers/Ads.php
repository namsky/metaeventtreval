<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Ads extends \App\Modules\Admins\Controllers\AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\AdsLinkModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'ADS Manager');
		if($this->request->getMethod() == 'post') {
			$posts = $this->request->getPost();
			$id = intval($posts['id']);
			$data = array();
			$data['zone_id'] = intval($posts['zone_id']);
			$data['banner_id'] = intval($posts['banner_id']);
			$data['weight'] = intval($posts['weight']);
			$data['expired'] = intval(convert_time($posts['expired']));
			/* Check requiced */
			$status = true;
			if(empty($data['zone_id'])) {
				$status = false;
				$message = 'Zone is requiced';
			}
			elseif(empty($data['banner_id'])) {
				$status = false;
				$message = 'Banner is requiced';
			}
			/* Process to database */
			if($status) {
				if($id) {
					$status = $this->model->update($id, $data);
					$method = 'Update';
				} else {
					$status = $this->model->insert($data);
					$method = 'Add';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			set_message($json['message'], $json['status']=='error'?'warning':$json['status']);
		}
		
		$bannerModel = model('App\Modules\Cms\Models\AdsBannerModel');
		$_banners = $bannerModel->findAll();
		$banners = [];
		foreach($_banners as $banner) {
			$banners[$banner->id] = $banner;
		}
		$this->view->setVar('banners', $banners);
		
		$zoneModel = model('App\Modules\Cms\Models\AdsZoneModel');
		$zones = $zoneModel->with('links')->findAll();
		$this->view->setVar('zones', $zones);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'netable',
            'select2',
            'tinymce',
        ]);
		$this->view->setVar('cms', $this->cms);
		echo $this->view->render('ads');
	}
    public function remove() {
        $id = intval($this->request->getPost('id'));
        if($id) {
            $status = $this->model->delete($id);
            $json = ['status' => $status];
        } else $json = ['status' => false];
        $this->render_json($json);
    }
    public function get() {
        $id = intval($this->request->getPost('id'));
        if($id) {
            $resutl = $this->model->find($id);
            $json = $resutl;
        } else $json = ['status' => false];
        $this->render_json($json);
    }
    public function get_zone() {
        $id = intval($this->request->getPost('id'));
        if($id) {
            $model = model('App\Modules\Cms\Models\AdsZoneModel');
            $resutl = $model->find($id);
            $json = $resutl;
        } else $json = ['status' => false];
        $this->render_json($json);
    }
}