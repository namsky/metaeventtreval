<?php
namespace App\Modules\Cms\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Modules\Cms\Libraries\UserAuth;
use App\Core\Cms\CmsController;

class Api extends CmsController
{
	protected $auth;
	public function __construct()
	{
		$this->session = \Config\Services::session();
		$this->auth = new UserAuth();
	}
	public function index()
	{
		
	}
	public function user($action=null)
	{
		switch($action) {
			case "login":
				$username = $this->request->getPost('account');
				$password = $this->request->getPost('password');
				if($username && $password) {
					$is_logged = $this->auth->is_logged();
					if($is_logged) {
						$json = ['status'=>'success','message'=>cms_lang('login_success')];
					} else {
						$json = $this->auth->login($username, $password);
					}
					$this->render_json($json);
				} else {
					$this->render_json(['status'=>'error','message'=>'Vui lòng điền đầy đủ thông tin']);
				}
				break;
			case "logout":
				$this->auth->logout();
				$this->render_json(['status'=>'success','message'=>'Đăng xuất thành công']);
				break;
			case "info":
				$account = $this->auth->get_account();
				if($account) {
					$json = ['status'=>'success','data'=>$account];
				} else {
					$json = ['status'=>'error','message'=>cms_lang('you_not_logged')];
				}
				$this->render_json($json);
				break;
			case "forgot":
				$json = $this->auth->forgot();
				$this->render_json($json);
				break;
			case "register":
				$args['username'] = $this->request->getPost('account');
				$args['email'] = $this->request->getPost('email');
                $args['password'] = $this->request->getPost('password');
                $args['confirm'] = $this->request->getPost('confirm');
                $secure_code = $this->request->getPost('secure_code');
                $server_code = $this->session->get('secure_code');
                if(cms_config('user_catcha')) {
                    if(empty($json) && $secure_code != $server_code) {
                        $json = ['status'=>'error','message'=>cms_lang('secure_code_not_match')];
                    } 
                }
                if(empty($json)) {
                    $json = $this->auth->register($args);
                }
                if(empty($json)) {
					$json = ['status'=>'error','message'=>cms_lang('some_thing_went_wrong')];
				}
				$this->render_json($json);
				break;
		}
    }
    private function render_json($json)
    {
        if(is_resource($json))
        {
            throw new RenderException('Resources can not be converted to JSON data.');
        }
		$callback = $this->request->getGet('callback');
		$this->response->setHeader("Access-Control-Allow-Origin", "*");
		$this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
		$this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
		$this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
		$this->response->setHeader("Access-Control-Allow-Credentials", "true");
		$this->response->setHeader("Expires", "0");
		$this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
		$this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		$this->response->setHeader("Pragma", "no-cache");
		$this->response->setHeader("Content-Type", "Application/json");
		if($callback) {
			echo $callback . '(' . json_encode($json) . ')';
		} else {
			echo json_encode($json);
		}
    }
}