<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Ads_zones extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'ADS Zone',
			'model' => 'App\Modules\Cms\Models\AdsZoneModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['name','description','width','height'],
				'orders' => ['id' => 'desc'],
				'bulk_actions' => true,
			],
			'select_options' => [
				'page' => [
					'all_page' => 'All Page',
					'home' => 'Home Page',
					'single' => 'Single Page',
					'archives' => 'Archives Page',
					'category' => 'Category Page',
					'tag' => 'Tag Page',
					'search' => 'Search Page',
				],
				'position' => [
					'header' => 'Header',
					'side_bar' => 'Side bar',
					'mid_post' => 'Middle post',
					'footer' => 'Footer',
					'in_archives' => 'in Archives',
					'float_left' => 'Float Left',
					'float_right' => 'Float Right',
				],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'name' => ['name' => 'Name', 'class' => 'text-center'],
				'description' => ['name' => 'Description'],
				'width' => ['name' => 'Width', 'class' => 'text-center d-sm-table-cell d-none'],
				'height' => ['name' => 'Height', 'class' => 'text-center d-sm-table-cell d-none'],
				'page' => ['name' => 'Page', 'class' => 'text-center d-sm-table-cell d-none'],
				'position' => ['name' => 'Position', 'class' => 'text-center d-sm-table-cell d-none'],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'rules' => [
				'required' => ['name', 'page']
			],
			'record' => [
				'colums' => 6,
				'fields' => [
					'name' => ['name' => 'Name', 'colums' => 12],
					'description' => ['name' => 'Description', 'colums' => 12],
					'width' => ['name' => 'Width (px)'],
					'height' => ['name' => 'Height (px)'],
					'page' => ['name' => 'Page', 'type' => 'select'],
					'position' => ['name' => 'Position', 'type' => 'select'],
				],
			],
		];
		return $config;
	}
}