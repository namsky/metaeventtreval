<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Languages extends \App\Modules\Admins\Controllers\AdminController
{
	function get_config() {
		$config = [
			'name' => 'Languages',
			'model' => 'App\Modules\Cms\Models\LanguageDataModel',
			'datagrid_options' => [
				'limit_perpage' => 20,
				'search_by' => ['key','value'],
				'filter_by' => ['lang_id','locale','group'],
				'filter_date' => false,
				'orders' => ['id' => 'desc'],
				'bulk_actions' => false,
			],
			'select_options' => [
				'lang_id' => 'language|id,name|App\Modules\Cms\Models\LanguageModel',
				'locale' => ['vn' => 'VN', 'en' => 'EN'],
			],
			'columns' => [
				'checkbox' => ['type' => 'checkbox', 'class' => 'text-center'],
				'id' => ['name' => 'ID', 'class' => 'text-center'],
				'key' => ['name' => 'Key'],
				'value' => ['name' => 'Value', 'class' => 'd-sm-table-cell d-none'],
				'lang_id' => [
					'name' => 'Language',
					'method' => 'template',
					'template' => '[{$locale}] {$language->name}',
					'class' => 'd-lg-table-cell d-none',
				],
				'group' => ['name' => 'Group', 'class' => 'text-center d-sm-table-cell d-none'],
				'created' => [
					'name' => 'Created',
					'method' => 'datetime',
					'class' => 'text-center d-lg-table-cell d-none',
				],
				'actions' => [
					'type' => 'actions',
					'class' => 'text-right'
				],
			],
			'with' => ['language|id,name,locale'],
			'rules' => [
				'required' => ['lang_id', 'key', 'value']
			],
			'record' => [
				'colums' => 6,
				'fields' => [
					'key' => ['name' => 'Key'],
					'group' => ['name' => 'Group'],
					'value' => ['name' => 'Value', 'colums' => 12],
					'lang_id' => [
						'name' => 'Language',
						'type' => 'select',
						'colums' => 12
					]
				],
			],
		];
		return $config;
	}
}