<?php namespace App\Modules\Cms\Controllers;
use CodeIgniter\Controller;
use CodeIgniter\HTTP\Message;

class Widgets extends \App\Modules\Admins\Controllers\AdminController
{
    function __construct() {
		//parent::__construct();
		$this->model = model('App\Modules\Cms\Models\WidgetModel');
    }
	public function index()
	{
		$this->view->setVar('title', 'Widgets');
		$items = $this->model->where('page_id', 1)->orderBy('order', 'ASC')->findAll();
        $this->view->setVar('items_1', $items);
		$items = $this->model->where('page_id', 2)->orderBy('order', 'ASC')->findAll();
        $this->view->setVar('items_2', $items);
		$items = $this->model->where('page_id', 3)->orderBy('order', 'ASC')->findAll();
        $this->view->setVar('items_3', $items);
        
		$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
		$categories = $categoryModel->findAll();
		$this->view->setVar('categories', $categories);
		
        $this->cms->load_vendors([
            'nucleo',
            'fontawesome',
            'jquery',
            'bootstrap',
            'bootstrap-notify',
            'js-cookie',
            'sweetalert2',
            'bootstrap-datepicker',
            'netable',
            'select2',
            'tinymce',
        ]);
		$this->view->setVar('cms', $this->cms);
		echo $this->view->render('widgets');
	}
    public function ajax($action = null)
    {
		$this->response->setHeader("Access-Control-Allow-Origin", "*");
		$this->response->setHeader("Access-Control-Expose-Headers", "Access-Control-Allow-Origin");
		$this->response->setHeader("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, OPTIONS");
		$this->response->setHeader("Access-Control-Allow-Headers", "Content-Type,X-CSRF-Token, XHR, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5,  Date, X-Api-Version, X-File-Name");
		$this->response->setHeader("Access-Control-Allow-Credentials", "true");
		$this->response->setHeader("Expires", "0");
		$this->response->setHeader("Last-Modified", gmdate("D, d M Y H:i:s") . " GMT");
		$this->response->setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
		$this->response->setHeader("Pragma", "no-cache");
		switch ($action) {
			case 'get_data':
				$item_id = $this->request->getGet('item_id');
				$item_id = intval($item_id);
				$item = null;
				if($item_id) {
					$item = $this->model->find($item_id);
				}
				if(isset($item->id)) {
					$json = $item->toArray();
					if(isset($json['tags'])) {
						if(strpos($json['tags'], ',')) {
							$tags = explode(',', $json['tags']);
						} else $tags = [intval($json['tags'])];
						$html = '';
						$tagModel = model('App\Modules\Cms\Models\TagModel');
						foreach($tags as $tag_id) {
							$tag = $tagModel->find($tag_id);
							if(isset($tag->id)) {
								$html .= '<option value="'.$tag->id.'" selected="selected">'.$tag->name.'</option>';
							}
						}
						$json['tags'] = $html;
					}
				} else $json = ['id'=>0];
			break;
			case 'remove':
				$resutl = $this->remove();
				$json = $resutl;
			break;
			case 'save_data':
				$resutl = $this->save_data();
				$json = $resutl;
			break;
			case 'update':
				$resutl = $this->update();
				$json = $resutl;
			break;
			default:
				$json = ['status' => false];
		}
		return $this->response->setJSON($json);
	}
    public function get_data()
    {
        $item_id = $this->request->getGet('item_id');
        $item_id = intval($item_id);
        $item = null;
        if($item_id) {
            $item = $this->model->find($item_id);
        }
        if(isset($item->id)) {
            $json = $item->toArray();
            if(isset($json['tags'])) {
                if(strpos($json['tags'], ',')) {
                    $tags = explode(',', $json['tags']);
                } else $tags = [intval($json['tags'])];
                $html = '';
                $tagModel = model('App\Modules\Cms\Models\TagModel');
                foreach($tags as $tag_id) {
                    $tag = $tagModel->find($tag_id);
                    if(isset($tag->id)) {
                        $html .= '<option value="'.$tag->id.'" selected="selected">'.$tag->name.'</option>';
                    }
                }
                $json['tags'] = $html;
            }
        } else $json = ['id'=>0];
        $this->render_json($json);
    }
    public function remove()
    {
		$id = intval($this->request->getPost('id'));
		$status = $this->model->delete($id);
        $json = ['status' => $status];
        $this->render_json($json);
    }
    public function update()
    {
		$page_id = intval($this->request->getGet('page_id'));
		$data = $this->request->getPost('data');
		$data = json_decode($data, true);
		$status = $this->update_order($data, 0, $page_id);
        $json = ['status' => $status];
        $this->render_json($json);
    }
	protected function update_order($data, $parent=0, $page_id=0) {
		try {
			if(is_array($data) && count($data))
			foreach ($data as $key => $item) {
                $arg = array('id'=>$item['id'], 'order'=>$key);
                if($page_id) $arg['page_id'] = $page_id;
				$this->model->update($arg['id'], $arg);
			}
			return true;
		} catch(Excaption $e) {
			return false;
		}
	}
    public function get_slug()
    {
		$id = intval($this->request->getPost('id'));
		$data = $this->request->getPost('data');
		$slug = clear_utf8($data);
        $json = ['status' => 'success', 'slug' => $slug];
        $this->render_json($json);
    }
    public function save_data()
    {
		$posts = $this->request->getPost();
		if(isset($posts['id'])) {
			$id = intval($posts['id']);
			$data = $posts;
			if(isset($data['tags']) && is_array($data['tags'])) {
				$data['tags'] = implode(",", $data['tags']);
			} else $data['tags'] = '';
			/* Check requiced */
			$status = true;
			if(empty($data['title'])) {
				$status = false;
				$message = 'Title is requiced';
			} elseif(empty($data['type'])) {
				$status = false;
				$message = 'Type is requiced';
			} elseif(empty($data['page_id'])) {
				$status = false;
				$message = 'Page is requiced';
			}
			/* Process to database */
			if($status) {
				if($id) {
					$status = $this->model->update($id, $data);
					$method = 'Update';
				} else {
					$status = $this->model->insert($data);
					$method = 'Add';
				}
			}
			$json = array();
			if($status) {
				$json['message'] = $method.' items successfully!';
				$json['status'] = 'success';
			} else {
				$json['message'] = !empty($message)?$message:'Something went wrong';
				$json['status'] = 'error';
			}
			$this->render_json($json);
		}
    }
}