<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class MenuModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'menu';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'position'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\Menu';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
	public function __construct()
	{
		$this->has_many['items'] = ['App\Modules\Cms\Models\MenuItemModel','menu_id','id'];
		parent::__construct();
	}
}