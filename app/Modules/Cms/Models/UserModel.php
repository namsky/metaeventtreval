<?php namespace App\Modules\Cms\Models;
use CodeIgniter\Model;
use App\Modules\Cms\Libraries\UserAuth;

class UserModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'users';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name','username','email','password','salt','avatar','activate_code','status','token','token_by','group_id','auth_secret','auth_status'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\User';
	protected $useSoftDeletes = true;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';

	protected $beforeInsert = ['data_proccess'];
	protected $afterInsert = [];
	protected $beforeUpdate = ['data_proccess'];
	protected $afterUpdate = [];
	protected $afterFind = [];
	public function __construct()
	{
		$this->has_one['group'] = ['App\Modules\Cms\Models\UserGroupModel','id','group_id'];
		$this->has_many['links'] = ['App\Modules\Cms\Models\UserLinkModel','user_id','id'];
		parent::__construct();
	}
	protected function data_proccess($data) {
		if(isset($data['data']['password'])) {
			$auth = new UserAuth();
			$data['data']['salt'] = $auth->_generate_hash(20);
			$data['data']['password'] = $auth->_hash_password($data['data']['password'], $data['data']['salt']);
		}
		return $data;
	}
}