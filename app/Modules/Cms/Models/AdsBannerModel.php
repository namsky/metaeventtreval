<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class AdsBannerModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ads_banners';
	protected $primaryKey = 'id';
	protected $allowedFields = ['name', 'width', 'height', 'responsive', 'code', 'link', 'image', 'clicks', 'views'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\AdsBanner';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
}