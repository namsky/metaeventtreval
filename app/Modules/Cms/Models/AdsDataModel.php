<?php namespace App\Modules\Cms\Models;

use CodeIgniter\Model;

class AdsDataModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'ads_data';
	protected $primaryKey = 'id';
	protected $allowedFields = ['zone_id', 'banner_id', 'type', 'value', 'optional_value', 'created'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\AdsData';
	protected $useSoftDeletes = false;
	protected $useTimestamps = false;
}