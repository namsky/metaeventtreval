<?php namespace App\Modules\Cms\Models;
use CodeIgniter\Model;
use App\Modules\Cms\Libraries\Auth;

class LanguageDataModel extends Model
{
	use \Core\VModels\ModelTrait;
	protected $table      = 'language_data';
	protected $primaryKey = 'id';
	protected $allowedFields = ['lang_id','locale','key','value','group'];

	protected $returnType = 'App\Modules\Cms\Models\Entities\LanguageData';
	protected $useSoftDeletes = false;
	protected $useTimestamps = true;
	protected $dateFormat = 'int';
	protected $createdField  = 'created';
	protected $updatedField  = 'modified';
	protected $deletedField  = 'deleted';
	
	protected $beforeInsert = ['data_proccess'];
	protected $afterInsert = [];
	protected $beforeUpdate = ['data_proccess'];
	protected $afterUpdate = [];
	protected $afterFind = [];
	public function __construct()
	{
		$this->has_one['language'] = ['App\Modules\Cms\Models\LanguageModel','id','lang_id'];
		parent::__construct();
	}
	protected function data_proccess($data) {
		if(!isset($data['data']['locale']) && isset($data['data']['lang_id'])) {
			$langModel = model('App\Modules\Cms\Models\LanguageModel');
			$lang = $langModel->find($data['data']['lang_id']);
			if(isset($lang->locale))
				$data['data']['locale'] = $lang->locale;
		}
		return $data;
	}
}