<?php
namespace App\Modules\Cms\Libraries;
use CodeIgniter\HTTP\Request;
use CodeIgniter\HTTP\UserAgent;
use App\Modules\Admins\Libraries\GoogleAuthenticator;
use App\Modules\Admins\Libraries\Auth;

class Images {
    private $upload_path;
	protected $allowed;
	public function __construct()
	{
		$this->allowed = ['image/jpg','image/jpeg','image/gif','image/png','image/webp'];
		$this->upload_path = FCPATH.'/writable/uploads/';
	}
    public function upload($field, $file_upload, $args=[]) {
        $auth = new Auth();
		if($auth->is_logged() != TRUE) {
			return ['status' => false, 'message' => 'Auth false!'];
		}
		if(!cms_config('upload_image')) {
			return ['status'=>'error', 'message'=>'Upload image is off'];
		}
		if(isset($file_upload[$field])) {
			$title = !empty($args['title'])?$args['title']:NULL;
			$relation_id = !empty($args['relation_id'])?intval($args['relation_id']):NULL;
			$relation_table = !empty($args['relation_table'])?$args['relation_table']:NULL;
			
			$content = @file_get_contents($file_upload[$field]['tmp_name']);
			$file_size = $file_upload[$field]['size'];
			$file_name = $file_upload[$field]['name'];
			$image_info = getimagesize($file_upload[$field]['tmp_name']);
			
			if($content) {
				if(empty($image_info) || !in_array($image_info['mime'], $this->allowed)) {
					$json = ['status'=>'error', 'message'=>cms_lang('file_not_allowed')];
				}
				$size_limit = cms_config('upload_size_limit');
				if($status && $file_size > ($size_limit*1024)) {
					$json = ['status'=>'error', 'message'=>cms_lang('upload_size_limit', [$size_limit])];
				}
                if(empty($json)) {
                    if(cms_config('cdn8')) {
                        $json = $this->upload_cdn8($file_name, $content);
                    } else {
                        $json = $this->upload_image($file_name, $content);
                    }
                    if($json['status'] == 'success' && !empty($json['file_url'])) {
                        $hash = md5($content);
                        $data = [
                            'title' => $title,
                            'relation_id' => $relation_id,
                            'relation_table' => $relation_table,
                            'hash' => $hash,
                            'file_name' => $file_name,
                            'file_size' => $file_size,
                            'origin_url' => $json['file_url']
                        ];
                        $photoModel = model('App\Modules\Cms\Models\PhotoModel');
                        $is_existed = $photoModel->where('hash', $hash)->first();
                        if(!$is_existed) {
                            $photo_id = $photoModel->insert($data);
                        } else {
                            $photo_id = $is_existed->id;
                        }
                        $json['photo_id'] = $photo_id;
                    }
                }
			} else $json = ['status'=>'error', 'message'=>cms_lang('image_is_empty')];
        } else $json = ['status'=>'error', 'message'=>cms_lang('no_image_upload')];
        return $json;
    }
    public function tranload($image_url, $args=[]) {
		if($image_url) {
			$title = !empty($args['title'])?$args['title']:NULL;
			$relation_id = !empty($args['relation_id'])?intval($args['relation_id']):NULL;
			$relation_table = !empty($args['relation_table'])?$args['relation_table']:NULL;
			
            $content = @file_get_contents($image_url);
			$image_info = getimagesize($image_url);
            if(strpos($image_url, '?')) {
                $explode = explode($image_url, '?');
                $image_url = $explode[0];
            }
            $file_name = basename($image_url);
			if($content) {
                $file_size = strlen($content);
				if(empty($image_info) || !in_array($image_info['mime'], $this->allowed)) {
					$json = ['status'=>'error', 'message'=>cms_lang('file_not_allowed')];
				}
				$size_limit = cms_config('upload_size_limit');
				if($file_size > ($size_limit*1024)) {
					$json = ['status'=>'error', 'message'=>cms_lang('upload_size_limit', [$size_limit])];
				}
                if(empty($json)) {
                    if(cms_config('cdn8')) {
                        $json = $this->upload_cdn8($file_name, $content);
                    } else {
                        $json = $this->upload_image($file_name, $content);
                    }
                    if($json['status'] == 'success' && !empty($json['file_url'])) {
                        $hash = md5($content);
                        $data = [
                            'title' => $title,
                            'relation_id' => $relation_id,
                            'relation_table' => $relation_table,
                            'hash' => $hash,
                            'file_name' => $file_name,
                            'file_size' => $file_size,
                            'origin_url' => $json['file_url']
                        ];
                        $photoModel = model('App\Modules\Cms\Models\PhotoModel');
                        $is_existed = $photoModel->where('hash', $hash)->first();
                        if(!$is_existed) {
                            $photo_id = $photoModel->insert($data);
                        } else {
                            $photo_id = $is_existed->id;
                        }
                        $json['photo_id'] = $photo_id;
                    }
                }
			} else $json = ['status'=>'error', 'message'=>cms_lang('image_is_empty')];
        } else $json = ['status'=>'error', 'message'=>cms_lang('no_image_upload')];
        return $json;
    }
    private function upload_image($file_name, $content) {
        $hash = md5($content);
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        $file_path = $this->upload_path.$hash.'.'.$ext;
        $new_name = $hash.'/'.$file_name;
        $file_url = URL.'/images/s0/'.$new_name;
        if(!file_exists($file_path)) {
            $return = @file_put_contents($file_path, $content);
            if(!$return) {
                return ['status'=>'error', 'message'=>cms_lang('upload_error')];
            }
        } else {
            $checksum = md5(file_get_contents($file_path));
            if($hash != $checksum) {
                $return = @file_put_contents($file_path, $content);
                if(!$return) {
                    return ['status'=>'error', 'message'=>cms_lang('upload_error')];
                }
            }
        }
        return ['status'=>'success', 'location'=>$file_url, 'message'=>cms_lang('upload_success')];
    }
    public function upload_cdn8($file_name, $content)
    {
        $cdn8_token = cms_config('cdn8_token');
		if(!cms_config('cdn8') || !$cdn8_token) {
			return ['status'=>'error', 'message'=>'Upload to CDN8 is off or not config'];
		}
        $headers = array('Content-Type: application/json');
        $post = '{"name": "'.$file_name.'","api_token": "'.$cdn8_token.'","data":"'.base64_encode($content).'"}';
        $url = "https://cdn8.net/api/upload";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        if($data = json_decode($response, true)) {
            if($data['status'] == true && isset($data['url'])) {
                return ['status'=>'success', 'location'=>$data['url'], 'message'=>cms_lang('upload_success')];
            } elseif(isset($data['message'])) {
                return ['status'=>'error', 'message'=>$data['message']];
            }
        } else return ['status'=>'error', 'message'=>'API not responding'];
    }
}