<?php namespace App\Modules\Cms\Widgets;
use App\Core\Cms\CmsWidget;

class Footer_menu extends CmsWidget
{
    function index($args=[]) {
		$cateModel = model('App\Modules\Cms\Models\CategoryModel');
		
		/* Load from cache */
		//$cached = cms_config('cache');
		$items = false;
		if(!$items) {
			/* Load from db */
			$items = $cateModel->findAll(100, 0);
			$this->cache->save('footer_menu', $items, 86400);
		}
		
		$request = \Config\Services::request();
		
		$this->view->setVar('items', $items);
		return $this->view->render('footer_menu');
    }
}