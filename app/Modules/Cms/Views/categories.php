<?//=$this->extend('layouts/main');?>
<?=$this->section('content');?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left"></div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<button type="button" class="btn btn-primary btn-sm" onclick="render()"><?=lang('Add_new');?></button>
									</div>
								</div>
							</form>
						</div>
						<div class="card-content row">
							<div class="col-md-4">
								<div class="dd nestable m-3">
									<?=$cms->show_categories($items);?>
								</div>
							</div>
							<div class="col-md-8">
								<form id="form-item">
									<div class="row m-0">
										<div class="col-md-4">
											<div class="form-group">
												<label for="name" class="form-control-label"><?=lang('Name');?></label>
												<input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
												<input class="form-control form-control-sm" type="text" value="" id="name" name="name">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="slug" class="form-control-label"><?=lang('Slug');?></label>
												<input class="form-control form-control-sm" type="slug" value="" id="slug" name="slug">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="parent" class="form-control-label"><?=lang('Parent');?></label>
												<select class="form-control form-control-sm" id="parent" name="parent">
												<?=$cms->dropdown_categories($items);?>
												</select>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="title" class="form-control-label"><?=lang('Title');?></label>
												<input class="form-control form-control-sm" type="text" value="" id="title" name="title">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="description" class="form-control-label"><?=lang('Description');?></label>
												<input class="form-control form-control-sm" type="text" value="" id="description" name="description">
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<label for="keywords" class="form-control-label"><?=lang('Keywords');?></label>
												<input class="form-control form-control-sm" type="keywords" value="" id="keywords" name="keywords">
											</div>
										</div>
										<div class="col-md-12 text-right mb-2">
											<button type="button" id="btn_save" class="btn btn-success btn-sm" onClick="save();"><?=lang('Save');?></button>
										</div>
										<div class="col-md-12">
											<div class="form-group mb-0">
												<textarea class="editor" id="summary" name="summary"></textarea>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
							<div class="col-sm-12 col-md-12">
								<span class="total"></span>
								<nav class="float-right">
								</nav>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<?php
		$args = array(
			'field_slug' => 'name',
			'editor' => true,
		);
		$cms->gen_js($args);
		$cms->load_editor('.editor', 300, 'mini');
		?>
		<script>
			$(document).ready(function(e) {
				if ($('.nestable').length && $.fn.nestable) {
					$('.nestable').nestable();
				}
				$('.dd').on('change', function() {
					var data = $('.nestable').nestable('serialize');
					data = window.JSON.stringify(data);
					$.post("<?=current_url();?>/update", {data:data}, function(data,status){});
				});
			});
		</script>
<?=$this->endSection();?>