<?php if(isset($items)) { ?>
<div class="col-md-4 footer_column">
	<div class="footer_widget widget_categories" id="categories-2">
		<div class="jeg_footer_heading jeg_footer_heading_1">
			<h3 class="jeg_footer_title"><span>Danh mục bài viết</span></h3>
		</div>
		<ul>
			<?php foreach($items as $item) { ?>
			<li class="cat-item cat-item-50"><a href="<?=category_url($item)?>" title="<?=(isset($item->description) && $item->description != '')?$item->description:$item->name?>"><?=$item->name?></a></li>
			<?php } ?>
		</ul>
	</div>
</div>
<?php } ?>