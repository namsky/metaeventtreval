<?php if(isset($items)) foreach($items as $item) {
		$class = '';
		$set = '';
		if($item->type=="category") {
			$link = category_url($item->value);
			if($item->value == $current) {
				$class = "current-menu-item current_page_item ";
				$set = 'aria-current="page"';
			}
		}
		elseif($item->type=="post" || $item->type=="page") {
			$link = post_url($item->value);
			if($item->value == $current) {
				$class = "current-menu-item current_page_item ";
				$set = 'aria-current="page"';
			}
		}
		else {
			$link = $item->value;
			$temp = str_replace('/', '', $link);
			if($temp == $current) {
				$class = "current-menu-item current_page_item ";
				$set = 'aria-current="page"';
			}
			if($link == '/') $link = URL.'/';
		}
		$label = strip_tags($item->title);
		if(!$label) $label = cms_config('site_name');
	?>
	<li class="menu-item menu-item-type-post_type menu-item-object-page <?=$class?>"><a href="<?=$link?>" <?=$set?>><?=$item->title?></a></li>
<?php } ?>