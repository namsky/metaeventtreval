<div class="jeg_bottombar jeg_navbar jeg_container jeg_navbar_wrapper jeg_navbar_normal jeg_navbar_normal">
	<div class="container">
		<div class="jeg_nav_row">
			<div class="jeg_nav_col jeg_nav_left jeg_nav_grow">
				<div class="item_wrap jeg_nav_alignleft">
					<div class="jeg_nav_item jeg_main_menu_wrapper">
						<div class="jeg_mainmenu_wrap">
							<ul class="jeg_menu jeg_main_menu jeg_menu_style_1" data-animation="animate">
								<?
								function show_menu($items, $current, $is_sub=0) {
									$html = '';
									if($is_sub) {
										$html .= '<ul class="sub-menu">';
									}
									foreach($items as $item) {
										$class = '';
										if($item->type=="category") {
											$link = category_url($item->value);
											if($item->value == $current) $class .= "current-menu-item current_page_item ";
										}
										elseif($item->type=="post" || $item->type=="page") {
											$link = post_url($item->value);
											if($item->value == $current) $class .= "current-menu-item current_page_item ";
										}
										else {
											$link = $item->value;
											$temp = str_replace('/', '', $link);
											if($temp == $current) $class .= "current-menu-item current_page_item ";
											if($link == '/') $link = URL.'/';
										}
										$label = strip_tags($item->title);
										if(!$label) $label = cms_config('site_name');
										if(!$is_sub) {
											if(!$item->child) {
												$html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item bgnav '.$class.'"><a class="nav-link" href="'.$link.'">'.$label.'</a></li>';
											} else {
												$class .= 'current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor ';
												$sub_menu = show_menu($item->child, $current, 1);
												$html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item bgnav '.$class.' menu-item-has-children"><a href="'.$link.'">'.$item->title.'</a>'.$sub_menu.'</li>';
											}
										} else {
											if(!$item->child) {
												$html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item bgnav"><a href="'.$link.'">'.$label.'</a></li>';
											} else {
												$class .= 'current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor ';
												$sub_menu = show_menu($item->child, $current, 1);
												$html .= '<li class="menu-item menu-item-type-post_type menu-item-object-page page_item bgnav menu-item-has-children '.$class.'"><a href="'.$link.'">'.$item->title.'</a>'.$sub_menu.'</li>';
											}
										}
									}
									if($is_sub) {
										$html .= '</ul>';
									}
									return $html;
								}
								?>
								<?=show_menu($items, $current);?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="jeg_nav_col jeg_nav_center jeg_nav_normal">
				<div class="item_wrap jeg_nav_aligncenter"></div>
			</div>
			<div class="jeg_nav_col jeg_nav_right jeg_nav_normal">
				<div class="item_wrap jeg_nav_alignright">
					<div class="jeg_nav_item jeg_search_wrapper search_icon jeg_search_popup_expand"> <a href="#" class="jeg_search_toggle"><i class="fa fa-search"></i></a>
						<form action="" method="get" class="jeg_search_form" target="_top">
							<input name="s" class="jeg_search_input" placeholder="Search..." type="text" value="" autocomplete="off">
							<button aria-label="Search Button" type="submit" class="jeg_search_button btn"><i class="fa fa-search"></i></button>
						</form>
						<div class="jeg_search_result jeg_search_hide with_result">
							<div class="search-result-wrapper"></div>
							<div class="search-link search-noresult"> No Result</div>
							<div class="search-link search-all-button"> <i class="fa fa-search"></i> View All Result</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>