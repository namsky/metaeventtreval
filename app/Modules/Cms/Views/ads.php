<?=$this->section('content');?>
<?php
	$base_url = rebuild_url(1);
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=$title;?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left"></div>
							<form method="GET" id="form-filter">
								<div class="float-right mb-0">
									<div class="float-left ml-2">
										<button type="button" class="btn btn-primary btn-sm" onclick="clearForm()"><?=lang('Add new');?></button>
									</div>
								</div>
							</form>
						</div>
						<div class="card-content row">
							<div class="col-md-8">
								<div class="list_ads">
									<?php
									foreach($zones as $item) {
									?>
									<div class="zone_item">
										<div class="zone_header row">
											<div class="col-6">
												<a class="zone_name" href="javascript:;" onclick="render_zone(<?=$item->id?>);">
													<i class="fas fa-caret-right"></i> <?=$item->name?>
												</a>
											</div>
											<div class="col-2 text-center">
												<?=$item->page?>
											</div>
											<div class="col-4 text-right">
												[<?=$item->width?> x <?=$item->height?>]
											</div>
										</div>
										<div class="ads_content row">
											<div class="col-12">
												<?php
												if(count($item->links)) {
													foreach($item->links as $link) {
														if(isset($banners[$link->banner_id])) {
															$banner = $banners[$link->banner_id];
												?>
												<div class="row have_ads" id="item-<?=$link->id?>">
													<div class="col-6">
														<a class="banner_name" href="javascript:;" onclick="render_ads(<?=$link->id?>);">											
															<i class="fas fa-ad"></i> <?=$banner->name?>
														</a>
													</div>
													<div class="col-1 text-center">
														<?=$link->weight?>
													</div>
													<div class="col-3 text-center">
														<?php
														if($link->expired) {
															$date = date("d/m/Y", $link->expired);
															if($link->expired>time()) echo '<b style="color: green">'.$date.'</b>';
															else echo '<b style="color: red">'.$date.'</b>';
														} else echo '-';
														
														?>
													</div>
													<div class="col-2 text-right">
														<a class="delete btn-sm" href="javascript:;" onclick="remove_ads(<?=$link->id?>);"><i class="fa fa-times-circle"></i></a>
													</div>
												</div>
												<?php
														} else {
												?>
												<div class="row none_ads">
													<div class="col-12">NONE</div>
												</div>
												<?php
														}
													}
												} else {
												?>
												<div class="row none_ads">
													<div class="col-12">NONE</div>
												</div>
												<?php
												}
												?>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
							<div class="col-md-4">
								<form method="POST" id="form-item">
									<div class="row m-0">
										<input type="hidden" id="id" name="id" value="">
										<div class="col-md-12">
											<div class="form-group">
												<label for="zone_id" class="form-control-label"><?=lang('Zones');?></label>
												<select class="form-control form-control-sm select2" id="zone_id" name="zone_id">
													<option value=""></option>
												<?php
												foreach($zones as $zone) {
													echo '<option value="'.$zone->id.'">'.$zone->name.' - ['.$zone->width.'x'.$zone->height.']</option>';
												}
												?>
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label for="banner_id" class="form-control-label"><?=lang('Banners');?></label>
												<select class="form-control form-control-sm select2" id="banner_id" name="banner_id">
													<option value=""></option>
												<?php
												foreach($banners as $banner) {
													echo '<option value="'.$banner->id.'">'.$banner->name.' - ['.$banner->width.'x'.$banner->height.']</option>';
												}
												?>
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label for="expired" class="form-control-label"><?=lang('Expired');?></label>
												<input class="form-control form-control-sm datepicker" type="text" value="" id="expired" name="expired">
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label for="weight" class="form-control-label"><?=lang('Weight');?></label>
												<input class="form-control form-control-sm" type="number" value="" min="0" id="weight" name="weight">
											</div>
										</div>
										<div class="col-md-12 text-right mb-2">
											<button type="button" id="btn_save" class="btn btn-success btn-sm" onClick="$('#form-item').submit();"><?=lang('Save');?></button>
										</div>
									</div>
								</form>
							</div>
						</div>
                        <!-- Card footer -->
                        <div class="card-footer py-4">
							<div class="col-sm-12 col-md-12">
								<span class="total"></span>
								<nav class="float-right">
								</nav>
							</div>
                        </div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function(e) {
				if(typeof($('.select2').select2) !== 'undefined') {
					$('.select2').select2({});
				}
			});
			function remove_ads(id) {
				swal({
					title: "Are you sure?",
					text: "You won't be able to revert this!",
					type: "warning",
					showCancelButton: !0,
					buttonsStyling: !1,
					confirmButtonClass: "btn btn-danger",
					confirmButtonText: "Yes, delete it!",
					cancelButtonClass: "btn btn-secondary"
				}).then(t => {
					if(id && t.value) {
						$.post("<?=current_url();?>/remove", {id:id}, function(data,status){
							if(data) {
								$("#item-"+id).remove();
								swal({
									title: "Your item has been deleted.",
									type: "success",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-success"
								});
							}
							else {
								swal({
									title: "Something went wrong.",
									type: "warning",
									buttonsStyling: !1,
									confirmButtonClass: "btn btn-primary"
								});
							}
						});
					}
				});
				return false;
			}
			function render_zone(id) {
				data = '';
				$.post("<?=current_url();?>/get_zone", {id:id},
				function(data, status) {
					$("#zone_id").val(data.id).change();
					$("#banner_id").val('').change();
					$("#expired").val('');
					$("#weight").val('');
					$("#btn_save").html('Add new');
				});
			}
			function render_ads(id) {
				data = '';
				$.post("<?=current_url();?>/get", {id:id},
				function(data,status){
					$("#id").val(data.id);
					$("#zone_id").val(data.zone_id).change();
					$("#banner_id").val(data.banner_id).change();
					if(data.expired>0) {
						var date = new Date(data.expired*1000);
						var year = date.getFullYear();
						var month = date.getMonth()+1;
						var date = date.getDate();
						$("#expired").val(date+"/"+month+"/"+year).change();
					} else $("#expired").val('');
					$("#weight").val(data.weight);
					$("#btn_save").html('Update');
				});
			}
			function clearForm() {
					$("#id").val(0);
					$("#zone_id").val('').change();
					$("#banner_id").val('').change();						
					$("#expired").val('').change();
					$("#weight").val('');
					$("#btn_save").val('Save');
			}
			function generate_code() {
				var image = $('#image').val();
				var link = $('#link').val();
				if(image && link) {
					var img = new Image();
					img.src = image;
					img.onload = function(){
						$('#width').val(this.width);
						$('#height').val(this.height);
					};
					$("#code").val('<a href="'+link+'" target="_blank"><img src="'+image+'" /></a>');
				}
			}
		</script>
<?=$this->endSection();?>