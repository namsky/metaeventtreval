<div style="max-width: 450px;margin:0 auto">
	<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td style="border-bottom:1px solid #e6e6e6;font-size:18px;padding:28px 0"><?=isset($action)?mb_strtoupper($action):''?></td>
			</tr>
			<tr>
				<td style="font-size:14px;line-height:30px;padding:20px 0;color:#666">
					Chào mừng bạn đến với <strong><?=isset($site_name)?$site_name:''?></strong>!<br />
					Tên tài khoản: <strong><?=isset($username)?$username:''?></strong><br />
					Ấn vào link bên dưới để <?=isset($action)?$action:''?>:
				</td>
			</tr>
			<tr>
				<td style="padding:5px 0; text-align:center;">
					<a href="<?=isset($link)?$link:''?>" style="padding:10px 28px;background:#34495f;color:#fff;text-decoration:none" target="_blank"><span class="il"><?=isset($text_button)?mb_strtoupper($text_button):''?></span></a>
				</td>
			</tr>
			<tr>
				<td style="padding:20px 0 0 0;line-height:26px;color:#666">Nếu <?=isset($action)?$action:''?> không thành công vui lòng liên với quản trị.</td>
			</tr>
			<tr>
				<td>
					<a style="color:#34495f" href="<?=isset($url)?$url:''?>" target="_blank"><?=isset($url)?$url:''?></a>
				</td>
			</tr>
			<tr>
				<td style="padding:30px 0 15px 0;font-size:12px;color:#999;line-height:20px; text-align:center"><?=isset($site_name)?$site_name:''?> Team<br />Tin nhắn tự động. Vui lòng không phải hồi.</td>
			</tr>
		</tbody>
	</table>
</div>