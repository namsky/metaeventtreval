<?=$this->section('content');?>
<?php
    $base_url = isset($current_menu->id)?rebuild_url(1):current_url();
    $root_url = isset($current_menu->id)?rebuild_url(2):rebuild_url(1);
    function show_menu($data) {
		$code = '<ol class="dd-list">';
		if(is_array($data) && count((array)$data))
		foreach ($data as $item) {
			$code .= '<li id="item-'.$item->id.'" class="dd-item" data-id="'.$item->id.'">';
				$code .= '<div class="dd-handle">'.clearString($item->title, false).'</div>';
				$code .= '<div class="dd-actions">';
				$code .= '<span>'.$item->type.'</span>';
				$code .= '<a class="edit p-1" href="javascript:;" onclick="render('.$item->id.');" data-toggle="modal" data-target="#modal-item"><i class="fa fa-edit"></i></a>';
				$code .= '<a class="delete p-1 text-red" href="javascript:;" onclick="remove('.$item->id.');"><i class="fa fa-times-circle"></i></a></div>';
				if(is_array($item->child) && count($item->child)) $code .= show_menu($item->child);
			$code .= '</li>';
		}
		$code .= '</ol>';
		return $code;
	}
?>
		<!-- Header -->
        <div class="header pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb">
                                    <li class="breadcrumb-item"><a href="<?=site_url('v-manager');?>"><i class="fas fa-home"></i> <?=lang('Dashboards');?></a></li>
                                    <li class="breadcrumb-item" aria-current="page">
										<a href="<?=$root_url;?>/menus">Menus</a>
									</li>
                                    <li class="breadcrumb-item active" aria-current="page">
										<?=!empty($current_menu->name)?strtoupper($current_menu->name):'NEW';?>
									</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
			<div class="row">
				<div class="col">
					<div class="card">
						<!-- Card header -->
						<div class="card-header border-0">
							<div class="float-left">
                                <a href="<?=$root_url;?>/menus" class="btn btn-primary btn-sm"><i class="fas fa-angle-left"></i> <?=lang('Back');?></a>
                            </div>
						</div>
						<div class="card-content row px-3 pb-3">
							<div class="col-md-12">
                                <div class="form-group m-3">
                                    <label for="icon" class="form-control-label"><?=cms_lang('Tên Menu');?></label>
                                    <input class="form-control" type="text" value="<?=!empty($current_menu->name)?$current_menu->name:''?>" id="menu_name" name="menu_name" disabled>
                                </div>
                                <hr class="my-2" />
                                <div class="text-center p-3 m-2">
                                    MENU ITEMS
                                </div>
								<div class="dd nestable m-3">
									<?=!empty($items)?show_menu($items):'';?>
                                    <div class="form-group mt-3">
                                        <button type="button" style="width: 100%;" id="btn_new" class="btn btn-primary" onClick="render_item();" data-toggle="modal" data-target="#modal-item"><?=lang('New Item');?></button>
                                    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade cms_box" id="modal-item" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form id="form-item">
						<div class="modal-header">
							<h5 class="modal-title">Menu Item</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
                            <div class="row m-0">
                                <input class="form-control form-control-sm" type="hidden" value="" id="id" name="id">
                                <input class="form-control form-control-sm" type="hidden" value="<?=!empty($menu_id)?$menu_id:'';?>" id="menu_id" name="menu_id">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="type" class="form-control-label"><?=lang('Type');?></label>
                                        <select class="form-control form-control-sm" id="type" name="type" onChange="updateEvent()">
                                            <option value="" selected>-</option>
                                            <option value="post">Post</option>
                                            <option value="page">Page</option>
                                            <option value="category">Category</option>
                                            <option value="link">Custom Link</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 custom-value" id="category-box">
                                    <div class="form-group">
                                        <label for="category" class="form-control-label"><?=lang('Category');?></label>
                                        <select class="form-control form-control-sm" id="category" name="category">
                                        <?=!empty($categories)?$cms->dropdown_categories($categories):'';?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 custom-value" id="post-box" style="display:none">
                                    <div class="form-group">
                                        <label for="post" class="form-control-label"><?=lang('Post');?></label>
                                        <select class="form-control post" name="post" id="post"></select>
                                        <script>
                                            $('.post').select2({
                                                ajax: {
                                                    url: '<?=$root_url?>/posts/search_posts',
                                                    results: function (data) {
                                                        return { results: data.items };
                                                    },
                                                    cache: true,
                                                    dataType: 'json'
                                                }
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-6 custom-value" id="page-box" style="display:none">
                                    <div class="form-group">
                                        <label for="page" class="form-control-label"><?=lang('Page');?></label>
                                        <select class="form-control page" name="page" id="page"></select>
                                        <script>
                                            $('.page').select2({
                                                ajax: {
                                                    url: '<?=$root_url?>/posts/search_pages',
                                                    results: function (data) {
                                                        return { results: data.items };
                                                    },
                                                    cache: true,
                                                    dataType: 'json'
                                                }
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-6 custom-value" id="link-box" style="display:none">
                                    <div class="form-group">
                                        <label for="link" class="form-control-label"><?=lang('Link');?></label>
                                        <input class="form-control form-control-sm" type="text" value="" id="link" name="link">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="parent" class="form-control-label"><?=lang('Parent');?></label>
                                        <select class="form-control form-control-sm" id="parent" name="parent">
                                        <?=!empty($items)?$cms->dropdown_menus($items):'';?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="icon" class="form-control-label"><?=lang('Icon');?></label>
                                        <input class="form-control form-control-sm" type="text" value="" id="icon" name="icon">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="title" class="form-control-label"><?=lang('Title');?></label>
                                        <input class="form-control form-control-sm" type="text" value="" id="title" name="title">
                                    </div>
                                </div>
                            </div>
						</div>
						<div class="modal-footer">
							<button type="button" id="btn_save" class="btn btn-sm btn-primary" onClick="save();"><?=lang('Save');?></button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
		$args = array(
			'base_url' => $base_url,
			'field_slug' => 'name',
			'render' => false,
			'save' => true,
		);
		$cms->gen_js($args);
		?>
		<script>
			function render(id) {
				$('#form-item')[0].reset();
				if(id) {
					$.ajax({url: "<?=$base_url;?>/get_data?item_id="+id, success: function(result){
						$("#id").val(parseInt(result.id));
						$("#menu_id").val(parseInt(result.menu_id));
						$("#parent").val(result.parent).change();
						$("#type").val(result.type).change();
                        $("#post").html('');
                        $("#page").html('');
                        if(result.type == 'category') {
						    $("#category").val(result.value).change();
                        }
                        else if(result.type == 'post') {
                            if(typeof(result.options) !== 'undefined') {
                                var options = '<option value="'+result.options.id+'">'+result.options.title+'</option>';
						        $("#post").append(options);
                            }
						    $("#post").val(result.value).change();
                        }
                        else if(result.type == 'page') {
                            if(typeof(result.options) !== 'undefined') {
                                var options = '<option value="'+result.options.id+'">'+result.options.title+'</option>';
						        $("#page").append(options);
                            }
						    $("#page").val(result.value).change();
                        } else {
						    $("#link").val(result.value);
                        }
						$("#title").val(result.title);
						$("#parent").val(parseInt(result.parent));
						$("#order").val(parseInt(result.order));
						var mega = parseInt(result.mega);
						$("#mega").attr("checked", !!mega);
					}, dataType: "json"});
					$("#btn_item_save").html("Update");
				} else {
					$("#id").val('');
					$("#btn_item_save").html("Save");
					$("#mega").attr("checked", false);
				}
				updateEvent();
			}
			function updateEvent() {
				$("#title").val('');
                var type = $("#type").val();
                switch(type) {
                    case 'category':
                        $(".custom-value").hide();
                        $("#category-box").show();
                        break;
                    case 'post':
                        $(".custom-value").hide();
                        $("#post-box").show();
                        break;
                    case 'page':
                        $(".custom-value").hide();
                        $("#page-box").show();
                        break;
                    default:
                        $(".custom-value").hide();
                        $("#link-box").show();
                        break;
                }
			}
			$(document).ready(function(e) {
				$('#category').on('change', function() {
					var category = $('#category option:selected').text();
					$("#title").val(category);
				});
				if ($('.nestable').length && $.fn.nestable) {
					$('.nestable').nestable();
				}
				$('.dd').on('change', function() {
					var data = $('.nestable').nestable('serialize');
					data = window.JSON.stringify(data);
					$.post("<?=$base_url;?>/update", {data:data}, function(data,status){});
				});
			});
		</script>
<?=$this->endSection();?>