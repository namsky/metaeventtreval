<?php namespace App\Modules\jnews\Widgets;
use App\Core\Cms\CmsWidget;

class Box_news extends CmsWidget
{
    function index($args=[]) {
		/* Set box title */
		if(isset($args['title'])) $this->view->setVar('title', $args['title']);
		if(isset($args['title_heading'])) $this->view->setVar('title_heading', $args['title_heading']);
		if(isset($args['category_heading'])) $this->view->setVar('category_heading', $args['category_heading']);
		if(isset($args['post_heading'])) $this->view->setVar('post_heading', $args['post_heading']);
		/* Load from cache */
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('box_'.md5(serialize($args))):false;
		$category = $cached?$this->cache->get('box_category_'.md5(serialize($args))):false;
		$tags = $cached?$this->cache->get('box_tags_'.md5(serialize($args))):false;
		if(!$items) {
			/* Load from db */
			$model = model('App\Modules\Cms\Models\PostModel');
			$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
			if(!isset($args['type'])) $args['type'] = 'blog';
			if(isset($args['views']) && $args['views'] == 1) $model->with('user', ['fields'=>'id,username,name'])->with('relations')->orderBy('views', 'DESC');
			else $model->with('user', ['fields'=>'id,username,name'])->with('relations')->orderBy('published', 'DESC');
			/* Excludes IDs Post */
			if(isset($args['excludes'])) {
				if(is_numeric($args['excludes'])) $args['excludes'] = [$args['excludes']];
				if(is_array($args['excludes'])) {
					$model->whereNotIn('id', $args['excludes']);
				}
			}
			/* Filter by category */
			if(isset($args['category'])) {
				$cat_id = intval($args['category']);
				$category = $categoryModel->find($cat_id);
				$sub_categories = $categoryModel->get_list($cat_id);
				$this->cache->save('box_category_'.md5(serialize($args)), $category, 86400);
				$model->categories([$cat_id]);
			}
			/* Filter by tags */
			if(isset($args['tags'])) {
				if(is_numeric($args['tags'])) $args['tags'] = [$args['tags']];
				if(is_array($args['tags'])) {
					$this->cache->save('box_tags_'.md5(serialize($args)), $category, 86400);
					$model->tags($args['tags']);
				}
			}
			/* Filter by type -1 - Page, 1 - News, 2 - Video */
			if(isset($args['post_type'])) {
				$type = intval($args['post_type']);
				$model->where('type', $type);
			} else {
                $model->where('type>', 0);
            }
			/* Filter by HOT */
			if(isset($args['hot'])) {
				$hot = intval($args['hot']);
				$model->where('hot', $hot);
			}
			if(isset($args['recommend'])) {
				$recommend = intval($args['recommend']);
				$model->where('recommend', $recommend);
			}
			$model->where('status', 1);
			$model->where('published<', time());
			if($args['type'] == 'carousel_posts') {
				$model->where('is_slider', 1);
			}
			/* Custom limit */
			if(isset($args['limit'])) {
				$limit = intval($args['limit']);
			}
			/* FindAll item */
			$items = $model->findAll($limit, 0);
			$this->cache->save('box_'.md5(serialize($args)), $items, 86400);
		}
		$this->view->setVar('items', $items);
		if(isset($category) && $category) $this->view->setVar('category', $category);
		if(isset($sub_categories) && $sub_categories) $this->view->setVar('sub_categories', $sub_categories);
		return $this->view->render('box_'.$args['type']);
    }
}