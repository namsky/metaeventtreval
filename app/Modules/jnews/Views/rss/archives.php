<?='<?xml version="1.0" encoding="UTF-8" ?>';?>

<rss version="2.0"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:webfeeds="http://webfeeds.org/rss/1.0"
	xmlns:media="http://search.yahoo.com/mrss/">
	<channel>
		<title><?=isset($title)?$title:''?></title>
		<link><?=URL?>/rss-channels/</link>
		<description>
			<![CDATA[<?=isset($description)?$description:''?>]]>
		</description>
		<atom:link href="<?=current_url();?>" rel="self"></atom:link>
		<copyright><?=cms_config('site_name')?></copyright>
		<language>vi-vn</language>
		<lastBuildDate><?=date("Y-m-d\TG:i:sT:00", time())?></lastBuildDate>
		<!-- 2020-07-06T14:33:28+07:00 -->
		<?php
		if(isset($items) && count($items)) {
			foreach($items as $item) {
		?>
		<item>
			<title>
				<![CDATA[<?=$item->title?>]]>
			</title>
			<link><?=post_url($item)?></link>
			<guid isPermaLink="true"><?=post_url($item)?></guid>
			<description>
				<![CDATA[<?=$item->summary?>]]>
			</description>
			<dc:creator
				xmlns:dc="http://purl.org/dc/elements/1.1/">Anony Stick
			</dc:creator>
			<pubDate><?=date("Y-m-d G:i:s", $item->published)?></pubDate>
		</item>
		<?php
			}
		}
		?>
	</channel>
</rss>