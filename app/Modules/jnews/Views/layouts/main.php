<!doctype html>
<html class="no-js" lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name='viewport' content='width=device-width, initial-scale=1, user-scalable=yes' />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<link rel="pingback" href="https://jnews.io/default/xmlrpc.php" />
	<meta name="theme-color" content="#020202">
	<meta name="msapplication-navbutton-color" content="#020202">
	<meta name="apple-mobile-web-app-status-bar-style" content="#020202">
	<title><?=isset($title)?$title:cms_config('site_name');?></title>
	<meta name="description" content="<?=isset($description)?$description:cms_config('site_desc');?>">
	<meta name="keywords" content="<?=isset($keywords)?$keywords:'';?>">
	<meta property="og:type" content="website">
	<meta property="og:site_name" content="<?=cms_config('site_name');?>">
	<meta property="og:title" content="<?=isset($title)?$title:cms_config('site_name');?>">
	<meta property="og:url" content="<?=current_url();?>">
	<meta property="og:description" content="<?=isset($description)?$description:cms_config('site_desc');?>">
	<meta property="og:url" content="https://jnews.io/default">
	<meta property="og:locale" content="en_US">
	<link rel="icon" href="<?=CDN?>/themes/jnews/images/cropped-favicon-32x32.png" sizes="32x32" />
	<link rel="icon" href="<?=CDN?>/themes/jnews/images/cropped-favicon.png" sizes="192x192" />
	<link rel="apple-touch-icon" href="<?=CDN?>/themes/jnews/images/cropped-favicon.png" />
	<meta name="msapplication-TileImage" content="<?=CDN?>/themes/jnews/images/cropped-favicon.png" />
	<script>
		var base_url = "<?=URL?>";
	</script>
	<?php if(isset($page) && $page == 'single') { ?>
		<link href="<?=CDN?>/themes/jnews/css/single_main.css" rel="stylesheet">
	<?php } else { ?>
		<link href="<?=CDN?>/themes/jnews/css/main.css" rel="stylesheet">
	<?php } ?>
	<?=load_js('core.js', 'themes/jnews'); ?>
</head>

<body class="home page-template page-template-template-builder page-template-template-builder-php page page-id-15 wp-embed-responsive jeg_toggle_dark jnews jsc_normal wpb-js-composer js-comp-ver-6.7.0 vc_responsive">
	<div class="jeg_viewport">
		<div class="jeg_header_wrapper">
			<div class="jeg_header_instagram_wrapper"></div>
			<div class="jeg_header normal">
				<div class="jeg_topbar jeg_container dark">
					<div class="container">
						<div class="jeg_nav_row">
							<div class="jeg_nav_col jeg_nav_left  jeg_nav_grow">
								<div class="item_wrap jeg_nav_alignleft">
									<div class="jeg_nav_item">
										<ul class="jeg_menu jeg_top_menu">
											<li id="menu-item-184" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184"><a href="mailto:<?=cms_config('email')?>"><i class="fa fa-envelope-o"></i> <?=cms_config('email')?></a></li>
											<li id="menu-item-186" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186"><a href="tel:<?=cms_config('phone')?>"><i class="fa fa-phone"></i> <?=cms_config('phone')?></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="jeg_nav_col jeg_nav_center  jeg_nav_normal">
								<div class="item_wrap jeg_nav_aligncenter"></div>
							</div>
							<div class="jeg_nav_col jeg_nav_right  jeg_nav_normal">
								<div class="item_wrap jeg_nav_alignright">
									<div class="jeg_nav_item socials_widget jeg_social_icon_block nobg"> <a href="<?=cms_config('fanpage')?>" target='_blank' rel='external noopener nofollow' class="jeg_facebook"><i class="fa fa-facebook"></i> </a><a href="<?=cms_config('youtube')?>" target='_blank' rel='external noopener nofollow' class="jeg_youtube"><i class="fa fa-youtube-play"></i> </a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="jeg_midbar jeg_container normal">
					<div class="container">
						<div class="jeg_nav_row">
							<div class="jeg_nav_col jeg_nav_left jeg_nav_normal">
								<div class="item_wrap jeg_nav_alignleft">
									<div class="jeg_nav_item jeg_logo jeg_desktop_logo">
										<h1 class="site-title"> <a href="/" style="padding:0"> <img class='jeg_logo_img' src="<?=CDN?>/themes/jnews/images/logo.png"><span style="border:0;padding:0;margin:0;position:absolute!important;height:1px;width:1px;overflow:hidden;clip:rect(1px 1px 1px 1px);clip:rect(1px,1px,1px,1px);-webkit-clip-path:inset(50%);clip-path:inset(50%);white-space:nowrap">Meta Event Travel</span> </a></h1></div>
								</div>
							</div>
							<div class="jeg_nav_col jeg_nav_center jeg_nav_normal">
								<div class="item_wrap jeg_nav_aligncenter"></div>
							</div>
							<div class="jeg_nav_col jeg_nav_right jeg_nav_grow">
								<div class="item_wrap jeg_nav_alignright">
									<div class="jeg_nav_item jeg_ad jeg_ad_top jnews_header_ads">
										<div class='ads-wrapper  '>
											<a href='#' rel="noopener" class='adlink ads_image '> <img src='data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==' class='lazyload' data-src='<?=CDN?>/themes/jnews/images/729x90_3.png' alt='Advertisement Banner' data-pin-no-hover="true"> </a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php
					$args = [];
					$args['type'] = 'main';
					echo widget('Cms/Main_menu', $args);
				?>
			</div>
		</div>
		<?php
			$args = [];
			$args['type'] = 'sticky';
			echo widget('Cms/Main_menu', $args);
		?>
		<div class="jeg_navbar_mobile_wrapper">
			<div class="jeg_navbar_mobile" data-mode="scroll">
				<div class="jeg_mobile_bottombar jeg_mobile_midbar jeg_container dark">
					<div class="container">
						<div class="jeg_nav_row">
							<div class="jeg_nav_col jeg_nav_left jeg_nav_normal">
								<div class="item_wrap jeg_nav_alignleft">
									<div class="jeg_nav_item"> <a href="#" class="toggle_btn jeg_mobile_toggle"><i class="fa fa-bars"></i></a></div>
								</div>
							</div>
							<div class="jeg_nav_col jeg_nav_center jeg_nav_grow">
								<div class="item_wrap jeg_nav_aligncenter">
									<div class="jeg_nav_item jeg_mobile_logo">
										<div class="site-title">
											<a href="/"> <img class='jeg_logo_img' src="<?=CDN?>/themes/jnews/images/logo.png"> </a>
										</div>
									</div>
								</div>
							</div>
							<div class="jeg_nav_col jeg_nav_right jeg_nav_normal">
								<div class="item_wrap jeg_nav_alignright">
									<div class="jeg_nav_item jeg_search_wrapper jeg_search_popup_expand"> <a href="#" class="jeg_search_toggle"><i class="fa fa-search"></i></a>
										<form action="https://jnews.io/default/" method="get" class="jeg_search_form" target="_top">
											<input name="s" class="jeg_search_input" placeholder="Search..." type="text" value="" autocomplete="off">
											<button aria-label="Search Button" type="submit" class="jeg_search_button btn"><i class="fa fa-search"></i></button>
										</form>
										<div class="jeg_search_result jeg_search_hide with_result">
											<div class="search-result-wrapper"></div>
											<div class="search-link search-noresult"> No Result</div>
											<div class="search-link search-all-button"> <i class="fa fa-search"></i> View All Result</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="jeg_navbar_mobile_menu">
					<div class="container">
						<div class="menu-main-menu-container">
							<ul id="menu-main-menu" class="jeg_mobile_menu_style_1">
								<?php
									$args = [];
									$args['type'] = 'mobile';
									echo widget('Cms/Main_menu', $args);
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="sticky_blankspace" style="height: 60px;"></div>
		</div>
		<?=$this->renderSection('content')?>
		<div class="footer-holder" id="footer" data-id="footer">
			<div class="jeg_footer jeg_footer_1 dark">
				<div class="jeg_footer_container jeg_container">
					<div class="jeg_footer_content">
						<div class="container">
							<div class="row">
								<div class="jeg_footer_primary clearfix">
									<div class="col-md-4 footer_column">
										<div class="footer_widget widget_jnews_about" id="jnews_about-2">
											<div class="jeg_about ">
												<a class="footer_logo" href="/"> <img class='lazyload' src="<?=CDN?>/themes/jnews/images/logo.png"> </a>
												<p>We bring you the best Premium WordPress Themes that perfect for news, magazine, personal blog, etc. Check our landing page for details.</p>
											</div>
										</div>
										<div class="footer_widget widget_jnews_social" id="jnews_social-2">
											<div class="jeg_social_wrap ">
												<p> <strong>Kết nối với chúng tôi</strong></p>
												<div class="socials_widget   rounded">
													<a href="https://www.facebook.com/jegtheme/" target="_blank" rel="external noopener nofollow" class="jeg_facebook"> <i class="fa fa-facebook"></i> </a>
													<a href="https://twitter.com/jegtheme" target="_blank" rel="external noopener nofollow" class="jeg_twitter"> <i class="fa fa-twitter"></i> </a>
													<a href="https://www.instagram.com/jegtheme/" target="_blank" rel="external noopener nofollow" class="jeg_instagram"> <i class="fa fa-instagram"></i> </a>
													<a href="https://www.behance.net/jegtheme" target="_blank" rel="external noopener nofollow" class="jeg_behance"> <i class="fa fa-behance"></i> </a>
													<a href="https://dribbble.com/jegtheme" target="_blank" rel="external noopener nofollow" class="jeg_dribbble"> <i class="fa fa-dribbble"></i> </a>
												</div>
												<style scoped></style>
											</div>
										</div>
									</div>
									<?=widget('Cms/Footer_menu');?>
									<div class="col-md-4 footer_column">
										<div class="footer_widget widget_mc4wp_form_widget" id="mc4wp_form_widget-3">
											<div class="jeg_footer_heading jeg_footer_heading_1">
												<h3 class="jeg_footer_title"><span>Liên hệ với chúng tôi</span></h3>
											</div>
											<form id="mc4wp-form-2" class="mc4wp-form mc4wp-form-68" method="post" data-name="Contact Forms">
												<div class="mc4wp-form-fields">
													<p><input type="text" name="name" placeholder="Tên của bạn" required /></p>
													<p><input type="email" name="email" placeholder="Địa chỉ Email" required /></p>
													<p><input type="submit" value="Gửi" /></p>
													<p><small>*Vui lòng không Spam</small></p>
												</div>
												<div class="mc4wp-response"></div>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div class="jeg_footer_secondary clearfix">
								<div class="footer_right">
									<ul class="jeg_menu_footer">
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-184"><a href="#">About</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-186"><a href="#">Advertise</a></li>
										<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-185"><a href="#">Privacy &#038; Policy</a></li>
										<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-558"><a href="https://jnews.io/default/contact-us/">Contact Us</a></li>
									</ul>
								</div>
								<p class="copyright"> &copy; 2022 <a href="http://jegtheme.com" title="MetaEventTravel">MetaEventTravel</a> - Website được thiết kế bởi: <a href="https:net5s.vn" title="Net5s Media">Net5s</a>.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="jscroll-to-top"> <a href="#back-to-top" class="jscroll-to-top_link"><i class="fa fa-angle-up"></i></a></div>
	</div>
	<div id="jeg_off_canvas" class="normal"> <a href="#" class="jeg_menu_close"><i class="jegicon-cross"></i></a>
		<div class="jeg_bg_overlay"></div>
		<div class="jeg_mobile_wrapper">
			<div class="nav_wrap">
				<div class="item_main">
					<div class="jeg_aside_item jeg_search_wrapper jeg_search_no_expand round"> <a href="#" class="jeg_search_toggle"><i class="fa fa-search"></i></a>
						<form action="https://jnews.io/default/" method="get" class="jeg_search_form" target="_top">
							<input name="s" class="jeg_search_input" placeholder="Search..." type="text" value="" autocomplete="off">
							<button aria-label="Search Button" type="submit" class="jeg_search_button btn"><i class="fa fa-search"></i></button>
						</form>
						<div class="jeg_search_result jeg_search_hide with_result">
							<div class="search-result-wrapper"></div>
							<div class="search-link search-noresult"> No Result</div>
							<div class="search-link search-all-button"> <i class="fa fa-search"></i> View All Result</div>
						</div>
					</div>
					<div class="jeg_aside_item">
						<ul class="jeg_mobile_menu">
							<?php
								$args = [];
								$args['type'] = 'mobile';
								echo widget('Cms/Main_menu', $args);
							?>
						</ul>
					</div>
				</div>
				<div class="item_bottom">
					<div class="jeg_aside_item socials_widget nobg"> <a href="https://facebook.com" target='_blank' rel='external noopener nofollow' class="jeg_facebook"><i class="fa fa-facebook"></i> </a><a href="https://twitter.com" target='_blank' rel='external noopener nofollow' class="jeg_twitter"><i class="fa fa-twitter"></i> </a><a href="https://www.instagram.com/jegtheme/" target='_blank' rel='external noopener nofollow' class="jeg_instagram"><i class="fa fa-instagram"></i> </a><a href="https://youtube.com" target='_blank' rel='external noopener nofollow' class="jeg_youtube"><i class="fa fa-youtube-play"></i> </a><a href="https://jnews.io/default/feed/" target='_blank' rel='external noopener nofollow' class="jeg_rss"><i class="fa fa-rss"></i> </a></div>
					<div class="jeg_aside_item jeg_aside_copyright">
						<p>&copy; 2022 <a href="http://jegtheme.com" title="">MetaEventTravel</a> - Website được thiết kế bởi: <a href="https:net5s.vn" title="Net5s Media">Net5s</a>.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type='text/javascript' id='jnews-main-js-extra'>
	var jnewsoption = {
		"popup_script": "magnific",
		"single_gallery": "",
		"ismobile": "",
		"isie": "",
		"sidefeed_ajax": "",
		"language": "en_US",
		"module_prefix": "jnews_module_ajax_",
		"live_search": "1",
		"postid": "0",
		"isblog": "",
		"admin_bar": "0",
		"follow_video": "",
		"follow_position": "top_right",
		"rtl": "0",
		"gif": "1",
		"lang": {
			"invalid_recaptcha": "Invalid Recaptcha!",
			"empty_username": "Please enter your username!",
			"empty_email": "Please enter your email!",
			"empty_password": "Please enter your password!"
		},
		"recaptcha": "0",
		"site_slug": "\/default\/",
		"zoom_button": "0"
	};
	</script>
	<?=load_js('main.js', 'themes/jnews'); ?>
</body>

</html>