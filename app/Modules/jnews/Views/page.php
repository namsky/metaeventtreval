<?=$this->section('content');?>
	<!-- CONTENT -->
	<div class="single-news">
		<div class="container">
			<div class="row">
				<div class="header col-12 mb-4">
					<?=$cms->schemas('item_page', ['url'=>post_url($item),'title'=>$item->title]);?>
					<h1 class="title"><?=isset($item->title)?$item->title:''?></h1>
				</div>
				<div class="col-sm-12">
					<?=($item->is_adsense)?widget('Cms/Ads', ['zone_id'=>8]):'';?>
					<div class="single-content">
						<?php
							$content = $item->content;
							$cdn = cdn_url();
							if($cdn) {
								$content = preg_replace('/<img(.*?)src=\"http:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"https:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"(.*?)\"/', '<img$1src="'.$cdn.'$2"', $content);
								$content = preg_replace('/&nbsp;/', ' ', $content);
							}
							if(cms_config('lazy_load')) {
								$content = preg_replace('/<img(.*?)class=\"(.*?)\"/', '<img$1', $content);
								$content = preg_replace('/<img(.*?)src=/', '<img src="'.CDN.'/images/blank.gif" class="lazy"$1data-src=', $content);
							}
							if($item->is_adsense) {
								$ads = widget('Cms/Ads', ['zone_id'=>7]);
								$count = substr_count($content,"</p>");
								$pos = round($count/2);
								$ads_pos = 0;
								if($pos) {
									$i = 0;
									while($i<$pos) {
										$ads_pos = strpos($content, "</p>", $ads_pos);
										$ads_pos = $ads_pos + 4;
										$i++;
									}
								}
								if($ads_pos < strlen($content)) {
									$content_first = substr($content, 0, $ads_pos);
									$content_last = substr($content, $ads_pos+1);
									echo $content_first;
									echo $ads;
									echo $content_last;
								} else echo $content;
							} else echo $content;
						?>
					</div>
					<?php if($item->faqs) { ?>
					<div class="single-faqs mb-3">
						<div class="faq-title">Câu hỏi thường gặp</div>
						<?php
						foreach($item->faqs as $faq) {
						?>
						<div class="faq-items">
							<a class="faq-question collapsed" data-toggle="collapse" href="#faq-<?=$faq->id?>" aria-expanded="false" aria-controls="faq-<?=$faq->id?>">
								<h3><?=$faq->question?></h3>
								<i class="fas fa-angle-down rotate-icon"></i>
							</a>
						</div>
						<div class="collapse faq-content" id="faq-<?=$faq->id?>">
							<p><?=$faq->answer?></p>
						</div>
						<?php
						}
						echo $cms->schemas('faq', ['faqs' => $item->faqs]);
						?>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>