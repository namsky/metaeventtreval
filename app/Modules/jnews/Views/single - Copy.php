<?=$this->section('content');?>
    <div class="archives post post1 padding-top-30">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-8">
                    <div class="row">
                        <div class="col-4 align-self-center">
                            <div class="page_category">
                                <?php
                                if(isset($item->categories)) {
                                    $categories = $item->categories;
                                    $category = reset($categories);
                                    if(isset($category->id)) {
                                        echo '<h4><a href="'.category_url($category).'">'.$category->name.'</a></h4>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-8 text-right">
                            <div class="page_comments">
                                <ul class="inline">
                                    <li><i class="fas fa-eye"></i><?=isset($item->views)?$item->views:'0'?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="space-30"></div>
                    <div class="single_post_heading">
                        <h1><?=isset($item->title)?$item->title:''?></h1>
                        <div class="space-10"></div>
                        <p><?=isset($item->summary)?$item->summary:''?></p>
                    </div>
                    <div class="space-20"></div>
                    <div class="row">
                        <div class="col-lg-6 align-self-center">
                            <div class="author">
                                <div class="author_img">
                                    <div class="author_img_wrap">
                                        <img src="<?=!empty($item->user->avatar)?show_thumb($item->user->avatar,40,40):CDN.'/images/no-avatar.jpg'?>" alt="">
                                    </div>
                                </div>
                                <a href="javascript:;" title="" rel="author">
                                    <?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?>
                                </a>
                                <ul>
                                    <li>
                                        <time datetime="<?=date("Y-m-d", $item->published)?>"><?=show_date($item->published)?></time>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 align-self-center">
                            <div class="author_social inline text-right">
                                <ul>
                                    <li>
                                        <a href="#" onclick="window.open('http://www.facebook.com/sharer.php?t=<?=isset($item->title)?$item->title:''?>&amp;u=<?=post_url($item);?>', 'facebookShare', 'width=626,height=436'); return false;" title="Share on Facebook"><i class="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" onclick="window.open('http://twitter.com/share?text=<?=isset($item->title)?$item->title:''?>&amp;url=<?=post_url($item);?>', 'twitterShare', 'width=626,height=436'); return false;" title="Tweet This Post"><i class="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#comments"><i class="far fa-comment"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="space-20"></div>
                    <div class="content">
						<?php
							$content = $item->content;
							$cdn = cdn_url();
							if($cdn) {
								$content = preg_replace('/<img(.*?)src=\"http:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"https:\/\/(.*?)\"/', '<img$1src="$2"', $content);
								$content = preg_replace('/<img(.*?)src=\"(.*?)\"/', '<img$1src="'.$cdn.'$2"', $content);
								$content = preg_replace('/&nbsp;/', ' ', $content);
							}
							if(cms_config('lazy_load')) {
								$content = preg_replace('/<img(.*?)class=\"(.*?)\"/', '<img$1', $content);
								$content = preg_replace('/<img(.*?)src=/', '<img src="'.CDN.'/images/blank.gif" class="lazy"$1data-src=', $content);
							}
							if($item->is_adsense) {
								$ads = widget('Cms/Ads', ['zone_id'=>7]);
								$count = substr_count($content,"</p>");
								$pos = round($count/2);
								$ads_pos = 0;
								if($pos) {
									$i = 0;
									while($i<$pos) {
										$ads_pos = strpos($content, "</p>", $ads_pos);
										$ads_pos = $ads_pos + 4;
										$i++;
									}
								}
								if($ads_pos < strlen($content)) {
									$content_first = substr($content, 0, $ads_pos);
									$content_last = substr($content, $ads_pos+1);
									echo $content_first;
									echo $ads;
									echo $content_last;
								} else echo $content;
							} else echo $content;
						?>
                    </div>
                    
                    <div class="space-40"></div>

					<?php if($item->tags) { ?>
                    <div class="tags">
                        <ul class="inline">
                            <li class="tag_list"><i class="fas fa-tag"></i> TAGS</li>
                            <?php
                            $tags = [];
                            foreach($item->tags as $tag) {
                                $tags[] = $tag->id;
                            ?>
                                <li><a href="<?=tag_url($tag);?>"><?=$tag->name;?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
					<?php } ?>
					<?=($item->is_adsense)?widget('Cms/Ads', ['zone_id'=>9]):'';?>
					<?php if($item->tags) { ?>
                    <div class="space-40"></div>
                    <div class="border_black"></div>
                    <div class="space-40"></div>
					<?=widget('evworld/Box_news', ['type'=>'single_related', 'limit' => 2, 'post_heading' => 'h4', 'tags'=>$tags, 'excludes' => $item->id]) ?>
					<?php } ?>
					<hr>
					<div class="fb-comments" data-href="<?=post_url($item)?>" data-width="100%" data-numposts="5"></div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <?php include_once('sidebar.php');?>
                </div>
            </div>
        </div>
    </div>
    <div class="space-60"></div>
    <?=widget('evworld/Box_news', ['type'=>'lastnews', 'limit'=>3])?>
    <div class="space-60"></div>
<?=$this->endSection();?>