	<?php
		if(isset($items) && is_array($items) && count($items)) {
	?>
		<?php foreach($items as $key=>$item) {
			if($key == 0) { ?>
			<div class="jeg_postblock_5 jeg_postblock jeg_module_hook jeg_pagination_disable jeg_col_2o3 jnews_module_15_2_62047a260f148">
				<div class="jeg_block_heading jeg_block_heading_5 jeg_subcat_right">
					<h3 class="jeg_block_title"><span>Featured<strong>Stories</strong></span></h3></div>
				<div class="jeg_block_container">
					<div class="jeg_posts jeg_load_more_flag">
						<article class="jeg_post jeg_pl_lg_2 format-standard">
							<div class="jeg_thumb">
								<a href="<?=post_url($item)?>">
									<div class="thumbnail-container animate-lazy size-715 "><img width="350" height="250" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
								</a>
								<?php
									$categories = $item->categories;
									if(is_array($categories) && count($categories)) {
										$cate = reset($categories);
								?>
								<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
								<?php } ?>
							</div>
							<div class="jeg_postblock_content">
								<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
								<div class="jeg_post_meta">
									<div class="jeg_meta_author"><span class="by">by</span> <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></div>
									<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
									<div class="jeg_meta_comment"><a href="javascript:;"><i class="fa fa-eye"></i> <?=$item->views?> </a></div>
								</div>
								<div class="jeg_post_excerpt">
									<p>
										<?=isset($item->summary)?cutOf($item->summary, 120):cutOf(strip_tags($item->content), 120)?>
									</p> 
									<a href="<?=post_url($item)?>" class="jeg_readmore">Read more</a></div>
							</div>
						</article>
					</div>
				</div>
			</div>
		<?php } } ?>
	<div class="jeg_postblock_22 jeg_postblock jeg_module_hook jeg_pagination_loadmore jeg_col_2o3">
		<div class="jeg_block_container">
			<div class="jeg_posts_wrap">
				<div class="jeg_posts jeg_load_more_flag">
					<?php foreach($items as $key=>$item) {
						if($key > 0) { ?>
						<article class="jeg_post jeg_pl_md_5 format-standard">
							<div class="jeg_thumb">
								<a href="<?=post_url($item)?>">
									<div class="thumbnail-container animate-lazy size-715 "><img width="350" height="250" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
								</a>
								<?php
									$categories = $item->categories;
									if(is_array($categories) && count($categories)) {
										$cate = reset($categories);
								?>
								<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
								<?php } ?>
							</div>
							<div class="jeg_postblock_content">
								<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
								<div class="jeg_post_meta">
									<div class="jeg_meta_date"><a href="javascript:;"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
								</div>
							</div>
						</article>
					<?php } } ?>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>