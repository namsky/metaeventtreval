<?php
	if(isset($items) && is_array($items) && count($items) > 1) {
?>
	<div class="jegwidgetpopular">
		<?php foreach($items as $key=>$item) { ?>
		<div class="jeg_post jeg_pl_sm format-standard">
			<div class="jeg_thumb">
				<a href="<?=post_url($item)?>">
					<div class="thumbnail-container animate-lazy  size-715 "><img width="120" height="86" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="Why Millennials Need to Save Twice as Much as Boomers Did" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
				</a>
			</div>
			<div class="jeg_postblock_content">
				<h3 class="jeg_post_title"><a property="url" href="<?=post_url($item)?>"><?=$item->title?></a></h3>
				<div class="jeg_post_meta">
					<div class="jeg_meta_date"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></div>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
<?php } ?>