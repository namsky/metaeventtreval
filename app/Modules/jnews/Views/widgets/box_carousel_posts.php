	<?php
		if(isset($items) && is_array($items) && count($items)) {
	?>
	<div class="jeg_slider_wrapper jeg_slider_type_1_wrapper jnews_module_15_1_62047a2609c62  ">
		<div class="jeg_slider_type_1 jeg_slider" data-autoplay="" data-delay="3000" data-hover-action="">
			<?php foreach($items as $item) { ?>
			<div class="jeg_slide_item">
				<a href="<?=post_url($item)?>" class="jeg_slide_img">
					<div class="thumbnail-container size-500 "><img width="750" height="375" src="<?=$item->thumb?>" class="" alt="" /></div>
				</a>
				<div class="jeg_slide_caption">
					<div class="jeg_caption_container">
						<?php
							$categories = $item->categories;
							if(is_array($categories) && count($categories)) {
								$cate = reset($categories);
						?>
						<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
						<?php } ?>
						<h2 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h2>
						<div class="jeg_post_meta"><span class="jeg_meta_author">by <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></span><span class="jeg_meta_date"><?=show_date($item->published)?></span></div>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="jeg_slider_thumbnail_wrapper">
			<div class="jeg_slider_thumbnail">
				<?php foreach($items as $item) { ?>
				<div class="jeg_slide_thumbnail_item_wrapper">
					<div class="jeg_slide_thumbnail_item format-video">
						<a href="<?=post_url($item)?>">
							<div class="thumbnail-container size-715 "><img width="120" height="86" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="owl-lazy lazyload" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" /></div>
						</a>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php } ?>