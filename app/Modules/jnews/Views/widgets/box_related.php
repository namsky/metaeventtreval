<?php
	if(isset($items) && is_array($items) && count($items)) {
?>
<div class="jnews_related_post_container">
	<div class="jeg_postblock_22 jeg_postblock jeg_module_hook jeg_pagination_disable jeg_col_2o3">
		<div class="jeg_block_heading jeg_block_heading_6 jeg_subcat_right">
			<h3 class="jeg_block_title"><span>Similar<strong> News</strong></span></h3>
		</div>
		<div class="jeg_block_container">
			<div class="jeg_posts_wrap">
				<div class="jeg_posts jeg_load_more_flag">
					<?php foreach($items as $item) { ?>
					<article class="jeg_post jeg_pl_md_5 format-standard">
						<div class="jeg_thumb">
							<a href="<?=post_url($item)?>">
								<div class="thumbnail-container animate-lazy size-715 "><img width="350" height="250" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
							</a>
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$cate = reset($categories);
							?>
							<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
							<?php } ?>
						</div>
						<div class="jeg_postblock_content">
							<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
							<div class="jeg_post_meta">
								<div class="jeg_meta_date"><a href="javascript:;"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
							</div>
						</div>
					</article>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php } ?>