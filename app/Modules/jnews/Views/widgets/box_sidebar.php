<?php
	if(isset($items) && is_array($items) && count($items) > 1) {
?>
	<div class="jeg_postblock_9 jeg_postblock jeg_module_hook jeg_pagination_disable jeg_col_1o3">
		<div class="jeg_block_heading jeg_block_heading_6 jeg_subcat_right">
			<?php if(isset($category)) { ?>
				<h3 class="jeg_block_title"><span><?=$category->name?></span></h3>
			<?php } ?>
		</div>
		<div class="jeg_block_container">
			<div class="jeg_posts_wrap">
				<div class="jeg_posts jeg_load_more_flag">
					<?php foreach($items as $key=>$item) { ?>
					<article class="jeg_post jeg_pl_md_1 format-standard">
						<div class="jeg_thumb">
							<a href="<?=post_url($item)?>">
								<div class="thumbnail-container animate-lazy  size-500 "><img width="360" height="180" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="Young adults and the evolution of modern food culture" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
							</a>
							<?php
								$categories = $item->categories;
								if(is_array($categories) && count($categories)) {
									$cate = reset($categories);
							?>
							<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
							<?php } ?>
						</div>
						<div class="jeg_postblock_content">
							<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
							<div class="jeg_post_meta">
								<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
							</div>
						</div>
					</article>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
<?php } ?>