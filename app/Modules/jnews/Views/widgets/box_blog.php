<?php
	if(isset($items) && is_array($items) && count($items)) {
?>
	<div data-vc-full-width="true" data-vc-full-width-init="false" class="row vc_row wpb_row vc_row-fluid vc_custom_1483584986908 vc_row-has-fill">
		<div class="jeg-vc-wrapper">
			<div class="wpb_column jeg_column vc_column_container vc_col-sm-12 jeg_main_content">
				<div class="jeg_wrapper wpb_wrapper">
					<div class="jeg_heroblock jeg_heroblock_2 jeg_col_3o3 jeg_hero_style_1 jnews_module_15_10_62047a262638c  " data-margin="2">
						<div class="jeg_hero_wrapper">
							<div class="jeg_heroblock_wrapper" style='margin: 0px 0px -2px -2px;'>
								
								<article class="jeg_post jeg_hero_item_1 format-standard" style="padding: 0 0 2px 2px;">
									<div class="jeg_block_container"> <span class="jeg_postformat_icon"></span>
										<div class="jeg_thumb">
											<a href="<?=post_url($items[0])?>">
												<div class="thumbnail-container thumbnail-background" data-src="<?=$items[0]->thumb?>">
													<div class="lazyloaded" data-src="<?=$items[0]->thumb?>" style="background-image: url(<?=$items[0]->thumb?>)"></div>
												</div>
											</a>
										</div>
										<div class="jeg_postblock_content">
											<?php
												$categories = $items[0]->categories;
												if(is_array($categories) && count($categories)) {
													$cate = reset($categories);
											?>
											<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
											<?php } ?>
											<div class="jeg_post_info">
												<h2 class="jeg_post_title"> <a href="<?=post_url($items[0])?>"><?=$items[0]->title?></a></h2>
												<div class="jeg_post_meta">
													<div class="jeg_meta_author"><span class="by">by</span> <a href="javascript:;"><?=!empty($items[0]->user->name)?$items[0]->user->name:cms_config('site_name')?></a></div>
													<div class="jeg_meta_date"><a href="<?=post_url($items[0])?>"><i class="fa fa-clock-o"></i> <?=show_date($items[0]->published)?></a></div>
												</div>
											</div>
										</div>
									</div>
								</article>
								<div class="jeg_heroblock_scroller">
									<?php 
										foreach($items as $key=>$item) { 
											if($key > 0) {
									?>
									<article class="jeg_post jeg_hero_item_<?=$key+1?> format-standard" style="padding: 0 0 2px 2px;">
										<div class="jeg_block_container"> <span class="jeg_postformat_icon"></span>
											<div class="jeg_thumb">
												<a href="<?=post_url($item)?>">
													<div class="thumbnail-container thumbnail-background" data-src="<?=$item->thumb?>">
														<div class="lazyloaded" data-src="<?=$item->thumb?>" style="background-image: url(<?=$item->thumb?>)"></div>
													</div>
												</a>
											</div>
											<div class="jeg_postblock_content">
												<?php
													$categories = $item->categories;
													if(is_array($categories) && count($categories)) {
														$cate = reset($categories);
												?>
												<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
												<?php } ?>
												<div class="jeg_post_info">
													<h2 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h2>
													<div class="jeg_post_meta">
														<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
													</div>
												</div>
											</div>
										</div>
									</article>
									<?php } } ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>