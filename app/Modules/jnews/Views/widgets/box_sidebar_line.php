<?php
	if(isset($items) && is_array($items) && count($items) > 1) {
?>
	<div class="jeg_sidebar left jeg_sticky_sidebar col-sm-4">
		<div class="jegStickyHolder">
			<div class="theiaStickySidebar">
				<div class="widget widget_jnews_module_block_21" id="jnews_module_block_21-2">
					<div class="jeg_postblock_21 jeg_postblock jeg_module_hook jeg_pagination_disable jeg_col_1o3">
						<div class="jeg_block_heading jeg_block_heading_6 jeg_subcat_right">
							<h3 class="jeg_block_title"><span><?=$title?></span></h3>
						</div>
						<div class="jeg_block_container">
							<div class="jeg_posts jeg_load_more_flag">
								<?php foreach($items as $key=>$item) { ?>
								<article class="jeg_post jeg_pl_sm format-standard">
									<div class="box_wrap">
										<div class="jeg_thumb">
											<a href="<?=post_url($item)?>">
												<div class="thumbnail-container animate-lazy size-715 "><img width="350" height="250" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
											</a>
										</div>
										<div class="jeg_postblock_content">
											<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
											<div class="jeg_post_meta">
												<div class="jeg_meta_date"><a href="javascript:;"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
											</div>
										</div>
									</div>
								</article>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>