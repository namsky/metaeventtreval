<?=$this->section('content');?>
	<?php
	if(isset($item->categories)) {
		$categories = $item->categories;
		$category = reset($categories);
		if(isset($category->id)) {
			$bread_cat = '<span class=""> <a href="'.category_url($category).'">'.$category->name.'</a> </span>';
		} else {
			$bread_cat = '';
		}
	}
	?>
	<div class="post-wrapper">
		<div class="post-wrap">
			<div class="jeg_main ">
				<div class="jeg_container">
					<div class="jeg_content jeg_singlepage">
						<div class="container">
							<div class="jeg_ad jeg_article jnews_article_top_ads">
								<div class='ads-wrapper  '></div>
							</div>
							<div class="row">
								<div class="jeg_main_content col-md-8">
									<div class="jeg_inner_content">
										<div class="jeg_breadcrumbs jeg_breadcrumb_container">
											<div id="breadcrumbs">
												<span class=""> <a href="<?=URL?>/">Home</a> </span>
												<i class="fa fa-angle-right"></i>
												<?=$bread_cat?>
											</div>
										</div>
										<div class="entry-header">
											<h1 class="jeg_post_title"><?=$item->title?></h1>
											<div class="jeg_meta_container">
												<div class="jeg_post_meta jeg_post_meta_1">
													<div class="meta_left">
														<div class="jeg_meta_author"> <span class="meta_text">By</span> <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></div>
														<div class="jeg_meta_date"> <a href="javascript:;"><?=show_date($item->published)?></a></div>
													</div>
													<div class="meta_right">
														<div class="jeg_meta_zoom" data-in-step="3" data-out-step="2">
															<div class="zoom-dropdown">
																<div class="zoom-icon"> <span class="zoom-icon-small">A</span> <span class="zoom-icon-big">A</span></div>
																<div class="zoom-item-wrapper">
																	<div class="zoom-item">
																		<button class="zoom-out"><span>A</span></button>
																		<button class="zoom-in"><span>A</span></button>
																		<div class="zoom-bar-container">
																			<div class="zoom-bar"></div>
																		</div>
																		<button class="zoom-reset"><span>Reset</span></button>
																	</div>
																</div>
															</div>
														</div>
														<div class="jeg_meta_comment"><a href="javascript:;"><i class="fa fa-eye-o"></i> <?=$item->views?></a></div>
													</div>
												</div>
											</div>
										</div>
										<div class="jeg_featured featured_image">
											<a href="javascript:;">
												<div class="thumbnail-container" style="padding-bottom:50%"><img width="750" height="375" src="<?=$item->thumb?>" class=" wp-post-image" alt="<?=$item->title?>" /></div>
											</a>
										</div>
										<div class="entry-content no-share">
											<div class="content-inner  mobile-truncate">
												<?php
													$content = $item->content;
													$cdn = cdn_url();
													if($cdn) {
														$content = preg_replace('/<img(.*?)src=\"http:\/\/(.*?)\"/', '<img$1src="$2"', $content);
														$content = preg_replace('/<img(.*?)src=\"https:\/\/(.*?)\"/', '<img$1src="$2"', $content);
														$content = preg_replace('/<img(.*?)src=\"(.*?)\"/', '<img$1src="'.$cdn.'$2"', $content);
														$content = preg_replace('/&nbsp;/', ' ', $content);
													}
													if(cms_config('lazy_load')) {
														$content = preg_replace('/<img(.*?)class=\"(.*?)\"/', '<img$1', $content);
														$content = preg_replace('/<img(.*?)src=/', '<img src="'.CDN.'/images/blank.gif" class="lazy"$1data-src=', $content);
													}
													if($item->is_adsense) {
														$ads = widget('Cms/Ads', ['zone_id'=>7]);
														$count = substr_count($content,"</p>");
														$pos = round($count/2);
														$ads_pos = 0;
														if($pos) {
															$i = 0;
															while($i<$pos) {
																$ads_pos = strpos($content, "</p>", $ads_pos);
																$ads_pos = $ads_pos + 4;
																$i++;
															}
														}
														if($ads_pos < strlen($content)) {
															$content_first = substr($content, 0, $ads_pos);
															$content_last = substr($content, $ads_pos+1);
															echo $content_first;
															echo $ads;
															echo $content_last;
														} else echo $content;
													} else echo $content;
												?>
											</div>
										</div>
										<?php 
											$args = [];
											$args['type'] = 'related';
											$args['category'] = $category->id;
											$args['excludes'] = $item->id;
											$args['limit'] = 6;
											echo widget('jnews/Box_news', $args); 
										?>
									</div>
								</div>
								<?php 
									$args = [];
									$args['type'] = 'sidebar_line';
									$args['title'] = 'Recent news';
									$args['limit'] = 5;
									echo widget('jnews/Box_news', $args); 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="post-body-class" class="post-template-default single single-post postid-1324 single-format-standard wp-embed-responsive jeg_toggle_dark jeg_single_tpl_1 jnews jsc_normal wpb-js-composer js-comp-ver-6.7.0 vc_responsive"></div>
		</div>
	</div>
<?=$this->endSection();?>