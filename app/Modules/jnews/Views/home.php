<?=$this->section('content');?>
	<!-- CONTENT -->
    <div class="jeg_main">
		<div class="jeg_container">
			<div class="jeg_content">
				<div class="jeg_vc_content">
					<div class="row vc_row wpb_row vc_row-fluid">
						<div class="jeg-vc-wrapper">
							<div class="wpb_column jeg_column vc_column_container vc_col-sm-8 jeg_main_content">
								<div class="jeg_wrapper wpb_wrapper">
									<?php 
										$args = [];
										$args['type'] = 'carousel_posts';
										$args['limit'] = 8;
										echo widget('jnews/Box_news', $args); 
									?>
									<?php 
										$args = [];
										$args['type'] = 'feature';
										$args['limit'] = 4;
										echo widget('jnews/Box_news', $args); 
									?>
									<?php 
										$args = [];
										$args['type'] = 'grid';
										$args['category'] = 6;
										$args['limit'] = 6;
										echo widget('jnews/Box_news', $args); 
									?>
									
									<?php 
										$args = [];
										$args['type'] = 'line';
										$args['category'] = 4;
										$args['limit'] = 3;
										echo widget('jnews/Box_news', $args); 
									?>
								</div>
							</div>
							<div class="wpb_column jeg_column vc_column_container vc_col-sm-4 vc_hidden-xs jeg_sticky_sidebar jeg_sidebar">
								<div class="jegStickyHolder">
									<div class="theiaStickySidebar">
										<div class="jeg_wrapper wpb_wrapper">
											<div class="wpb_widgetised_column wpb_content_element">
												<div class="wpb_wrapper">
													<div class="widget widget_jnews_tab_post" id="jnews_tab_post-2">
														<div class="jeg_tabpost_widget">
															<ul class="jeg_tabpost_nav">
																<li data-tab-content="jeg_tabpost_1" class="active">Bài viêt nổi bật</li>
																<li data-tab-content="jeg_tabpost_2">Bài viết mới nhất</li>
															</ul>
															<div class="jeg_tabpost_content">
																<div class="jeg_tabpost_item active" id="jeg_tabpost_1">
																	<?php 
																		$args = [];
																		$args['type'] = 'sidebar_tab';
																		$args['views'] = 1;
																		$args['limit'] = 3;
																		echo widget('jnews/Box_news', $args); 
																	?>
																</div>
																<div class="jeg_tabpost_item" id="jeg_tabpost_2">
																	<?php 
																		$args = [];
																		$args['type'] = 'sidebar_tab';
																		$args['limit'] = 3;
																		echo widget('jnews/Box_news', $args); 
																	?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<?php 
												$args = [];
												$args['type'] = 'sidebar';
												$args['category'] = 9;
												$args['limit'] = 2;
												echo widget('jnews/Box_news', $args); 
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php 
						$args = [];
						$args['type'] = 'blog';
						$args['category'] = 3;
						$args['hot'] = 1;
						$args['views'] = 1;
						$args['limit'] = 5;
						echo widget('jnews/Box_news', $args); 
					?>
					<div class="vc_row-full-width vc_clearfix"></div>
					<div class="row vc_row wpb_row vc_row-fluid">
						<div class="jeg-vc-wrapper">
							<?php 
								$args = [];
								$args['type'] = 'mixed';
								$args['category'] = 3;
								$args['limit'] = 5;
								echo widget('jnews/Box_news', $args); 
							?>
							<div class="wpb_column jeg_column vc_column_container vc_col-sm-4 jeg_sidebar">
								<div class="jeg_wrapper wpb_wrapper">
									<?php 
										$args = [];
										$args['type'] = 'sidebar';
										$args['category'] = 7;
										$args['limit'] = 2;
										echo widget('jnews/Box_news', $args); 
									?>
								</div>
							</div>
						</div>
					</div>
					
					<div class="vc_row-full-width vc_clearfix"></div>
					<div class="row vc_row wpb_row vc_row-fluid">
						<div class="jeg-vc-wrapper">
							<div class="wpb_column jeg_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs jeg_main_content">
								<div class="jeg_wrapper wpb_wrapper">
									<div class="vc_empty_space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="container">
					<div class="jeg_latestpost ">
						<div class="row">
							<div class="jeg_main_content col-sm-8">
								<?php 
									$args = [];
									$args['type'] = 'line';;
									$args['title'] = 'Latest posts';
									$args['limit'] = 5;
									echo widget('jnews/Box_news', $args); 
								?>
								<div class="jeg_block_navigation"><div class="navigation_overlay"><div class="module-preloader jeg_preloader"><span></span><span></span><span></span></div></div><div class="jeg_block_loadmore "> <a href="" class="" data-load="Load More" data-loading="Loading..."> Load More</a></div></div>
							</div>
							<?php 
								$args = [];
								$args['type'] = 'sidebar_line';
								$args['title'] = 'Recommend';
								$args['recommend'] = 1;
								$args['limit'] = 4;
								echo widget('jnews/Box_news', $args); 
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>