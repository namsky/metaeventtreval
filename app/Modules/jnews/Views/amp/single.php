<!DOCTYPE html>
<html amp lang="en-US" i-amphtml-layout="" i-amphtml-no-boilerplate="" transformed="self;v=1">
<?php
$icon_url = cms_config('site_favicon');
$site_name = cms_config('site_name');
$site_logo = cms_config('site_logo');
if(!$site_logo) {
	$site_logo = CDN.'/images/logo.png';
} elseif(!strstr($site_logo, 'http')) {
	$site_logo = URL.$site_logo;
}
$post_url = post_url($item);
$primary_color = cms_config('primary_color');
$primary_color = $primary_color?$primary_color:'#E74C3C';
$text_color = cms_config('text_color');
$text_color = $text_color?$text_color:'#3F3F3F';
?>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">
    <meta name="generator" content="VCMS">
    <title><?=isset($title)?$title:cms_config('site_name');?></title>
    <link rel="preconnect" href="https://cdn.ampproject.org">
    <link rel="preload" as="script" href="https://cdn.ampproject.org/v0.js">
    <script async="" src="https://cdn.ampproject.org/v0.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
	<style amp-runtime="" i-amphtml-version="012008290323001">
        html {
            overflow-x: hidden !important
        }
        html.i-amphtml-fie {
            height: 100% !important;
            width: 100% !important
        }
        html:not([amp4ads]),
        html:not([amp4ads]) body {
            height: auto !important
        }
        html:not([amp4ads]) body {
            margin: 0 !important
        }
        body {
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            text-size-adjust: 100%
        }
        html.i-amphtml-singledoc.i-amphtml-embedded {
            -ms-touch-action: pan-y;
            touch-action: pan-y
        }
        html.i-amphtml-fie>body,
        html.i-amphtml-singledoc>body {
            overflow: visible !important
        }
        html.i-amphtml-fie:not(.i-amphtml-inabox)>body,
        html.i-amphtml-singledoc:not(.i-amphtml-inabox)>body {
            position: relative !important
        }
        html.i-amphtml-webview>body {
            overflow-x: hidden !important;
            overflow-y: visible !important;
            min-height: 100vh !important
        }
        html.i-amphtml-ios-embed-legacy>body {
            overflow-x: hidden !important;
            overflow-y: auto !important;
            position: absolute !important
        }
        html.i-amphtml-ios-embed {
            overflow-y: auto !important;
            position: static
        }
        #i-amphtml-wrapper {
            overflow-x: hidden !important;
            overflow-y: auto !important;
            position: absolute !important;
            top: 0 !important;
            left: 0 !important;
            right: 0 !important;
            bottom: 0 !important;
            margin: 0 !important;
            display: block !important
        }
        html.i-amphtml-ios-embed.i-amphtml-ios-overscroll,
        html.i-amphtml-ios-embed.i-amphtml-ios-overscroll>#i-amphtml-wrapper {
            -webkit-overflow-scrolling: touch !important
        }
        #i-amphtml-wrapper>body {
            position: relative !important;
            border-top: 1px solid transparent !important
        }
        #i-amphtml-wrapper+body {
            visibility: visible
        }
        #i-amphtml-wrapper+body .i-amphtml-lightbox-element,
        #i-amphtml-wrapper+body[i-amphtml-lightbox] {
            visibility: hidden
        }
        #i-amphtml-wrapper+body[i-amphtml-lightbox] .i-amphtml-lightbox-element {
            visibility: visible
        }
        #i-amphtml-wrapper.i-amphtml-scroll-disabled,
        .i-amphtml-scroll-disabled {
            overflow-x: hidden !important;
            overflow-y: hidden !important
        }
        amp-instagram {
            padding: 54px 0px 0px !important;
            background-color: #fff
        }
        amp-iframe iframe {
            box-sizing: border-box !important
        }
        [amp-access][amp-access-hide] {
            display: none
        }
        [subscriptions-dialog],
        body:not(.i-amphtml-subs-ready) [subscriptions-action],
        body:not(.i-amphtml-subs-ready) [subscriptions-section] {
            display: none !important
        }
        amp-experiment,
        amp-live-list>[update] {
            display: none
        }
        .i-amphtml-jank-meter {
            position: fixed;
            background-color: rgba(232, 72, 95, 0.5);
            bottom: 0;
            right: 0;
            color: #fff;
            font-size: 16px;
            z-index: 1000;
            padding: 5px
        }
        amp-list[resizable-children]>.i-amphtml-loading-container.amp-hidden {
            display: none !important
        }
        amp-list [fetch-error],
        amp-list[load-more] [load-more-button],
        amp-list[load-more] [load-more-end],
        amp-list[load-more] [load-more-failed],
        amp-list[load-more] [load-more-loading] {
            display: none
        }
        amp-list[diffable] div[role=list] {
            display: block
        }
        amp-story-page,
        amp-story[standalone] {
            min-height: 1px !important;
            display: block !important;
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            overflow: hidden !important;
            width: 100% !important
        }
        amp-story[standalone] {
            background-color: #202125 !important;
            position: relative !important
        }
        amp-story-page {
            background-color: #757575
        }
        amp-story .amp-active>div,
        amp-story .i-amphtml-loader-background {
            display: none !important
        }
        amp-story-page:not(:first-of-type):not([distance]):not([active]) {
            transform: translateY(1000vh) !important
        }
        amp-autocomplete {
            position: relative !important;
            display: inline-block !important
        }
        amp-autocomplete>input,
        amp-autocomplete>textarea {
            padding: 0.5rem;
            border: 1px solid rgba(0, 0, 0, 0.33)
        }

        .i-amphtml-autocomplete-results,
        amp-autocomplete>input,
        amp-autocomplete>textarea {
            font-size: 1rem;
            line-height: 1.5rem
        }

        [amp-fx^=fly-in] {
            visibility: hidden
        }

        amp-script[nodom] {
            position: fixed !important;
            top: 0 !important;
            width: 1px !important;
            height: 1px !important;
            overflow: hidden !important;
            visibility: hidden
        }

        /*# sourceURL=/css/ampdoc.css*/
        [hidden] {
            display: none !important
        }

        .i-amphtml-element {
            display: inline-block
        }

        .i-amphtml-blurry-placeholder {
            transition: opacity 0.3s cubic-bezier(0.0, 0.0, 0.2, 1) !important;
            pointer-events: none
        }

        [layout=nodisplay]:not(.i-amphtml-element) {
            display: none !important
        }

        .i-amphtml-layout-fixed,
        [layout=fixed][width][height]:not(.i-amphtml-layout-fixed) {
            display: inline-block;
            position: relative
        }

        .i-amphtml-layout-responsive,
        [layout=responsive][width][height]:not(.i-amphtml-layout-responsive),
        [width][height][heights]:not([layout]):not(.i-amphtml-layout-responsive),
        [width][height][sizes]:not([layout]):not(.i-amphtml-layout-responsive) {
            display: block;
            position: relative
        }

        .i-amphtml-layout-intrinsic,
        [layout=intrinsic][width][height]:not(.i-amphtml-layout-intrinsic) {
            display: inline-block;
            position: relative;
            max-width: 100%
        }

        .i-amphtml-layout-intrinsic .i-amphtml-sizer {
            max-width: 100%
        }

        .i-amphtml-intrinsic-sizer {
            max-width: 100%;
            display: block !important
        }

        .i-amphtml-layout-container,
        .i-amphtml-layout-fixed-height,
        [layout=container],
        [layout=fixed-height][height]:not(.i-amphtml-layout-fixed-height) {
            display: block;
            position: relative
        }

        .i-amphtml-layout-fill,
        [layout=fill]:not(.i-amphtml-layout-fill) {
            display: block;
            overflow: hidden !important;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0
        }

        .i-amphtml-layout-flex-item,
        [layout=flex-item]:not(.i-amphtml-layout-flex-item) {
            display: block;
            position: relative;
            -ms-flex: 1 1 auto;
            flex: 1 1 auto
        }

        .i-amphtml-layout-fluid {
            position: relative
        }

        .i-amphtml-layout-size-defined {
            overflow: hidden !important
        }

        .i-amphtml-layout-awaiting-size {
            position: absolute !important;
            top: auto !important;
            bottom: auto !important
        }

        i-amphtml-sizer {
            display: block !important
        }

        .i-amphtml-blurry-placeholder,
        .i-amphtml-fill-content {
            display: block;
            height: 0;
            max-height: 100%;
            max-width: 100%;
            min-height: 100%;
            min-width: 100%;
            width: 0;
            margin: auto
        }

        .i-amphtml-layout-size-defined .i-amphtml-fill-content {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0
        }

        .i-amphtml-replaced-content,
        .i-amphtml-screen-reader {
            padding: 0 !important;
            border: none !important
        }

        .i-amphtml-screen-reader {
            position: fixed !important;
            top: 0px !important;
            left: 0px !important;
            width: 4px !important;
            height: 4px !important;
            opacity: 0 !important;
            overflow: hidden !important;
            margin: 0 !important;
            display: block !important;
            visibility: visible !important
        }

        .i-amphtml-screen-reader~.i-amphtml-screen-reader {
            left: 8px !important
        }

        .i-amphtml-screen-reader~.i-amphtml-screen-reader~.i-amphtml-screen-reader {
            left: 12px !important
        }

        .i-amphtml-screen-reader~.i-amphtml-screen-reader~.i-amphtml-screen-reader~.i-amphtml-screen-reader {
            left: 16px !important
        }

        .i-amphtml-unresolved {
            position: relative;
            overflow: hidden !important
        }

        .i-amphtml-select-disabled {
            -webkit-user-select: none !important;
            -moz-user-select: none !important;
            -ms-user-select: none !important;
            user-select: none !important
        }

        .i-amphtml-notbuilt,
        [layout]:not(.i-amphtml-element),
        [width][height][heights]:not([layout]):not(.i-amphtml-element),
        [width][height][sizes]:not([layout]):not(.i-amphtml-element) {
            position: relative;
            overflow: hidden !important;
            color: transparent !important
        }

        .i-amphtml-notbuilt:not(.i-amphtml-layout-container)>*,
        [layout]:not([layout=container]):not(.i-amphtml-element)>*,
        [width][height][heights]:not([layout]):not(.i-amphtml-element)>*,
        [width][height][sizes]:not([layout]):not(.i-amphtml-element)>* {
            display: none
        }

        .i-amphtml-notbuilt:not(.i-amphtml-layout-container),
        [layout]:not([layout=container]):not(.i-amphtml-element),
        [width][height][heights]:not([layout]):not(.i-amphtml-element),
        [width][height][sizes]:not([layout]):not(.i-amphtml-element) {
            color: transparent !important;
            line-height: 0 !important
        }

        .i-amphtml-ghost {
            visibility: hidden !important
        }

        .i-amphtml-element>[placeholder],
        [layout]:not(.i-amphtml-element)>[placeholder],
        [width][height][heights]:not([layout]):not(.i-amphtml-element)>[placeholder],
        [width][height][sizes]:not([layout]):not(.i-amphtml-element)>[placeholder] {
            display: block
        }

        .i-amphtml-element>[placeholder].amp-hidden,
        .i-amphtml-element>[placeholder].hidden {
            visibility: hidden
        }

        .i-amphtml-element:not(.amp-notsupported)>[fallback],
        .i-amphtml-layout-container>[placeholder].amp-hidden,
        .i-amphtml-layout-container>[placeholder].hidden {
            display: none
        }

        .i-amphtml-layout-size-defined>[fallback],
        .i-amphtml-layout-size-defined>[placeholder] {
            position: absolute !important;
            top: 0 !important;
            left: 0 !important;
            right: 0 !important;
            bottom: 0 !important;
            z-index: 1
        }

        .i-amphtml-notbuilt>[placeholder] {
            display: block !important
        }

        .i-amphtml-hidden-by-media-query {
            display: none !important
        }

        .i-amphtml-element-error {
            background: red !important;
            color: #fff !important;
            position: relative !important
        }

        .i-amphtml-element-error:before {
            content: attr(error-message)
        }

        i-amp-scroll-container,
        i-amphtml-scroll-container {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            display: block
        }

        i-amp-scroll-container.amp-active,
        i-amphtml-scroll-container.amp-active {
            overflow: auto;
            -webkit-overflow-scrolling: touch
        }

        .i-amphtml-loading-container {
            display: block !important;
            pointer-events: none;
            z-index: 1
        }

        .i-amphtml-notbuilt>.i-amphtml-loading-container {
            display: block !important
        }

        .i-amphtml-loading-container.amp-hidden {
            visibility: hidden
        }

        .i-amphtml-element>[overflow] {
            cursor: pointer;
            position: relative;
            z-index: 2;
            visibility: hidden;
            display: initial;
            line-height: normal
        }

        .i-amphtml-element>[overflow].amp-visible {
            visibility: visible
        }

        template {
            display: none !important
        }

        .amp-border-box,
        .amp-border-box *,
        .amp-border-box :after,
        .amp-border-box :before {
            box-sizing: border-box
        }

        amp-pixel {
            display: none !important
        }

        amp-analytics,
        amp-auto-ads,
        amp-story-auto-ads {
            position: fixed !important;
            top: 0 !important;
            width: 1px !important;
            height: 1px !important;
            overflow: hidden !important;
            visibility: hidden
        }

        html.i-amphtml-fie>amp-analytics {
            position: initial !important
        }

        [visible-when-invalid]:not(.visible),
        form [submit-error],
        form [submit-success],
        form [submitting] {
            display: none
        }

        amp-accordion {
            display: block !important
        }

        amp-accordion>section {
            float: none !important
        }

        amp-accordion>section>* {
            float: none !important;
            display: block !important;
            overflow: hidden !important;
            position: relative !important
        }

        amp-accordion,
        amp-accordion>section {
            margin: 0
        }

        amp-accordion>section>:last-child {
            display: none !important
        }

        amp-accordion>section[expanded]>:last-child {
            display: block !important
        }
    </style>
    <style amp-custom="">
        #amp-mobile-version-switcher {
            position: absolute;
            width: 100%;
            left: 0;
            z-index: 100
        }

        #amp-mobile-version-switcher>a {
            display: block;
            padding: 15px 0;
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;
            font-size: 16px;
            font-weight: 600;
            color: #eaeaea;
            text-align: center;
            text-decoration: none;
            background-color: #444;
            border: 0
        }

        #amp-mobile-version-switcher>a:hover,
        #amp-mobile-version-switcher>a:focus,
        #amp-mobile-version-switcher>a:active {
            text-decoration: underline
        }

        .block-image {
            margin-bottom: 1em
        }

        .block-image amp-img {
            max-width: 100%
        }

        amp-img.amp-enforced-sizes[layout="intrinsic"]>img {
            object-fit: contain
        }

        .amp-enforced-sizes {
            max-width: 100%;
            margin: 0 auto
        }

        html {
            background: <?=$primary_color?>
        }

        body {
            background: #fff;
            color: <?=$text_color?>;
            font-family: Roboto, Tahoma, Serif;
            font-weight: 300;
            line-height: 1.75em
        }

        p,
        figure {
            margin: 0 0 1em;
            padding: 0
        }

        a,
        a:visited {
            color: <?=$primary_color?>
        }

        a:hover,
        a:active,
        a:focus {
            color: <?=$text_color?>
        }

        .amp-meta,
        .amp-header div,
        .amp-title,
        .amp-tax-category,
        .amp-comments-link,
        .amp-footer p,
        .back-to-top {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen-Sans", "Ubuntu", "Cantarell", "Helvetica Neue", sans-serif
        }
		.amp-site-icon {
			float: left;
			margin-right: 20px;
		}
		.amp-site-title {
			float: left;
			line-height: 32px;
		}
        .amp-header {
            background-color: <?=$primary_color?>
        }

        .amp-header div {
            color: #fff;
            font-size: 1em;
            font-weight: 400;
            margin: 0 auto;
            max-width: calc(840px - 32px);
            padding: .875em 16px;
            position: relative
        }

        .amp-header a {
            color: #fff;
            text-decoration: none
        }

        .amp-article {
            color: <?=$text_color?>;
            font-weight: 400;
            margin: 1.5em auto;
            max-width: 840px;
            overflow-wrap: break-word;
            word-wrap: break-word
        }

        .amp-article-header {
            align-items: center;
            align-content: stretch;
            display: flex;
            flex-wrap: wrap;
            justify-content: space-between;
            margin: 1.5em 16px 0
        }

        .amp-title {
            color: <?=$text_color?>;
            display: block;
            flex: 1 0 100%;
            font-weight: 900;
            margin: 0 0 .625em;
            width: 100%
        }

        .amp-meta {
            color: #696969;
            display: inline-block;
            flex: 2 1 50%;
            font-size: .875em;
            line-height: 1.5em;
            margin: 0 0 1.5em;
            padding: 0
        }
		.amp-meta a {
			padding-right: 10px;
		}

        .amp-article-header .amp-meta:last-of-type {
            text-align: right
        }

        .amp-article-header .amp-meta:first-of-type {
            text-align: left
        }

        .amp-byline amp-img,
        .amp-byline .amp-author {
            display: inline-block;
            vertical-align: middle
        }

        .amp-byline amp-img {
            border: 1px solid <?=$primary_color?>;
            border-radius: 50%;
            position: relative;
            margin-right: 6px
        }

        .amp-posted-on {
            text-align: right
        }

        .amp-article-featured-image {
            margin: 0 0 1em
        }

        .amp-article-featured-image amp-img {
            margin: 0 auto;
			width: 100%
        }

        .amp-article-content {
            margin: 0 16px
        }

        .amp-article-content .caption {
            max-width: 100%
        }

        .amp-article-content amp-img {
            margin: 0 auto
        }

        .caption {
            padding: 0
        }

        .amp-article-footer .amp-meta {
            display: block
        }

        .amp-tax-category {
            color: #696969;
            font-size: .875em;
            line-height: 1.5em;
            margin: 1.5em 16px
        }
        .amp-tax-tag {
            color: #696969;
            font-size: .875em;
            line-height: 1.5em;
            margin: 1.5em 16px
        }

        .amp-comments-link {
            color: #696969;
            font-size: .875em;
            line-height: 1.5em;
            text-align: center;
            margin: 2.25em 0 1.5em
        }

        .amp-comments-link a {
            border-style: solid;
            border-color: #c2c2c2;
            border-width: 1px 1px 2px;
            border-radius: 4px;
            background-color: transparent;
            color: <?=$primary_color?>;
            cursor: pointer;
            display: block;
            font-size: 14px;
            font-weight: 600;
            line-height: 18px;
            margin: 0 auto;
            max-width: 200px;
            padding: 11px 16px;
            text-decoration: none;
            width: 50%;
            -webkit-transition: background-color .2s ease;
            transition: background-color .2s ease
        }

        .amp-footer {
            border-top: 1px solid #c2c2c2;
            margin: calc(1.5em - 1px) 0 0
        }

        .amp-footer div {
            margin: 0 auto;
            max-width: calc(840px - 32px);
            padding: 1.25em 16px 1.25em;
            position: relative
        }

        .amp-footer h2 {
			font-size: 1em;
			line-height: 1em;
			margin: 0;
			float: left;
        }

        .amp-footer p {
            color: #696969;
            font-size: .8em;
            line-height: 1.5em;
            margin: 0 85px 0 0
        }

        .amp-footer a {
            text-decoration: none
        }

        .back-to-top {
			font-size: .8em;
			font-weight: 600;
			line-height: 1em;
			float: right;
        }
        .clearfix {
			clear: both;
			margin: 0 !important;
			padding: 0 !important;
        }
		
		.related-post .related-title {
			text-transform: uppercase;
			font-weight: bold;
			font-size: 20px;
			padding: 10px 20px;
		}
		.related-post ul {
			list-style: disc;
			padding: 0 40px;
			margin: 0;
		}
		.related-post .art-list a {
			text-decoration: none;
		}
		.related-post .art-list .title-text {
			font-size: 20px;
			margin: 5px 0;
			font-weight: normal;
		}
		.faq-title {
			text-transform: uppercase;
			font-weight: bold;
			font-size: 20px;
			padding: 10px;
		}
		.faq-question {
			padding: 8px 20px;
			display: block;
			margin-top: 5px;
			border-left: 3px solid #DA6050;
			font-size: 18px;
			background-color: #eee;
		}
		.faq-question h3 {
			font-size: 18px;
			display: inline-block;
		}
		.faq-question .rotate-icon {
			float: right;
			line-height: 30px;
		}
		.faq-question:not(.collapsed) .rotate-icon {
			-webkit-transform: rotate(180deg);
			transform: rotate(180deg);
		}
		.faq-content p {
			padding: 10px 20px;
			border: 3px solid var(--border-color);
		}
        /*# sourceURL=amp-custom.css */
    </style>
    <link rel="canonical" href="<?=$post_url?>">
    <script type="application/ld+json">
        {
            "@context": "http:\/\/schema.org",
            "publisher": {
                "@type": "Organization",
                "name": "<?=$site_name?>",
                "logo": {
                    "@type": "ImageObject",
                    "url": "<?=$site_logo?>"
                }
            },
            "@type": "BlogPosting",
            "mainEntityOfPage": "<?=$post_url?>",
            "headline": "<?=isset($item->title)?$item->title:''?>",
            "datePublished": "<?php echo gmdate('c', $item->published)?>",
            "dateModified": "<?php echo gmdate('c', $item->modified)?>",
			<?php
			if(!empty($item->user)) {
				$username = !empty($item->user->name)?$item->user->name:cms_config('site_name');
			?>
            "author": {
                "@type": "Person",
                "name": "<?=$username?>"
            },
			<?php } ?>
            "image": "<?=isset($item->thumb)?$item->thumb:''?>"
        }
    </script>
</head>
<body>
    <header id="top" class="amp-header">
        <div>
            <a href="<?=URL?>" rel="amphtml">
				<?php
				if($icon_url) echo '<amp-img src="'.$icon_url.'" width="32" height="32" class="amp-site-icon"></amp-img>';
				?>
				<span class="amp-site-title">
					<?=$site_name?>
				</span>
				<div class="clearfix"></div>
            </a>
        </div>
    </header>

    <article class="amp-article">
        <header class="amp-article-header">
            <h1 class="amp-title"><?=isset($item->title)?$item->title:''?></h1>
			<?php
			if(!empty($item->user)) {
				$avatar = !empty($item->user->avatar)?show_thumb($item->user->avatar,24,24):CDN.'/images/no-avatar.jpg';
				$username = !empty($item->user->name)?$item->user->name:cms_config('site_name');
			?>
            <div class="amp-meta amp-byline">
                <amp-img src="<?=$avatar?>" alt="<?=$username?>" width="24" height="24" layout="fixed" class="i-amphtml-layout-fixed i-amphtml-layout-size-defined" style="width:24px;height:24px;" i-amphtml-layout="fixed"></amp-img>
                <span class="amp-author author vcard"><?=$username?></span>
            </div>
			<div class="amp-meta amp-posted-on">
				<time datetime="<?php echo gmdate('c', $item->published)?>">
					<?=show_date($item->published)?>
				</time>
			</div>
			<?php
			}
			?>
        </header>
		<div class="amp-article-content">
			<?php
				$content = $item->content;
				$content = preg_replace('/width="(.*)"/', '', $content);
				$content = preg_replace('/height="(.*)"/', '', $content);
				preg_match_all("/<img(.*)src=\"(.*)\"/U", $content, $images);
				if(count($images) == 3) {
					foreach($images[2] as $image) {
						$replace_image = show_thumb($image, 840, 600);
						$content = str_replace($image, $replace_image, $content);
					}
				}
				$content = str_replace('<img', '<amp-img width="840" height="600" layout="responsive"', $content);
				$content = str_replace('<span class="toc_toggle">[ Ẩn ]</span>', '', $content);
				$content = str_replace('<iframe', '<amp-iframe width="840" height="600" layout="responsive"', $content);
				$content = str_replace('</iframe>', '</amp-iframe>', $content);
				echo $content;
			?>
		</div>
        <footer class="amp-article-footer">
			<?php
			if(!empty($item->categories)) {
			?>
            <div class="amp-meta amp-tax-category">
                Categories: <?php
				foreach($item->categories as $category) {
					echo '<a href="'.category_url($category).'" rel="category amphtml">'.$category->name.'</a>';
				}
				?>
			</div>
			<?php } ?>
			<?php
			$tags = [];
			if(!empty($item->tags)) {
			?>
			<div class="amp-meta amp-tax-tag">
				Tags: <?php
				foreach($item->tags as $tag) {
					$tags[] = $tag->id;
					echo '<a href="'.tag_url($tag).'" rel="tag amphtml">'.$tag->name.'</a>';
				}
				?>
			</div>
			<?php if($item->faqs) { ?>
			<div class="single-faqs mb-3">
				<div class="faq-title">Câu hỏi thường gặp</div>
				<?php
				foreach($item->faqs as $faq) {
				?>
				<div class="faq-items">
					<a class="faq-question collapsed" data-toggle="collapse" href="#faq-<?=$faq->id?>" aria-expanded="false" aria-controls="faq-<?=$faq->id?>">
						<h3><?=$faq->question?></h3>
						<i class="fas fa-angle-down rotate-icon"></i>
					</a>
				</div>
				<div class="collapse faq-content" id="faq-<?=$faq->id?>">
					<p><?=$faq->answer?></p>
				</div>
				<?php
				}
				echo $cms->schemas('faq', ['faqs' => $item->faqs]);
				?>
			</div>
			<?php } ?>
			<div class="amp-related">
			<?php } ?>
			<?=widget('evworld/Box_news', ['type'=>'related', 'title'=>'Bài viết liên quan', 'post_heading' => 'h4', 'tags'=>$tags, 'excludes' => $item->id]) ?>
			</div>
        </footer>
    </article>
    <footer class="amp-footer">
        <div>
            <h2><?=$site_name?></h2>
            <a href="#top" class="back-to-top">Back to top</a>
			<div class="clearfix"></div>
        </div>
    </footer>
    <div id="amp-mobile-version-switcher">
        <a rel="noamphtml nofollow" href="<?=$post_url?>">Exit mobile version</a>
    </div>
</body>
</html>