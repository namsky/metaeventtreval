<?=$this->section('content');?>
    <!-- CONTENT -->
    <div class="jeg_main ">
		<div class="jeg_container">
			<div class="jeg_content">
				<div class="jnews_category_header_top"></div>
				<div class="jeg_section">
					<div class="container">
						<div class="jeg_ad jeg_category jnews_archive_above_hero_ads ">
							<div class='ads-wrapper  '></div>
						</div>
						<div class="jnews_category_hero_container">
							<div class="jeg_heroblock jeg_heroblock_1 jeg_col_3o3 jeg_hero_style_1" data-margin="10">
								<div class="jeg_hero_wrapper">
									<div class="jeg_heroblock_wrapper" style='margin: 0px 0px -10px -10px;'>
										<?php
										foreach($items as $key=>$item) {
											if($key == 0) {
										?>
										<article class="jeg_post jeg_hero_item_1 format-standard" style="padding: 0 0 10px 10px;">
											<div class="jeg_block_container"> <span class="jeg_postformat_icon"></span>
												<div class="jeg_thumb">
													<a href="<?=post_url($item)?>" title="">
														<div class="thumbnail-container thumbnail-background" data-src="<?=$item->thumb?>">
															<div class="lazyloaded" data-src="<?=$item->thumb?>" style="background-image: url(<?=$item->thumb?>)"></div>
														</div>
													</a>
												</div>
												<div class="jeg_postblock_content">
													<?php
														$categories = $item->categories;
														if(is_array($categories) && count($categories)) {
															$cate = reset($categories);
													?>
													<div class="jeg_post_category"><a href="<?=category_url($cate)?>"><?=$cate->name?></a></div>
													<?php } ?>
													<div class="jeg_post_info">
														<h2 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h2>
														<div class="jeg_post_meta">
															<div class="jeg_meta_author"><span class="by">by</span> <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></div>
															<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
														</div>
													</div>
												</div>
											</div>
										</article>
										<?php } } ?>
										<div class="jeg_heroblock_scroller">
											<?php
											foreach($items as $key=>$item) {
												if($key > 0 && $key < 4) {
											?>
											<article class="jeg_post jeg_hero_item_<?=$key+1?> format-standard" style="padding: 0 0 10px 10px;">
												<div class="jeg_block_container"> <span class="jeg_postformat_icon"></span>
													<div class="jeg_thumb">
														<a href="<?=post_url($item)?>" title="">
															<div class="thumbnail-container thumbnail-background" data-src="<?=$item->thumb?>">
																<div class="lazyloaded" data-src="<?=$item->thumb?>" style="background-image: url(<?=$item->thumb?>)"></div>
															</div>
														</a>
													</div>
													<div class="jeg_postblock_content">
														<?php
															$categories = $item->categories;
															if(is_array($categories) && count($categories)) {
																$cate = reset($categories);
														?>
														<div class="jeg_post_category"><a href="<?=category_url($cate)?>"><?=$cate->name?></a></div>
														<?php } ?>
														<div class="jeg_post_info">
															<h2 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h2>
															<div class="jeg_post_meta">
																<div class="jeg_meta_author"><span class="by">by</span> <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></div>
																<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
															</div>
														</div>
													</div>
												</div>
											</article>
											<?php } } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						if(isset($page)) {
							switch($page) {
								case 'search':
									$page_title = 'Search: '.htmlentities($key);
									break;
								case 'category':
									$page_title = $category->name;
									break;
								case 'tag':
									$page_title = $tag->name;
									break;
							}
						}
						?>
						<div class="jeg_cat_content row">
							<div class="jeg_main_content jeg_column col-sm-8">
								<div class="jeg_inner_content">
									<div class="jnews_category_header_bottom">
										<div class="jeg_cat_header jeg_cat_header_1">
											<div class="jeg_breadcrumbs jeg_breadcrumb_category jeg_breadcrumb_container">
												<div id="breadcrumbs">
													<span class=""> <a href="<?=URL?>/">Home</a> </span>
													<i class="fa fa-angle-right"></i>
													<span class="breadcrumb_last_link"> 
														<a href="javascript:;"><?=$page_title?></a> 
													</span>
												</div>
											</div>
											<h1 class="jeg_cat_title"><?=$page_title?></h1></div>
									</div>
									<div class="jnews_category_content_wrapper">
										<div class="jeg_postblock_5 jeg_postblock jeg_module_hook jeg_pagination_nav_1 jeg_col_2o3">
											<div class="jeg_block_container">
												<div class="jeg_posts jeg_load_more_flag">
													<?php
													foreach($items as $key=>$item) {
														if($key >= 4) {
													?>
													<article class="jeg_post jeg_pl_lg_2 format-standard">
														<div class="jeg_thumb">
															<a href="<?=post_url($item)?>">
																<div class="thumbnail-container animate-lazy size-715 "><img width="350" height="250" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==" class="lazyload wp-post-image" alt="<?=$item->title?>" loading="lazy" data-src="<?=$item->thumb?>" data-sizes="auto" data-expand="700" /></div>
															</a>
															<?php
																$categories = $item->categories;
																if(is_array($categories) && count($categories)) {
																	$cate = reset($categories);
															?>
															<div class="jeg_post_category"><span><a href="<?=category_url($cate)?>"><?=$cate->name?></a></span></div>
															<?php } ?>
														</div>
														<div class="jeg_postblock_content">
															<h3 class="jeg_post_title"> <a href="<?=post_url($item)?>"><?=$item->title?></a></h3>
															<div class="jeg_post_meta">
																<div class="jeg_meta_author"><span class="by">by</span> <a href="javascript:;"><?=!empty($item->user->name)?$item->user->name:cms_config('site_name')?></a></div>
																<div class="jeg_meta_date"><a href="<?=post_url($item)?>"><i class="fa fa-clock-o"></i> <?=show_date($item->published)?></a></div>
																<div class="jeg_meta_comment"><a href="javascript:;"><i class="fa fa-eye"></i> <?=$item->views?> </a></div>
															</div>
															<div class="jeg_post_excerpt">
																<p>
																	<?=isset($item->summary)?cutOf($item->summary, 120):cutOf(strip_tags($item->content), 120)?>
																</p> 
																<a href="<?=post_url($item)?>" class="jeg_readmore">Read more</a></div>
														</div>
													</article>
													<?php } } ?>
												</div>
											</div>
											<div class="jeg_block_navigation">
												<div style="text-align:center">
												<?=$paging?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php 
								$args = [];
								$args['type'] = 'sidebar_line';
								$args['title'] = 'Recent news';
								$args['limit'] = 5;
								echo widget('jnews/Box_news', $args); 
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?=$this->endSection();?>