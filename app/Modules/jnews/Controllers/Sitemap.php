<?php
namespace App\Modules\evworld\Controllers;
use App\Modules\Cms\Widgets\Grids;
use App\Core\Cms\CmsController;

class Sitemap extends CmsController
{
	private $limit = 100;
	public function index()
	{
		$this->response->setHeader("Content-Type", "text/xml");
		$this->response->setHeader("Pragma", "no-cache");
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		$postModel = model('App\Modules\Cms\Models\PostModel');
		$count_post = $postModel->countAll();
		$total_page = 1;
		if($count_post > $this->limit) {
			$total_page = ceil($count_post/$this->limit);
		}
		$this->view->setVar('total_page', $total_page);
		$data = $this->view->getData();
		return view('\App\Modules\Frontend\Views\sitemap\index', $data, ['debug'=>false]);
	}
	public function news($page = 1)
	{
		$this->response->setHeader("Content-Type", "text/xml");
		$this->response->setHeader("Pragma", "no-cache");
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('sitemap_news'.$page):false;
		if(!$items) {
			$postModel = model('App\Modules\Cms\Models\PostModel');
			$offset = ($page-1)*$this->limit;
			$items = $postModel->where('is_search', 1)->orderBy('sitemap_priority', 'DESC')->orderBy('published', 'DESC')->findAll($this->limit, $offset);
			$this->cache->save('sitemap_news'.$page, $items, 86400);
		}
		$this->view->setVar('posts', $items);
		$data = $this->view->getData();
		return view('\App\Modules\Frontend\Views\sitemap\post', $data, ['debug'=>false]);
	}
	public function category($page = 1)
	{
		$this->response->setHeader("Content-Type", "text/xml");
		$this->response->setHeader("Pragma", "no-cache");
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('sitemap_category'.$page):false;
		if(!$items) {
			$categoryModel = model('App\Modules\Cms\Models\CategoryModel');
			$items = $categoryModel->findAll($this->limit);
			$this->cache->save('sitemap_category'.$page, $items, 86400);
		}
		$this->view->setVar('categories', $items);
		$data = $this->view->getData();
		return view('\App\Modules\Frontend\Views\sitemap\category', $data, ['debug'=>false]);
	}
	public function tag($page = 1)
	{
		$this->response->setHeader("Content-Type", "text/xml");
		$this->response->setHeader("Pragma", "no-cache");
        $this->cms->load_vendors([
            'fontawesome',
            'jquery',
            'bootstrap',
            'sweetalert2',
            'jquery.lazyload',
        ]);
		$cached = cms_config('cache');
		$items = $cached?$this->cache->get('sitemap_tag'.$page):false;
		if(!$items) {
			$tagModel = model('App\Modules\Cms\Models\TagModel');
			$items = $tagModel->findAll($this->limit);
			$this->cache->save('sitemap_tag'.$page, $items, 86400);
		}
		$this->view->setVar('tags', $items);
		$data = $this->view->getData();
		return view('\App\Modules\Frontend\Views\sitemap\tag', $data, ['debug'=>false]);
	}
	public function style()
	{
		$this->response->setHeader("Content-Type", "text/xml");
		$this->response->setHeader("Pragma", "no-cache");
		return view('\App\Modules\Frontend\Views\sitemap\style', [], ['debug'=>false]);
	}
}