<?php
namespace App\Core\Cms;
use CodeIgniter\Controller;
use Config\View;
use Helper\Cms;
use App\Core\Cms\CmsView;
use App\Modules\Cms\Libraries\UserAuth;

class CmsController extends Controller
{
	protected $helpers = ['cms'];
	protected $auth;
	public $view;
	public $cache;
	public $cms;
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		parent::initController($request, $response, $logger);
		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// Load cache
		$this->cache = \Config\Services::cache();
		// Preload cms library
		$this->cms = new \App\Libraries\Cms();
		// Preload Auth library
		$this->auth = new UserAuth();
		// Load config views
		$path = dirname(dirname(__DIR__));
		$class = get_class($this);
		if(strpos($class, '\\')) {
			$_class = explode('\\', $class);
			if(count($_class) > 3) {
				$view_path = $_class[count($_class)-3];
			}
		} else $view_path = 'Frontend';
		$view_path = $path.DIRECTORY_SEPARATOR.'Modules'.DIRECTORY_SEPARATOR.$view_path.DIRECTORY_SEPARATOR.'Views';
		$this->view = new CmsView(config('View'), $view_path);
		$layout = $view_path.DIRECTORY_SEPARATOR.'layouts'.DIRECTORY_SEPARATOR.'main.php';
		$this->view->extend($layout);
		
		$this->view->setVar('auth', $this->auth);
		$this->view->setVar('cms', $this->cms);
	}
}
